var SWPrecacheWebpackPlugin = require('sw-precache-webpack-plugin');
module.exports = {
    navigateFallback: '/index.html',
    navigateFallbackWhitelist: [/^(?!\/__)/], // <-- necessary for Firebase OAuth
    stripPrefix: 'agvantage',
    root: 'agvantage/',
    runtimeCaching: [{
    	    urlPattern: /https:\/\/129.154.71.56:9074\AgVantageGetSellerCommodityListSB\/GetSellerCommodityListPS\/api\/v1\/getSellerCommodityList?UID=1/,
    	    handler: 'cacheFirst'
    	  }
    	  ],
    plugins: [
        new SWPrecacheWebpackPlugin({
          cacheId: 'firestarter',
          filename: 'service-worker.js',
          staticFileGlobs: [
            'agvantage/index.html',
            'agvantage/**.js',
            'agvantage/**.css'
          ],
          stripPrefix: 'agvantage/assets/',
          mergeStaticsConfig: true // if you don't set this to true, you won't see any webpack-emitted assets in your serviceworker config
        }),
      ]
  };