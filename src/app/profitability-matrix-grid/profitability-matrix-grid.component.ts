import { Component, ViewChild, OnInit,ChangeDetectorRef } from "@angular/core";
import {GridOptions} from "ag-grid/main";

@Component({
  selector: 'app-profitability-matrix-grid',
  templateUrl: './profitability-matrix-grid.component.html',
  styleUrls: ['./profitability-matrix-grid.component.scss']
})
export class ProfitabilityMatrixGridComponent implements OnInit {

  gridOptions: GridOptions;
     gridApi;
   gridColumnApi;

   columnDefs;

   rowData;
   defaultColDef;
   getRowNodeId;
 
     gridApi1;
   gridColumnApi1;

   rowData1;
   defaultColDef1;
   getRowNodeId1;
  constructor(private cdRef:ChangeDetectorRef) {
  
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
          this.gridOptions.api.sizeColumnsToFit();
          
      }
  };

      this.columnDefs = [        
        {headerName: "Sample", field: "sample"},
        {headerName: "Wheat", field: "wheat"},
        {headerName: "Durum", field: "durum"},
        {headerName: "Feed Barley", field: "feed"},
        {headerName: "Malt Barley", field: "malt"},
        {headerName: "Canola", field: "canola"},
        {headerName: "Chickpeas", field: "chickpeas"},
        {headerName: "Faba Beans", field: "faba"},
        {headerName: "Lupins", field: "lupin"},
        {headerName: "Cotton", field: "cotton"},
        {headerName: "Cotton Seed", field: "cottonseed"},
        {headerName: "Sorghum ", field: "sorghum"},
        {headerName: "Mungbeans ", field: "mungbeans"}        
    ];

    this.defaultColDef = { editable: false };
    this.getRowNodeId = function(data) {
      return data.id;
    };
  };

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  
    selectAllRows() {
        this.gridOptions.api.selectAll();
    }

  ngOnInit() {
  }

}
