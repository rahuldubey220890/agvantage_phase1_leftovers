import {Component, OnInit, ViewChild, NgZone, HostListener} from '@angular/core';
import {Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {FormGroup, FormBuilder, Validators, FormControl, NgForm} from '@angular/forms';
import {UserRoleService} from '../user-role.service';
import {Broadcaster} from '../broadcaster';
import {serviceUrl} from "../app.module";
declare var google;
declare var $: any;

@Component({
  selector: 'app-contacts-and-favorite',
  templateUrl: './contacts-and-favorite.component.html',
  styleUrls: ['./contacts-and-favorite.component.scss']

})

export class ContactsAndFavoriteComponent implements OnInit {

    contacts: any;
    showAlert;
    @ViewChild('addContactForm') public addContactForm;
    con: any;
    sporpartner: any;
    othname1: any;
    othname2: any;
    trdnm: any;
    cmpn: any;
    hmph: any;
    offph: any;
    hmfax: any;
    offfax: any;
    mob: any;
    prtnrmob: any;
    othmob1: any;
    othmob2: any;
    email: any;
    othnmemail1: any;
    othnmemail2: any;
    spemail: any;
    add1: any;
    add2: any;
    exfrmloc: any;
    uhf: any;
    ngr: any;
    grncrponlnreg: any;
    grnflwonlnreg: any;
    abn: any;
    bsb: any;
    bnkacctno: any;
    USER; userStatusRes; signupRes; checking; check;
    farmingRegions: Array<Object>;
    farmRegions: any;
    NGRStatus: string;
    active;
    currentUser: any;
    titleList;
    commoditiesFav:any;
    sameAdd;
    diffAdd;
    disableSubmit = true;
    form: FormGroup;
    isUser = true;
    
    @ViewChild('noBDModal') noBDModal;
    @ViewChild('contactModal') contactModal;
    @ViewChild('userform') public userform;
    fromcontact = "fromcontact";
    public options = { types: ['address'], componentRestrictions: { country: 'AUS' } };
    EntityName; NGR;
    constructor(private _ngZone: NgZone, private broadCaster:Broadcaster, private router: Router, private http: Http,
        private role: UserRoleService,
        private spinnerService: Ng4LoadingSpinnerService,
        private formBuilder: FormBuilder) {
        this.contacts = {};
        this.currentUser = this.role.getUser();
        console.log("--- current user: ", this.currentUser);
        this.USER = {};
        this.active = {};
        this.USER = { U_TYPE: "", U_Farm_Region: "" };
        this.userStatusRes = {};
        this.signupRes = {};
        this.checking = false;
        this.check = false;
        this.titleList = [{ title: 'Mr' }, { title: 'Mrs' }, { title: 'Other' }];
        this.EntityName = this.currentUser.USER.U_FNAME;
        this.NGR = this.currentUser.USER.U_NGR;
    }

    ngOnInit() {
      this.form = this.formBuilder.group({
        USER:this.formBuilder.group({
          U_TELEPHONE: [null, Validators.required]
        })  
      })
        this.getRegion();
        this.getCommodities();
        $('.selectpicker').selectpicker({
            style: 'btn-default',
            width: "100%",
            size: 4
        });
        $('.selectpicker1').selectpicker({
            style: 'btn-default',
            width: "100%",
            size: 4
        });
        this.sameAdd=true;
        this.USER = this.role.getUser().USER;
        console.log("--- role", this.role.getUser().USER);
    }

    radioDiffAddChange(e){
        console.log('radio diff add--', e);
        this.USER = {};
        // console.log('--- on change user',this.USER);
    }
    radioSameAddChange(e){
        console.log(' radio same add--', e);
        this.USER = this.role.getUser().USER;
        // console.log('--- on change user',this.USER);
    }
  
  place;
  getAddress(place: Object) {
    console.log("Address", place);
    this.place = place;
  }
  telephoneRequired;mobileRequired;

  keyPress(event: any) {

    console.log("keypressed...");
    const pattern = /[0-9\+\-\ ]/;

    let inputChar = String.fromCharCode(event.charCode);
    if (event.keyCode != 8 && !pattern.test(inputChar)) {
      event.preventDefault();
    }
    if (this.USER.U_MOBILE && (this.USER.U_MOBILE.length == 4 || this.USER.U_MOBILE.length == 8)) {
      this.mobileRequired = this.isFieldValid(this.form.get(this.USER.U_MOBILE));
      this.USER.U_MOBILE = this.USER.U_MOBILE + '-';
    }
    if (this.USER.U_TELEPHONE && (this.USER.U_TELEPHONE.length == 2 || this.USER.U_TELEPHONE.length == 7)) {
      console.log("",this.form.get(this.USER.U_TELEPHONE));
      this.telephoneRequired = this.isFieldValid(this.form.get(this.USER.U_TELEPHONE));
      this.USER.U_TELEPHONE = this.USER.U_TELEPHONE + '-';      
    }
  }
 
  isFieldValid(field) {
    Object.keys(this.form.controls).forEach(field => {  //{2}
      const control = this.form.get(field);  
      console.log("--- control: ", control);           //{3}
      if (control instanceof FormControl) { 
        console.log("--- FormControl: ", control);            //{4}
        control.markAsTouched({ onlySelf: true });
      }
       else if (control instanceof FormGroup) {
        console.log("--- FormGroup: ", control);  
        
        control.markAsTouched({ onlySelf: true });
        return true       //{5}
        // this.isFieldValid(control);            //{6}
      }
    });
    // console.log("isFieldValid...", field);
  
  }

  @ViewChild('viewContactModal') public viewContactModal;
  openViewFavorites() {
    this.getContactCommodityByUser();
    this.getContactFavByUser();
    this.viewContactModal.show();
  }
  closeModal() {
    this.contactModal.hide();
  }

  closeViewContact() {
    this.viewContactModal.hide();
  }
  //   @ViewChild('tab') tab: TabDirective;
  tabs = [
    {title: 'Dynamic Title 1', content: 'Dynamic content 1', active: true, disabled: false},
    {title: 'Dynamic Title 2', content: 'Dynamic content 2', active: false, disabled: true},
    {title: 'Dynamic Title 2', content: 'Dynamic content 3', active: false, disabled: true},
    {title: 'Dynamic Title 2', content: 'Dynamic content 4', active: false, disabled: true},
    {title: 'Dynamic Title 2', content: 'Dynamic content 5', active: false, disabled: true},
    {title: 'Dynamic Title 2', content: 'Dynamic content 6', active: false, disabled: true}
  ];

  selectNextTab(index) {
    this.tabs[index].active = true;
    this.tabs[index].disabled = false;
  }

  selectBackTab(index) {
    this.tabs[index].active = true;
    // this.tabs[index].disabled = false;        
  }

  gotoHome() {
    this._ngZone.run(() => this.router.navigate(['dashboard']));
  }

  commodities
  getCommodities() {
    let url = serviceUrl + "AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(url).subscribe((data) => {
      this.commodities = data.json().CommodityCollection.Commodity;
      this.commoditiesFav = data.json().CommodityCollection.Commodity;

      // this.selectedCommodity1=this.commodities[0].C_NAME;
      console.log(this.commodities);
    })
  }

  contactCommodities: any = [];
  getContactCommodityByUser() {
    let url = serviceUrl + "AgvContactGetCommSB/AgvGetContactComPS/api/v1/getContComm?U_ID=" + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      this.contactCommodities = data.json().CommoditiesList ? data.json().CommoditiesList.Commodity : [];
      this.contactCommodities.forEach((val) => {
        if (val.Status == "false") {
          val.Status = false;
        } else {
          val.Status = true;
        }
      })
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      console.log(this.commodities);
    })
  }

  setFav($event, status, contactComm) {

    let favObj = {"commodity_id": contactComm.CommID, "status": status, user_id: this.currentUser.USER.U_ID};
    let url = serviceUrl + "SetFavourites/SetFavourite/api/v1/setFavouriteById";
    this.notifyMsg = undefined;
    this.http.post(url, favObj).subscribe((data) => {
      console.log(data);
      this.notifyMsg = data.json().response;
    },
      (err) => {

      });

  }
  setComm($event, status, contactComm) {

    let favObj = {"commodity_id": contactComm.CommID, "status": status, user_id: this.currentUser.USER.U_ID};
    let url = serviceUrl + "SetFavourites/SetFavourite/api/v1/setFavouriteById";
    this.http.post(url, favObj).subscribe((data) => {
      console.log(data);
    },
      (err) => {

      });

  }
  contactFavs: any = [];
  getContactFavByUser() {
    let url = serviceUrl + "AgvContactGetFavSB/AgvContactGetFavPS/api/v1/getContactFav?uid=" + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {

      this.contactFavs = data.json().CommoditiesList ? data.json().CommoditiesList.Commodityfavs : [];
      this.contactFavs.forEach((val) => {
        if (val.Status == "false") {
          val.Status = false;
        } else {
          val.Status = true;
        }
      })
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      console.log(this.commodities);
    })
  }
  notifyMsg: string;
  emailNotify;
  smsNotify;

  getNotificationById() {
    let url = serviceUrl + "GetNotificationPreferenceById/GetNotificationPreferenceById/api/v1/getNotificationPreferenceById?user_id=" + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((succ) => {
      let res = succ.json().preferences ? succ.json().preferences : null;
      this.emailNotify = res.EMAIL;
      this.smsNotify = res.SMS;
    })
  }

  setNoitifcation($event, status, notifyFor) {
    this.notifyMsg = undefined;

    if (notifyFor == 'email') {
      let url = serviceUrl + "SetNotificationPreferences/SetNotificationPreferencePS/api/v1/setNotificationPreferenceById";
      let obj = {
        "user_id": this.currentUser.USER.U_ID,
        "preference": "EMAIL",
        "preferenceStatus": status
      };
      this.http.post(url, obj).subscribe((success) => {
        console.log(success.json());
        this.notifyMsg = success.json().response ? success.json().response : "Some error occurred";
      },
        (err) => {
          console.log(err.json());
          this.notifyMsg = "Server related errors.We will fix it soon.";

        }
      );

    }
    if (notifyFor == 'sms') {
      let url = serviceUrl + "SetNotificationPreferences/SetNotificationPreferencePS/api/v1/setNotificationPreferenceById";
      let obj = {
        "user_id": this.currentUser.USER.U_ID,
        "preference": "SMS",
        "preferenceStatus": status
      };
      this.http.post(url, obj).subscribe((success) => {
        console.log(success.json());
        this.notifyMsg = success.json().response ? success.json().response : "Some error occurred";
      },
        (err) => {
          console.log(err.json());
          this.notifyMsg = "Server related errors.We will fix it soon.";

        }
      );


    }
  }

  getRegion() {
    let url = serviceUrl + 'AgvGetRegionSB/AGVGetRegionPS/api/v1/getRegion?getRegion=true';
    // console.log();
    this.http.get(url).subscribe(data => {
      // Read the result field from the JSON response.
      console.log(data.json().getRegion);
      this.farmRegions = data.json().getRegion;
    });
  }
  commodityArrayProduced: any = [];
  commodotyFavoriteArray: any = [];
  produced($event, status, item) {
    if (status) {
      this.commodityArrayProduced.push({"c_id": item.C_ID, "status": status})
    } else {
      var index;
      this.commodityArrayProduced.forEach((val, key) => {
        if (val.C_ID === item.C_ID) {
          index = key;

        }
      });
      this.commodityArrayProduced.splice(index, 1);
    }
    console.log(this.commodityArrayProduced);
  }
  favorites($event, status, item) {
    if (status) {
      this.commodotyFavoriteArray.push({"c_id": item.C_ID, "status": status})
    } else {
      var index;
      this.commodotyFavoriteArray.forEach((val, key) => {
        if (val.C_ID === item.C_ID) {
          index = key;

        }
      });
      this.commodotyFavoriteArray.splice(index, 1);
    }
    console.log(this.commodotyFavoriteArray);
  }
  selectedRegion() {
    console.log('...selected region', this.USER.U_Farm_Region);
  }

  onFilterChange(event: any) {
    console.log("event",event);
    if (event.target.checked) {
      this.disableSubmit= false;
      console.log("--disableSubmit",this.disableSubmit);
    }
    else {
      this.disableSubmit= true;
      console.log("--disableSubmit",this.disableSubmit);
    }    
  }
  
  postContact() {

    var contactDetail = {
      "entity_id": this.currentUser.USER.U_ID,
      "ngr": this.USER.U_NGR,
      "first_name": this.USER.U_FNAME,
      "last_name": this.USER.U_LNAME,
      "role_title": this.USER.U_TYPE,
      "account_mgr": this.USER.U_AMNAME,
      "office_ph": this.USER.U_TELEPHONE,
      "office_fax": this.USER.off_fax,
      "home_ph": this.USER.hm_phone,
      "home_fax": this.USER.hm_fax,
      "work_mobile": this.USER.U_MOBILE,
      "personal_mobile": this.USER.prsnl_mob,
      "work_email": this.USER.work_email,
      "address": this.USER.U_A_STREET,
      "city": this.USER.U_A_CITY,
      "state": this.USER.U_A_STATE,
      "country": this.USER.U_A_COUNTRY,
      "postcode": this.USER.U_A_POSTCODE,
      "region": this.USER.U_Farm_Region,
      "UHF": this.USER.UHF,
      "add_title": this.contacts.c_title,
      "partner_name": this.contacts.c_sporpartner,
      "spouse_email": this.contacts.c_spemail,
      "add_homephone": this.contacts.c_hmph,
      "add_homefax": this.contacts.c_hmfax,
      "add_partnermobile": this.contacts.c_mob,
      "add_notes": this.contacts.c_notes
    }

    let url = serviceUrl + "AgvEntityAddContactSB/AgvEntityContactPS/api/v1/addcontact";
    this.contactModal.hide();
    this.spinnerService.show();
    this.http.post(url, contactDetail).subscribe(
      (data) => {
        this.addContactForm.reset();
        this.spinnerService.hide();
        this.broadCaster.broadcast('Save Event Contact', "Saved Succesfully");
      },
      (err) => {
        this.spinnerService.hide();
        console.log(err);
      }
    );
  }

  U_FINDREGN; ob;
  openContact() {
    // this.getNotificationById();
    this.contactModal.show();
    
  }

  setCity(event) {

    this._ngZone.run(() => this.USER.U_A_CITY = event)

  }

  setSelectedRegion() {
    // console.log('...selected region popup', this.USER.U_FINDREGN);
    // this.USER.U_Farm_Region = this.U_FINDREGN;
    //get lat long
    // let ob = this.farmRegions[this.U_FINDREGN - 1];
    // for(let i=0;i<this.farmRegions.length;i++){
    //     // console.log('--- farm ',this.farmRegions[i]);
    //     if(this.U_FINDREGN == this.farmRegions[i].RegionID){
    //         console.log('--- farm',this.farmRegions[i]);
    //         this.ob = this.farmRegions[i]
    //     }
    // }
    this.farmRegions.forEach((farm) => {
      if (this.U_FINDREGN == farm.RegionID) {
        this.ob = farm;
      }
    })
    console.log('...selected region popup', this.ob);
    this.USER.U_Farm_Region = this.ob.RegionName;
    console.log('...selected region popup', this.USER.U_Farm_Region);
    let coords = this.ob.RegionCoordinates.split(",");
    let currentPosition = new google.maps.LatLng(coords[0], coords[1]);//lat,long
    var markers = [];
    // Clear out the old markers.
    markers.forEach(function(marker) {
      marker.setMap(null);
    });
    markers = [];
    var originIcon = 'https://chart.googleapis.com/chart?' +
      'chst=d_map_pin_letter&chld=O|FFFF00|000000';
    var icon = {
      url: originIcon,
      size: new google.maps.Size(71, 71),
      origin: new google.maps.Point(0, 0),
      anchor: new google.maps.Point(17, 34),
      scaledSize: new google.maps.Size(25, 25)
    };

    var map = new google.maps.Map(document.getElementById('map'), {
      center: currentPosition,
      zoom: 13,
      mapTypeId: 'roadmap'
    });

    // Create a marker for each place.
    markers.push(new google.maps.Marker({
      map: map,
      icon: icon,
      position: currentPosition
    }));
  }




  ngrlist = [];
  selectedNGR(value) {
    this.USER.U_NGR = value;

    console.log("selected ngr", this.USER.NGRDDL);
    for (let i = 0; i < this.ngrlist.length; i++) {
      if (this.ngrlist[i].NGR == this.USER.U_NGR) {
        // console.log(this.ngrlist[i].NGRUserList.Users.User);
        this.USER.U_TITLE = this.ngrlist[i].Title;
        this.USER.U_FNAME = this.ngrlist[i].Fname;
        this.USER.U_LNAME = this.ngrlist[i].Lname;
        this.USER.U_USERNAME = this.ngrlist[i].Username;
        this.USER.U_MOBILE = this.ngrlist[i].Mobile;
        this.USER.U_TELEPHONE = this.ngrlist[i].Telephone;
        this.USER.U_A_STREET = this.ngrlist[i].A_Street;
        this.USER.U_A_CITY = this.ngrlist[i].A_City;
        this.USER.U_A_STATE = this.ngrlist[i].A_State;
        this.USER.U_A_COUNTRY = this.ngrlist[i].A_Country;
        this.USER.U_A_POSTCODE = this.ngrlist[i].A_Postcode;
      }
    }
  }
  hideValid;
  // checkUserStatus() {
  //   console.log(this.userform);
  //   let emailValid = this.userform.form.controls.email._status;
  //   this.checking = true;
  //   console.log("email check", emailValid );
  //   if (this.USER.work_email != undefined && this.USER.work_email.length > 0 && emailValid === "VALID") {
  //     this.checking = true;
  //     let url = serviceUrl + "AccountGatewayCheckUserStatus/AccountGatewayUserStatusPS/checkUserStatus?USERNAME=" + this.USER.work_email;
  //     this.http.get(url).subscribe(data => {
  //       // Read the result field from the JSON response.
  //       this.checking = false;
  //       this.hideValid = true;
  //       console.log(data.json());
  //       this.userStatusRes = data.json();
  //     });
  //   } else if (emailValid === "INVALID") {
  //     this.checking = false;
  //     this.hideValid = false;
  //   }
  // }
  checkUserStatus() {
    console.log(this.userform);
    let emailValid = this.userform.form.controls.email.status;
    console.log("-- contact email check", emailValid );
    this.checking = true;
    if (this.USER.work_email != undefined && this.USER.work_email.length > 0 && emailValid === "VALID") {
        this.checking = true;
        let url = serviceUrl + "AccountGatewayCheckUserStatus/AccountGatewayUserStatusPS/checkUserStatus?USERNAME=" + this.USER.work_email;
        this.http.get(url).subscribe(data => {
            // Read the result field from the JSON response.
            this.checking = false;
            this.hideValid = true;
            console.log(data.json());
            this.userStatusRes = data.json();
        },(err)=>{
            alert("Server error occured");
            this.checking = false;
        });
    } else if (emailValid === "INVALID") {
        this.checking = false;
        this.hideValid = false;
    }
}


  checkNgr() {
    this.spinnerService.show();
    let url = serviceUrl + "AccountGatewayCheckNGRStatusSB/ProxyService/AccountGatewayNGRPS/api/v1/checkNGRStatus?NGR=" + this.USER.U_NGR;
    this.http.get(url).subscribe(data => {
      // Read the result field from the JSON response.
      this.spinnerService.hide();
      console.log(data.json());
      var ngres = data.json();
      this.NGRStatus = ngres.Status;
      if (this.NGRStatus == "NOT EXISTS") {
        this.USER.U_TITLE = '',
          this.USER.U_FNAME = '',
          this.USER.U_LNAME = '',
          this.USER.U_USERNAME = '',
          this.USER.U_MOBILE = '',
          this.USER.U_TELEPHONE = '',
          this.USER.U_A_STREET = '',
          this.USER.U_A_CITY = '',
          this.USER.U_A_STATE = '',
          this.USER.U_A_COUNTRY = '',
          this.USER.U_A_POSTCODE = ''
      }
    });

  }
  private _markFormPristine(form: FormGroup | NgForm): void {
    Object.keys(form.controls).forEach(control => {
      form.controls[control].markAsDirty();
    });
  }

  status: {isopen: boolean} = {isopen: false};

  toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  change(value: boolean): void {
    this.status.isopen = value;
  }



}
