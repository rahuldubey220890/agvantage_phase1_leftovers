import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactsAndFavoriteComponent } from './contacts-and-favorite.component';

describe('ContactsAndFavoriteComponent', () => {
  let component: ContactsAndFavoriteComponent;
  let fixture: ComponentFixture<ContactsAndFavoriteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactsAndFavoriteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactsAndFavoriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
