import { Component, OnInit } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: 'app-edit-delete-renderer',
  templateUrl: './edit-delete-renderer.component.html',
  styleUrls: ['./edit-delete-renderer.component.scss']
})
export class EditDeleteRendererComponent implements ICellRendererAngularComp {

  
  public params: any;

  agInit(params: any): void {
      this.params = params;
  }

  public callEdit() {
      console.log('--- context:',this.params.context.componentParent);
      this.params.context.componentParent.methodFromParent(this.params.node);
  }
  public callDelete(){
    this.params.context.componentParent.methodFromParentDelete(this.params.node);
  }

  refresh(): boolean {
      return false;
  }

}
