import {Injectable} from '@angular/core';

@Injectable()
export class UserRoleService {
  private role: string;
  private user: any;
  constructor() {}

  setRole(role) {
    this.role = role;
    localStorage.setItem('role', JSON.stringify(role));
    
  }
  getRole(): string {
    if (this.role === undefined) {
      this.role = JSON.parse(localStorage.getItem('role'));
    }
    return this.role;
  }
  getUser() {
    if (this.user === undefined) {
      this.user = JSON.parse(localStorage.getItem('userDetails'));
    }
    return this.user;
  }
  setUser(user) {
    this.user = user;
    localStorage.setItem('userDetails', JSON.stringify(user));
  }
  isLoggedIn():boolean{
   if(this.user!=undefined || this.user!=null || this.getUser()!=null){
       
   return true;
   }    else{
       
   return false;
   }
  
  }

}
