import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesAndPurchaseComponent } from './sales-and-purchase.component';

describe('SalesAndPurchaseComponent', () => {
  let component: SalesAndPurchaseComponent;
  let fixture: ComponentFixture<SalesAndPurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesAndPurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesAndPurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
