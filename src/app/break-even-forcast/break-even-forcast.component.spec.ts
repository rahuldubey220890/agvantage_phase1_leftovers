import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakEvenForcastComponent } from './break-even-forcast.component';

describe('BreakEvenForcastComponent', () => {
  let component: BreakEvenForcastComponent;
  let fixture: ComponentFixture<BreakEvenForcastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreakEvenForcastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakEvenForcastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
