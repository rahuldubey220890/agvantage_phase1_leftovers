import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionCostGridComponent } from './production-cost-grid.component';

describe('ProductionCostGridComponent', () => {
  let component: ProductionCostGridComponent;
  let fixture: ComponentFixture<ProductionCostGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionCostGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionCostGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
