import {Component, OnInit, ViewChild} from '@angular/core';
import {Http, Headers, RequestOptions} from '@angular/http';
import {Ng4LoadingSpinnerService} from 'ng4-loading-spinner';
import {Router} from '@angular/router';
import {serviceUrl} from "../app.module";

declare var $: any;

@Component({
	selector: 'app-resetpswrd',
	templateUrl: './resetpswrd.component.html',
	styleUrls: ['./resetpswrd.component.scss']
})
export class ResetpswrdComponent implements OnInit {
	reasons: any;
	cnfrmpswrd: any;
	newpswrd: any;
	url: string;
	headers: Headers;
	options: RequestOptions;
	USER: any;
	showOutcome: string;
	outcomeMsg: boolean = false;


	constructor(private http: Http, public spinnerService: Ng4LoadingSpinnerService, private router: Router) {
		this.USER = {};
		this.reasons = [
			{img: "assets/images/dashboardMenu/comprehensive.png", why: "Comprehensive Farm Marketing Plans"},
			{img: "assets/images/dashboardMenu/marketing.jpg", why: "Marketing Support"},
			{img: "assets/images/dashboardMenu/automation.jpg", why: "Automation, Price Alerts & Reminders"},
			{img: "assets/images/dashboardMenu/alwaysKnow.jpg", why: "Always know their position"},
			{img: "assets/images/dashboardMenu/recordHistory.jpg", why: "Records, History, & Reports - in one place "},
			{img: "assets/images/dashboardMenu/higlySecure.jpg", why: "Highly Secure "}];
	}

	@ViewChild('resetform') public resetform;

	showMsg() {
		this.headers = new Headers({
			'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
			'Accept': 'application/json'
		});
		this.options = new RequestOptions({headers: this.headers});
		this.url = serviceUrl + "AccountGatewayResetPassword/AccountGatewayPasswordResetPS/api/v1/resetPassword";
		this.spinnerService.show();
		this.http.post(this.url, $.param(this.USER), this.options).subscribe((data) => {
			console.log(data.json());
			this.spinnerService.hide();
			this.showOutcome = data.json().outcome;
			this.outcomeMsg = true;
			this.USER = {UserName: "", OldPassword: "", NewPassword: ""};
			console.log(this.resetform);
			this.resetform.reset();

		})

	}

	gotoLogin() {
		this.router.navigate(['login']);
	}

	ngOnInit() {
	}

}
