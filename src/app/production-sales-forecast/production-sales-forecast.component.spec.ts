import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductionSalesForecastComponent } from './production-sales-forecast.component';

describe('ProductionSalesForecastComponent', () => {
  let component: ProductionSalesForecastComponent;
  let fixture: ComponentFixture<ProductionSalesForecastComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductionSalesForecastComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductionSalesForecastComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
