import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommodityMovementGridComponent } from './commodity-movement-grid.component';

describe('CommodityMovementGridComponent', () => {
  let component: CommodityMovementGridComponent;
  let fixture: ComponentFixture<CommodityMovementGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommodityMovementGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommodityMovementGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
