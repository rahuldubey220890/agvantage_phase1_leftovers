import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { serviceUrl } from '../app.module';
import {Http, Headers, RequestOptions} from '@angular/http';
import { EditDeleteRendererComponent } from '../edit-delete-renderer/edit-delete-renderer.component';
import { Broadcaster } from '../broadcaster';
import { UserRoleService } from '../user-role.service';

@Component({
  selector: 'app-commodity-movement-grid',
  templateUrl: './commodity-movement-grid.component.html',
  styleUrls: ['./commodity-movement-grid.component.scss']
})
export class CommodityMovementGridComponent implements OnInit {

  context: any;
  frameworkComponents: { 'editdeletecellrenderer': typeof EditDeleteRendererComponent; };
  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;

  columnDefs;
  currentUser;
  rowData;
  defaultColDef;
  getRowNodeId;
  saveMessage: any = false;

  constructor(private http : Http, private broadCaster : Broadcaster, private role: UserRoleService,) {
    this.currentUser = this.role.getUser();
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        this.gridOptions.api.sizeColumnsToFit();
      }
    };
    this.broadCaster.on<any>('Save Event Commodity Movement')
    .subscribe(res => {
        console.log(res);
        this.gridOptions.api.showLoadingOverlay()
         this.getCommodityMovementRecord();
       
        this.saveMessage = "Commodity Movement Record Saved Successfully";
          this.getCommodityMovementRecord();
        setTimeout(()=>{
          this.saveMessage = false;
        },6000);
    });
    this.frameworkComponents = {
      'editdeletecellrenderer': EditDeleteRendererComponent
    }
    this.context = { componentParent: this };
    this.columnDefs = [
     /*  {
        headerName: "Actions", pinned: 'left', field: "actions", width: 180,
        cellRenderer: 'editdeletecellrenderer', cellEditor: 'agRichSelect', editable: true
      }, */
      { headerName: "Date", field: "CollectionDate", pinned: 'left' },
      { headerName: "Storage Name", field: "StorageName", pinned: 'left' },
      { headerName: "Load", field: "StorageType", pinned: 'left' },
      { headerName: "Contract Number", valueGetter: params => params.data.TruckLoads.TruckLoad[0].ContractNo, pinned: 'left' },
      { headerName: "Buyer", valueGetter: params => params.data.TruckLoads.TruckLoad[0].Buyer, pinned: 'left' },
      { headerName: "Commodity Collected",valueGetter: params => params.data.TruckLoads.TruckLoad[0].CommodityCollected, pinned: 'left' },
      { headerName: "Grade", valueGetter: params => params.data.TruckLoads.TruckLoad[0].Grade,  pinned: 'left' },
      { headerName: "Truck Type", valueGetter: params => params.data.TruckLoads.TruckLoad[0].TruckType, pinned: 'left' },
      { headerName: "Estimated Weight", valueGetter: params => params.data.TruckLoads.TruckLoad[0].EstimatedWeight, pinned: 'left' },
      { headerName: "Delivery Company Name", valueGetter: params => params.data.TruckLoads.TruckLoad[0].DeliveryCompanyName, pinned: 'left' },
      { headerName: "Driver Name", valueGetter: params => params.data.TruckLoads.TruckLoad[0].DeliveryCompanyName,  pinned: 'left' },
      { headerName: "Truck Regn No", valueGetter: params => params.data.TruckLoads.TruckLoad[0].TruckRegNo,  pinned: 'left' }
    ];
    this.rowData = [];
  
    // this.context = { componentParent: this };
    // this.frameworkComponents = {
    //   commodityUpdateCellRendererComponent: CommodityUpdateCellRendererComponent
    // };
  
  
    this.getRowNodeId = function (data) {
      return data.id;
    };
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getCommodityMovementRecord();
  }
  getCommodityMovementRecord(){
    let url = serviceUrl + "CommodityMovementSB/CommodityMovementPS/api/v1/GetCommodityMovementHistory?U_ID="+this.currentUser.USER.U_ID;
    this.gridApi.showLoadingOverlay();
    this.http.get(url).subscribe((data)=>{
      console.log(data.json());
      this.rowData = data.json()?data.json().CommodityMovementHistory:[];
      this.gridApi.hideOverlay();
    })
  }
  ngOnInit() {
  }

  

  // this.broadCaster.on<string>('Save Event')
  //   .subscribe(message => {
  //     this.gridOptions.api.showLoadingOverlay()
  //     this.getGridData();
  //     this.saveMessage=message;
  //   });
  //      this.broadCaster.on<string>('Update Event')
  //   .subscribe(message => {
  //     this.gridOptions.api.showLoadingOverlay()
  //     this.getGridData();
  //     this.saveMessage=message;
  //   });
  
}
