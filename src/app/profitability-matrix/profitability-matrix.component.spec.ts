import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfitabilityMatrixComponent } from './profitability-matrix.component';

describe('ProfitabilityMatrixComponent', () => {
  let component: ProfitabilityMatrixComponent;
  let fixture: ComponentFixture<ProfitabilityMatrixComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfitabilityMatrixComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfitabilityMatrixComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
