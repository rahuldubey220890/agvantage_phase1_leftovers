import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPosDetailsViewComponent } from './sales-pos-details-view.component';

describe('SalesPosDetailsViewComponent', () => {
  let component: SalesPosDetailsViewComponent;
  let fixture: ComponentFixture<SalesPosDetailsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPosDetailsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPosDetailsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
