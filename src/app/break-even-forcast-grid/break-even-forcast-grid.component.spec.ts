import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BreakEvenForcastGridComponent } from './break-even-forcast-grid.component';

describe('BreakEvenForcastGridComponent', () => {
  let component: BreakEvenForcastGridComponent;
  let fixture: ComponentFixture<BreakEvenForcastGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BreakEvenForcastGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BreakEvenForcastGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
