import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import { Http, Headers, RequestOptions } from '@angular/http';
import { CommodityUpdateCellRendererComponent } from '../commodity-update-cell-renderer/commodity-update-cell-renderer.component';
import { UserRoleService } from '../user-role.service';
import { Broadcaster } from '../broadcaster';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {serviceUrl} from "../app.module";

@Component({
  selector: 'app-break-even-forcast-grid',
  templateUrl: './break-even-forcast-grid.component.html',
  styleUrls: ['./break-even-forcast-grid.component.scss']
})
export class BreakEvenForcastGridComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;

  columnDefs;

  rowData;
  defaultColDef;
  getRowNodeId;
  url: string;
  commodityGrid: any;
  commodity: any;
  context;
  frameworkComponents;
  currentUser; brkevenForcastGrid; saveMessage;
  headers: Headers;
  option: RequestOptions;

  options: RequestOptions;
  commodities: any;
  grades: any;
  selectedCommodity1: any;

  submitCommodity: string;
  grade: any;
  misc: any;
  roi: any;


  ngOnInit() {
    this.currentUser = this.role.getUser();
    this.getGridData();
    this.getCommodities();
    this.getCropyearList();
  }

  constructor(private cdRef: ChangeDetectorRef, private http: Http, private role: UserRoleService,
    private broadCaster: Broadcaster, public spinnerService: Ng4LoadingSpinnerService) {

    // we pass an empty gridOptions in, so we can grab the api out
    this.commodity = {};
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        this.gridOptions.api.sizeColumnsToFit();
      }
    };

    this.broadCaster.on<string>('Save Event')
      .subscribe(message => {
      	this.gridOptions.api.showLoadingOverlay()
        this.getGridData();
        this.saveMessage=message;
      });
         this.broadCaster.on<string>('Update Event')
      .subscribe(message => {
        this.gridOptions.api.showLoadingOverlay()
        this.getGridData();
        this.saveMessage=message;
      });
        


    this.columnDefs = [
      {
        headerName: "Actions", pinned: 'left', field: "actions", width: 130,
        cellRenderer: 'commodityUpdateCellRendererComponent', cellEditor: 'agRichSelect', editable: true
      },
      { headerName: "Total Production(in tonnes)", field: "TotalProduction", pinned: 'left' },
      { headerName: "Marketable Production", field: "MarketableProduction", pinned: 'left' },
      { headerName: "Total Cost Per Hectare (in $)", field: "TotalGrowingCostsPerHectare", pinned: 'left' },
      { headerName: "Total Cost Per Crop (in $)", field: "TotalGrowingCostsPerCrop", pinned: 'left' },
      { headerName: "Break Even Per Unit (Tonne/Bale)", field: "GrowingBreakevenPerUnit", pinned: 'left' },
      { headerName: "Target ROI (in %)", field: "TargetROI", pinned: 'left' },
      { headerName: "Commodity Name", field: "commodity_name", pinned: 'left' },
      { headerName: "Crop Year", field: "crop_year", pinned: 'left' },
    ];

    this.context = { componentParent: this };
    this.frameworkComponents = {
      commodityUpdateCellRendererComponent: CommodityUpdateCellRendererComponent
    };


    this.getRowNodeId = function (data) {
      return data.id;
    };

  } // constructor

  getGridData() {
    let uid = this.currentUser.USER.U_ID;
    this.url = serviceUrl+"AgvantageGetSummaryBreakevenSB/AgvantageGetSummaryBreakevenRestPS/api/v1/GetSummaryBreakeven?user_id=" + uid;
    this.http.get(this.url).subscribe((data) => {
      console.log('brk', data.json());
        if(data.json()!=null){
           this.brkevenForcastGrid = data.json()?data.json().BreakevenResponse : [];
      this.rowData = this.brkevenForcastGrid;  
        }else{
        this.rowData=[];    
        }
     
    })
  }

  @ViewChild('noBDModal') noBDModal;
  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  getCommodities() {
    this.url = serviceUrl+"AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(this.url).subscribe((data) => {
      this.commodities = data.json().CommodityCollection.Commodity;
      //this.selectedCommodity1=this.commodities[0].C_NAME;
      console.log('response : ', this.commodities);
    })
  }


  callGrades() {
    this.getCommodityGrade(this.commodity.SCC_C_ID);
  }

  getCommodityGrade(id) {
    // this.spinnerService.show();
    this.url = serviceUrl+"AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
    this.http.get(this.url).subscribe((data) => {
      // this.spinnerService.hide();
      this.grades = data.json().gradeCollection.grade;
    })
  }

  cropYearList;
  getCropyearList() {
    let url = serviceUrl+'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
    this.http.get(url).subscribe((data) => {
      // console.log('crp year data:',data.json().CropYear);
      this.cropYearList = data.json().CropYear;
      // this.selectedCommodity1=this.commodities[0].C_NAME;
      // console.log(this.commodities);
    })
  }

  @ViewChild('brkEvenForcastEditModel') brkEvenForcastEditModel;
  @ViewChild('addBrkevenfrcstForm') addBrkevenfrcstForm;
  customFieldArray;

  methodFromParent(rowObj) {
    //this.addBrkevenfrcstForm.reset();  
    this.commodity = rowObj.data;
    this.getCommodityGrade(this.commodity.SCC_C_ID);
    this.customFieldArray = rowObj.data.CustomFields==null?'': rowObj.data.CustomFields.otherCost; 
    
    this.brkEvenForcastEditModel.show();
  }

  
  createNewCustomFields() {
    let obj = { label: '', value: '' };
    let newObj = Object.assign({}, obj);
    
    if (this.customFieldArray.length < 11) {
      newObj.label = '';
      newObj.value = '';
      this.customFieldArray.push(newObj);
    }
  }

  removeNewCustomFields(obj, index) {
    this.customFieldArray.splice(index, 1);
  }

  closeModal(){
    this.brkEvenForcastEditModel.hide();
  }

  submitBrkevenForcast;
  updateBrakevenForcast() {
    let params = {
      "BreakEvenForecast" : {
        "Forecast_ID" : this.commodity.Forecast_ID,
        "SCC_C_ID" : this.commodity.SCC_C_ID,
        "SCC_GRD_ID" : this.commodity.SCC_GRD_ID,
        "SCC_CROP_YR" : this.commodity.SCC_CROP_YR,
        "SCC_AREA_H" : this.commodity.SCC_AREA_H,
        "SCC_YIELD_PER_H" : this.commodity.SCC_YIELD_PER_H,
        "SCC_SHARE_FARMER" : this.commodity.SCC_SHARE_FARMER,
        "SCC_AVL_STRG_PER" : this.commodity.SCC_AVL_STRG_PER,
        "SCC_OLD_CRP_LFT" : this.commodity.SCC_OLD_CRP_LFT,
        "SCC_LBR" : this.commodity.SCC_LBR,
        "SCC_ROI" : this.commodity.SCC_ROI,
        "SCC_REP_MAINT" : this.commodity.SCC_REP_MAINT,
        "U_ID" : this.currentUser.USER.U_ID,
        "CustomFields" : {
          "otherCost" : this.customFieldArray
        }
      }
    }

    console.log('params', params);

    let URL = serviceUrl+'AgvAddBreakEvenForCast/AgvAddBreakEvenForeCast/api/v1/addForeCast';
    // this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.option = new RequestOptions({ headers: this.headers });
    this.http.post(URL, params, new RequestOptions({ headers: this.headers })).subscribe((data) => {
      console.log('--- update : ', data.json());
      this.addBrkevenfrcstForm.reset();
      this.broadCaster.broadcast('Update Event', 'Updated Successfully.');
      $(".modal-content").scrollTop(0);
      this.brkEvenForcastEditModel.hide();
      this.getGridData();
    })
  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }

  actionRenderer() {
    let actiontemplate = `
<a >
        <span class="glyphicon glyphicon-edit" style="margin-right:12px;margin-left:2px" aria-hidden="true"></span>
      </a>  
  `;
    return actiontemplate;
  }


}
