import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProfitMatrixGridsComponent } from './view-profit-matrix-grids.component';

describe('ViewProfitMatrixGridsComponent', () => {
  let component: ViewProfitMatrixGridsComponent;
  let fixture: ComponentFixture<ViewProfitMatrixGridsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewProfitMatrixGridsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProfitMatrixGridsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
