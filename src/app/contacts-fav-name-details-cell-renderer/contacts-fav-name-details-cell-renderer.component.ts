import { Component, OnInit } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: 'app-contacts-fav-name-details-cell-renderer',
  templateUrl: './contacts-fav-name-details-cell-renderer.component.html',
  styleUrls: ['./contacts-fav-name-details-cell-renderer.component.scss']
})
export class ContactsFavNameDetailsCellRendererComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    public invokeParentMethod() {
        console.log("con-fav-details-cell",this.params.node);
        this.params.context.componentParent.methodFromParent(this.params.node);
    }

    refresh(): boolean {
        return false;
    }
}
