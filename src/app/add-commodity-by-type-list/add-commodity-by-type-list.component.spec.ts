import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCommodityByTypeListComponent } from './add-commodity-by-type-list.component';

describe('AddCommodityByTypeListComponent', () => {
  let component: AddCommodityByTypeListComponent;
  let fixture: ComponentFixture<AddCommodityByTypeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCommodityByTypeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCommodityByTypeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
