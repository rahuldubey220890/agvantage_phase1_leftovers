import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import {GridOptions} from "ag-grid/main";

@Component({
  selector: 'app-add-commodity-by-type-list',
  templateUrl: './add-commodity-by-type-list.component.html',
  styleUrls: ['./add-commodity-by-type-list.component.scss']
})
export class AddCommodityByTypeListComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
gridColumnApi;

columnDefs;

rowData;
defaultColDef;
getRowNodeId;

constructor(private cdRef: ChangeDetectorRef) { 
  // we pass an empty gridOptions in, so we can grab the api out

  this.gridOptions = <GridOptions>{
   onGridReady: () => {
    //  this.gridOptions.columnApi.autoSizeAllColumns();
    this.gridOptions.api.sizeColumnsToFit();
   }
 };


 this.columnDefs = [
   {
     headerName: "",
     children: [
     { headerName: "", field: "attrs" },
     { headerName: "Actions", field: "actions", cellRenderer: this.actionRenderer, cellEditor: 'agRichSelect', editable: true },
     { headerName: "Commodity Type", field: "commodity" },
     { headerName: "Commodity Name", field: "commodityName" },
     { headerName: "Grade", field: "grade" },
     { headerName: "Crop year", field: "Year"}
   ]
   }
 ];

 this.rowData = [
   {
     attrs: "Wheat",
     commodity: "Crop",
     commodityName: "Wheat",
     grade: "wa1",
     Year: "2017",
   },
   {
     attrs: "Wheat",
     commodity: "Crop",
     commodityName: "Wheat",
     grade: "wa2",
     Year: "2017",
   },
   {
     attrs: "Wheat",
     commodity: "Crop",
     commodityName: "Wheat",
     grade: "wa3",
     Year: "2017",
   },
   {
     attrs: "Wheat",
     commodity: "Crop",
     commodityName: "Wheat",
     grade: "wa4",
     Year: "2017",
   },
   {
     attrs: "Wheat",
     commodity: "Crop",
     commodityName: "Wheat",
     grade: "wa5",
     Year: "2017",
   }
   ];

 this.defaultColDef = { editable: false };

 this.getRowNodeId = function (data) {
   return data.id;
 };
}

ngOnInit() {
}

onGridReady(params) {
 this.gridApi = params.api;
 this.gridColumnApi = params.columnApi;

}

selectAllRows() {
 this.gridOptions.api.selectAll();
}
actionRenderer() {
 let actiontemplate = `
<a href="#">
       <span class="glyphicon glyphicon-edit" style="margin-right:12px;margin-left:2px" aria-hidden="true"></span>
     </a>
<a href="#">
       <span class="glyphicon glyphicon-trash"  aria-hidden="true"></span>
     </a>`;
 return actiontemplate;
}


}
