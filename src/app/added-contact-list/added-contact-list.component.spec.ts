import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddedContactListComponent } from './added-contact-list.component';

describe('AddedContactListComponent', () => {
  let component: AddedContactListComponent;
  let fixture: ComponentFixture<AddedContactListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddedContactListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddedContactListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
