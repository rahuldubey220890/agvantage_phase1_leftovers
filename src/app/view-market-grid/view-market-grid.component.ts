import { Component, OnInit,NgZone } from '@angular/core';
import  {Router} from '@angular/router';


@Component({
  selector: 'app-view-market-grid',
  templateUrl: './view-market-grid.component.html',
  styleUrls: ['./view-market-grid.component.scss']
})
export class ViewMarketGridComponent implements OnInit {

  

  constructor(private router:Router,private _ngZone:NgZone) {

   
   }

  ngOnInit() {
  }

  

gotoHome(){
    this._ngZone.run(() => this.router.navigate(['dashboard']));
    }

   
}
