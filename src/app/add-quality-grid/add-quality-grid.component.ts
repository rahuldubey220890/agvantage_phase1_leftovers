import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import { GridOptions } from "ag-grid/main";

@Component({
  selector: 'app-add-quality-grid',
  templateUrl: './add-quality-grid.component.html',
  styleUrls: ['./add-quality-grid.component.scss']
})
export class AddQualityGridComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  columnDefs;
  rowData;
  defaultColDef;
  getRowNodeId;
  constructor(private cdRef:ChangeDetectorRef) {
    // we pass an empty gridOptions in, so we can grab the api out

    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        this.gridOptions.api.sizeColumnsToFit();
      }
    };


    this.columnDefs = [
      {
        headerName: "",
        children: [
        { headerName: "Actions", field: "actions", cellRenderer: this.actionRenderer, cellEditor: 'agRichSelect', editable: true },
        { headerName: "Quality", field: "quality" },
        { headerName: "Quality Value", field: "qualityvalue" }
        ]
      }
    ];

    this.rowData = [
      {
        id: "aa",
        quality: "Protein % 13.00",
        qualityvalue: "Protein % 13.00"
      },
      {
        id: "aa",
        quality: "Protein % 13.00",
        qualityvalue: "Protein % 13.00"
      },
      {
        id: "aa",
        quality: "Protein % 13.00",
        qualityvalue: "Protein % 13.00"
      },
      {
        id: "aa",
        quality: "Protein % 13.00",
        qualityvalue: "Protein % 13.00"
      },
      {
        id: "aa",
        quality: "Protein % 13.00",
        qualityvalue: "Protein % 13.00"
      }
      

    ];

    this.defaultColDef = { editable: false };

    this.getRowNodeId = function (data) {
      return data.id;
    };
  }

  ngOnInit() {
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }

  actionRenderer() {
    let actiontemplate = `
<a href="#">
          <span class="glyphicon glyphicon-edit" style="margin-right:12px;margin-left:2px" aria-hidden="true"></span>
        </a>
<a href="#">
          <span class="glyphicon glyphicon-trash"  aria-hidden="true"></span>
        </a>
        
    `;
    return actiontemplate;
  }


}
