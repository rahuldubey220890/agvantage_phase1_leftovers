import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddPWByStateListComponent } from './add-pwby-state-list.component';

describe('AddPWByStateListComponent', () => {
  let component: AddPWByStateListComponent;
  let fixture: ComponentFixture<AddPWByStateListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPWByStateListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddPWByStateListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
