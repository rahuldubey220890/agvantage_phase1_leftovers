import { RouterModule, Routes, CanActivate  } from '@angular/router';

// Components
import { DashboardComponent } from './dashboard/dashboard.component';
import { Dashboard2Component } from './dashboard-2/dashboard-2.component';
//import { ColorPrimaryComponent } from './color-primary/color-primary.component';
//import { ColorDangerComponent } from './color-danger/color-danger.component';
//import { ColorSuccessComponent } from './color-success/color-success.component';
//import { ColorWarningComponent } from './color-warning/color-warning.component';
//import { ColorInfoComponent } from './color-info/color-info.component';
//import { ColorBlackComponent } from './color-black/color-black.component';
//import { ColorGrayComponent } from './color-gray/color-gray.component';
//import { UiButtonsComponent } from './ui-buttons/ui-buttons.component';
//import { UiPanelsComponent } from './ui-panels/ui-panels.component';
//import { UiModalsComponent } from './ui-modals/ui-modals.component';
//import { UiPageHeadersComponent } from './ui-page-headers/ui-page-headers.component';
//import { UiConfirmationComponent } from './ui-confirmation/ui-confirmation.component';
//import { UiDropdownMenuComponent } from './ui-dropdown-menu/ui-dropdown-menu.component';
//import { UiMediaObjectsComponent } from './ui-media-objects/ui-media-objects.component';
//import { BtAccordionsComponent } from './bt-accordions/bt-accordions.component';
//import { BtAlertsComponent } from './bt-alerts/bt-alerts.component';
//import { BtButtonDropdownComponent } from './bt-button-dropdown/bt-button-dropdown.component';
//import { BtButtonGroupsComponent } from './bt-button-groups/bt-button-groups.component';
//import { BtButtonsComponent } from './bt-buttons/bt-buttons.component';
//import { BtCarouselComponent } from './bt-carousel/bt-carousel.component';
//import { BtCodeComponent } from './bt-code/bt-code.component';
//import { BtCollapseComponent } from './bt-collapse/bt-collapse.component';
//import { BtDropdownsComponent } from './bt-dropdowns/bt-dropdowns.component';
//import { BtGridComponent } from './bt-grid/bt-grid.component';
//import { BtHelpersComponent } from './bt-helpers/bt-helpers.component';
//import { BtLabelsBadgesComponent } from './bt-labels-badges/bt-labels-badges.component';
//import { BtPaginationComponent } from './bt-pagination/bt-pagination.component';
//import { BtPopoverComponent } from './bt-popover/bt-popover.component';
//import { BtTabsComponent } from './bt-tabs/bt-tabs.component';
//import { BtTooltipsComponent } from './bt-tooltips/bt-tooltips.component';
//import { BtTypographyComponent } from './bt-typography/bt-typography.component';
//import { TableBasicComponent } from './table-basic/table-basic.component';
//import { Table10kRowsComponent } from './table-10k-rows/table-10k-rows.component';
//import { TableInlineEditComponent } from './table-inline-edit/table-inline-edit.component';
//import { TableFilterComponent } from './table-filter/table-filter.component';
//import { ChartColumnBarComponent } from './chart-column-bar/chart-column-bar.component';
//import { ChartLineAreaComponent } from './chart-line-area/chart-line-area.component';
//import { ChartPieChartComponent } from './chart-pie-chart/chart-pie-chart.component';
//import { ChartFunnelComponent } from './chart-funnel/chart-funnel.component';
//import { ChartOthersComponent } from './chart-others/chart-others.component';
//import { Error404Component } from './error-404/error-404.component';
//import { Error500Component } from './error-500/error-500.component';
//import { FormBasicComponent } from './form-basic/form-basic.component';
//import { FormLayoutsComponent } from './form-layouts/form-layouts.component';
//import { FormValidationsComponent } from './form-validations/form-validations.component';
import { PagesPricingComponent } from './pages-pricing/pages-pricing.component';
import { PagesProfileComponent } from './pages-profile/pages-profile.component';
import {LoginComponent} from './login/login.component';
import {SignupComponent} from './signup/signup.component';
import {ForgetpswrdComponent} from './forgetpswrd/forgetpswrd.component';
import {ResetpswrdComponent} from './resetpswrd/resetpswrd.component';
import {ViewStorageGridComponent} from './view-storage-grid/view-storage-grid.component';
import {ViewMarketGridComponent} from './view-market-grid/view-market-grid.component';
import {ViewProductionGridComponent} from './view-production-grid/view-production-grid.component';
import {AppComponent} from './app.component';
import {AddcommodityComponent} from './addcommodity/addcommodity.component';
import {AddCommodityByComponent} from './add-commodity-by/add-commodity-by.component';
import { ContactsAndFavoriteComponent } from './contacts-and-favorite/contacts-and-favorite.component';
import { GoogleMapComponent } from './google-map/google-map.component';
import { AddQualityComponent } from './add-quality/add-quality.component';
import { BreakEvenForcastComponent } from './break-even-forcast/break-even-forcast.component';
import { AccountDetailsGridComponent } from './account-details-grid/account-details-grid.component';
import { FreightCalculatorComponent } from './freight-calculator/freight-calculator.component';
import {OnlyLoggedInUserGuardService} from './only-logged-in-user-guard.service';
import { AboutusComponent } from './aboutus/aboutus.component';
import { TeamComponent } from './team/team.component';
import { ProductsComponent } from './products/products.component';
import { OurServicesComponent } from './our-services/our-services.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { FaqComponent } from './faq/faq.component';
import { ProducerComponent } from './producer/producer.component';
import { SalesAndM2mPositionComponent } from './sales-and-m2m-position/sales-and-m2m-position.component';
import { ContactsFavouritesComponent } from './contacts-favourites/contacts-favourites.component';
import { ProfitabilityMatrixComponent } from './profitability-matrix/profitability-matrix.component';
import { ContractorsOnFarmComponent } from './contractors-on-farm/contractors-on-farm.component';
import { ViewPcbGridComponent } from './view-pcb-grid/view-pcb-grid.component';
import { DocumentsComponent } from './documents/documents.component';
import { SuperadminComponent } from './superadmin/superadmin.component';
import { ProductionSalesForecastComponent } from './production-sales-forecast/production-sales-forecast.component';
import { SalesAndPurchaseComponent } from './sales-and-purchase/sales-and-purchase.component';
import { ViewProfitMatrixGridsComponent } from './view-profit-matrix-grids/view-profit-matrix-grids.component';
import {AddNewFieldsComponent} from './addfields/addfields.component';
// Routes
export const routes: Routes = [
    // { path: 'dashboard', component: DashboardComponent },
    { path: 'dashboard', component: Dashboard2Component, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'viewstoragegrid', component: ViewStorageGridComponent ,canActivate: [OnlyLoggedInUserGuardService]},
    { path: 'viewmarketgrid', component: ViewMarketGridComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'viewproductiongrid', component: ViewProductionGridComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'documentsgrid', component: DocumentsComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'Superadmin', component: SuperadminComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'addcommodity', component: AddcommodityComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'pages-pricing', component: PagesPricingComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'pages-profile', component: PagesProfileComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'login', component: LoginComponent},
    { path: 'forgetpswrd', component: ForgetpswrdComponent },
    { path: 'resetpswrd', component: ResetpswrdComponent,canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'signup', component: SignupComponent},
    { path: 'contacts', component: ContactsAndFavoriteComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'direction', component: GoogleMapComponent , canActivate: [OnlyLoggedInUserGuardService]},
    { path: 'addcommodityby', component: AddCommodityByComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'addquality', component: AddQualityComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'breakEvenForcast', component: BreakEvenForcastComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'accountDetailsGrid', component: AccountDetailsGridComponent , canActivate: [OnlyLoggedInUserGuardService]},
    { path: 'freightCalculator', component: FreightCalculatorComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'AboutusComponent', component: AboutusComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'TeamComponent', component: TeamComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'ProductsComponent', component: ProductsComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'OurServicesComponent', component: OurServicesComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'ContactUsComponent', component: ContactUsComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'FaqComponent', component: FaqComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'ProducerComponent', component: ProducerComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'SalesAndM2mPositionComponent', component: SalesAndM2mPositionComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'favourites', component: ContactsFavouritesComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'profMatrix', component: ViewProfitMatrixGridsComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'conOnFarm', component: ContractorsOnFarmComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'viewpcbgrid', component: ViewPcbGridComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'prodsaleforecast', component: ProductionSalesForecastComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'SalesPurchaseComponent', component: SalesAndPurchaseComponent, canActivate: [OnlyLoggedInUserGuardService] },
    { path: 'addNewFields', component: AddNewFieldsComponent },
    { path: '**', redirectTo: '/login' }
];
