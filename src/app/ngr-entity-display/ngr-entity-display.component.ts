import { Component, OnInit } from '@angular/core';
import { UserRoleService } from '../user-role.service';
@Component({
  selector: 'app-ngr-entity-display',
  templateUrl: './ngr-entity-display.component.html',
  styleUrls: ['./ngr-entity-display.component.scss']
})
export class NgrEntityDisplayComponent implements OnInit {
  userData: any;

  constructor(private user : UserRoleService) { }

  ngOnInit() {
    this.userData = this.user.getUser().USER;
  }

}
