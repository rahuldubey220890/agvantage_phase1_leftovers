import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgrEntityDisplayComponent } from './ngr-entity-display.component';

describe('NgrEntityDisplayComponent', () => {
  let component: NgrEntityDisplayComponent;
  let fixture: ComponentFixture<NgrEntityDisplayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgrEntityDisplayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgrEntityDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
