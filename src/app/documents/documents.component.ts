import { Component, OnInit, ViewChild } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import {Http } from "@angular/http";
import { serviceUrl } from '../app.module';
import { UserRoleService } from '../user-role.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html',
  styleUrls: ['./documents.component.scss']
})
export class DocumentsComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  columnDefs; 
  rowData: any;
 
  saleHistoryData: any;
  currentUser: any;
  document: any = {file:{}};
  selectedfile: any;
  uploadSuccesfull : boolean = false;
  documents: any;


  constructor(private role: UserRoleService, private http : Http, public spinnerService: Ng4LoadingSpinnerService) {
    this.columnDefs = [
      { headerName: "Action", checkboxSelection: true, pinned: 'left'},
      { headerName: "Contract No", field: "ContractNo", pinned: 'left' },
      { headerName: "Commodity", field: "Commodity", pinned: 'left' },
      { headerName: "Grade", field: "ContractNo", pinned: 'left' },
      { headerName: "Crop Year", field: "CropYear", pinned: 'left' },
      { headerName: "Sold Quantity", field: "Quantity", pinned: 'left' },
      { headerName: "Buyer", field: "BuyerName", pinned: 'left' },
    ];
    this.currentUser = this.role.getUser();

    
   }

  ngOnInit() {
    this.getSaleHistory();
  }
  onGridReady(params) {
    this.getSaleHistory();
  }

  @ViewChild('addDocModel') addDocModel;
  addDocModal(){
    this.addDocModel.show();
  }

  @ViewChild('browseDocumentModel') browseDocumentModel;
  browseDocument(){
    this.browseDocumentModel.show();
  }
  getSaleHistory(){
    let url = `${serviceUrl}SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseHistoryByUserId?U_ID=${this.currentUser.USER.U_ID}&U_Type=${this.currentUser.USER.U_TYPE}`;
    this.http.get(url).subscribe((data)=>{
      this.saleHistoryData = data.json()?data.json().SaleHistory : [];
    },
  (err)=>{
    this.saleHistoryData = [];
  });
  }
  uploadDoc(){
    console.log(this.document.file);
    this.spinnerService.show();
    let obj={
      "name" : this.selectedfile.name,
      "ContractNo" : "string",
      "RefNo" : "string",
      "Buyer" : "string",
      "Seller" : "string",
      "DocType" : this.selectedfile.type.split('/')[1],
      "UserId" : this.currentUser.USER.U_ID,
      "document" : this.document.file
      }
      let url = `${serviceUrl}DocumentsService/DocumentsServicePS/api/v1/UploadDocument`;
      this.http.post(url, obj).subscribe(data=>{
          console.log(data.json());
          this.browseDocumentModel.hide();
          this.spinnerService.hide();
          this.uploadSuccesfull = true;
         
          setTimeout(()=>{this.uploadSuccesfull = false},4000);
      },err=>{
        alert("Some error occured in the server")
      })
  }
  selectedFile(evt){
    this.selectedfile = evt.target.files[0];
    var reader = new FileReader();
   reader.readAsDataURL(this.selectedfile);
   reader.onload = ()=> {
     console.log(reader.result);
     this.document.file = reader.result.split(',')[1];
     console.log(this.document.file)
   };
   reader.onerror = function (error) {
     console.log('Error: ', error);
   };
  }
  

}
