import { Component, OnInit, Input } from '@angular/core';
import { GridOptions } from "ag-grid/main";

@Component({
  selector: 'app-ag-grid-wrapper',
  templateUrl: './ag-grid-wrapper.component.html',
  styleUrls: ['./ag-grid-wrapper.component.scss']
})
export class AgGridWrapperComponent implements OnInit {
  gridOptions: GridOptions;
  @Input() colDynamic;
  @Input() rowDynamic;
  @Input() columnTypesDynamic;
  columnDefs: any;
  getRowNodeId: (data: any) => any;
  rowData: any;
  gridApi: any;
  gridColumnApi: any;
  colTypes: any;
  constructor() { }

  ngOnInit() {
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        this.gridOptions.api.sizeColumnsToFit();
      }
    };
    this.getRowNodeId = function (data) {
      return data.id;
    };
    this.columnDefs = this.colDynamic;
    this.rowData = this.rowDynamic;
    this.colTypes = this.columnTypesDynamic;
  }
 

}
