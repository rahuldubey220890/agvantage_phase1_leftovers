import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContractorsOnFarmComponent } from './contractors-on-farm.component';

describe('ContractorsOnFarmComponent', () => {
  let component: ContractorsOnFarmComponent;
  let fixture: ComponentFixture<ContractorsOnFarmComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContractorsOnFarmComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContractorsOnFarmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
