import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPosM2MViewComponent } from './sales-pos-m2m-view.component';

describe('SalesPosDetailsViewComponent', () => {
  let component: SalesPosM2MViewComponent;
  let fixture: ComponentFixture<SalesPosM2MViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPosM2MViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPosM2MViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
