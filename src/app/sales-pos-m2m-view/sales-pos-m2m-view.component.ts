import { Component, OnInit } from '@angular/core';
import { GridOptions } from "ag-grid/main";
import {serviceUrl} from "../app.module";
import { Http, Headers, RequestOptions } from '@angular/http';
import { UserRoleService } from '../user-role.service';

@Component({
  selector: 'app-sales-pos-m2m-view',
  templateUrl: './sales-pos-m2m-view.component.html',
  styleUrls: ['./sales-pos-m2m-view.component.scss']
})
export class SalesPosM2MViewComponent implements OnInit {

  gridOptions: GridOptions;
  gridApi;
  gridColumnApi;
  getRowNodeId;
  defaultColDef;
  columnDefs;
  rowData;
  currentUser;

  constructor(private http: Http, private role: UserRoleService) {
    this.currentUser = this.role.getUser();
    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.api.sizeColumnsToFit();

      }
    };
    this.defaultColDef = { editable: false };
    this.getRowNodeId = function (data) {
      return data.id;
    };
    this.columnDefs = [
     
		{headerName: "Commodity", field: "commodity", pinned: 'left'},
		{headerName: "Quality", field: "quality", pinned: 'left'},
		{headerName: "Storage", field: "storage", pinned: 'left'},
		{headerName: "Crop Year", field: "crop_year", pinned: 'left'},
		{headerName: "Price Point", field: "price_point", pinned: 'left'},
		{headerName: "Unsold", field: "unsold", pinned: 'left'},
		{headerName: "Best Price", field: "best_price", pinned: 'left'},
		{headerName: "Exfarm Price", field: "exfarm_price", pinned: 'left'},
		{headerName: "M2M Revenue", field: "m2m_revenue", pinned: 'left'},
		{headerName: "Breakeven Per Unit", field: "breakeven_per_unit", pinned: 'left'},
		{headerName: "Profit Loss", field: "profit_loss", pinned: 'left'},
		{headerName: "M2M Profit", field: "m2m_profit", pinned: 'left'},
		{headerName: "M2M ROI", field: "m2m_roi", pinned: 'left'}
    ];
    this.rowData = [];
  }

  ngOnInit() {
    this.getSalesPosDetails();
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }





  getSalesPosDetails() {
    // https://129.154.71.56:9074/SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionDetailView?U_ID=279
	 // https://agnalysis.io:9074/GetM2MPosition/M2MPosition/api/v1/GetM2MPosition?user_id=261&farming_region_id=142
    let url = serviceUrl+'GetM2MPosition/M2MPosition/api/v1/GetM2MPosition?U_ID=' + this.currentUser.USER.U_ID+"&farming_region_id="+this.currentUser.USER.U_A_REGION;
    this.http.get(url).subscribe((data) => {
      if (data.json() != null) {
      	//mock data to test
       /* this.rowData=[ {
				"commodity" : "asdf",
				"quality" : 344,
				"storage" : "storage",
				"crop_year" : 4567,
				"price_point" : 345,
				"unsold" : 34,
				"best_price" : 45,
				"exfarm_price" : 33,
				"m2m_revenue" : 44,
				"breakeven_per_unit" : 4,
				"profit_loss" : 33,
				"m2m_profit" : 444,
				"m2m_roi" : 345
			},
				{
					"commodity" : "asdf",
					"quality" : 344,
					"storage" : "storage",
					"crop_year" : 4567,
					"price_point" : 345,
					"unsold" : 34,
					"best_price" : 45,
					"exfarm_price" : 33,
					"m2m_revenue" : 44,
					"breakeven_per_unit" : 4,
					"profit_loss" : 33,
					"m2m_profit" : 444,
					"m2m_roi" : 345
				} ]
		*/
      	this.rowData = data.json()?data.json().M2MPosition:[];
      } else {
        this.rowData = [];
      }
    })
  }

}
