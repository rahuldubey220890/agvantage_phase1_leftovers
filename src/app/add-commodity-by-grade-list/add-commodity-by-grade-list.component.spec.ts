import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddCommodityByGradeListComponent } from './add-commodity-by-grade-list.component';

describe('AddCommodityByGradeListComponent', () => {
  let component: AddCommodityByGradeListComponent;
  let fixture: ComponentFixture<AddCommodityByGradeListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCommodityByGradeListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddCommodityByGradeListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
