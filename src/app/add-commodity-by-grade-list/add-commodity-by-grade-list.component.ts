import { Component, ViewChild, OnInit,ChangeDetectorRef } from '@angular/core';
import {GridOptions} from "ag-grid/main";

@Component({
  selector: 'app-add-commodity-by-grade-list',
  templateUrl: './add-commodity-by-grade-list.component.html',
  styleUrls: ['./add-commodity-by-grade-list.component.scss']
})
export class AddCommodityByGradeListComponent implements OnInit {

  gridOptions: GridOptions;
     gridApi;
   gridColumnApi;
   
   columnDefs;

   rowData;
   defaultColDef;
   getRowNodeId;
  constructor(private cdRef: ChangeDetectorRef) { 
    // we pass an empty gridOptions in, so we can grab the api out

    this.gridOptions = <GridOptions>{
      onGridReady: () => {
        // this.gridOptions.columnApi.autoSizeAllColumns();
        this.gridOptions.api.sizeColumnsToFit();
      }
    };


    this.columnDefs = [
      {
        headerName: "",
        children: [
        { headerName: "", field: "attrs"},
        { headerName: "Actions", field: "actions", cellRenderer: this.actionRenderer, cellEditor: 'agRichSelect', editable: true },
        { headerName: "Commodity Type", field: "commodity" },
        { headerName: "Commodity Name", field: "commodityName" },
        { headerName: "Grade", field: "grade" },
        { headerName: "Grade", field: "grade1" },
        { headerName: "Grade", field: "grade2" }
        
      ]
      }
    ];

    this.rowData = [
      {     
        attrs: "Wheat",
        commodity: "Crop",
        commodityName: "Wheat",
        grade: "wa1",
        grade1: "wa1",
        grade2: "wa1"
      },
      {      
        attrs: "Wheat",
        commodity: "Crop",
        commodityName: "Wheat",
        grade: "wa2",
        grade1: "wa2",
        grade2: "wa2"
      },
      {      
        attrs: "Wheat",
        commodity: "Crop",
        commodityName: "Wheat",
        grade: "wa3",
        grade1: "wa3",
        grade2: "wa3"
      },
      {      
        attrs: "Wheat",
        commodity: "Crop",
        commodityName: "Wheat",
        grade: "wa4",
        grade1: "wa4",
        grade2: "wa4",
      },
      {      
        attrs: "Wheat",
        commodity: "Crop",
        commodityName: "Wheat",
        grade: "wa5",
        grade1: "wa5",
        grade2: "wa5",
      }
    ];

    this.defaultColDef = { editable: false };

    this.getRowNodeId = function (data) {
      return data.id;
    };
  }

  ngOnInit() {
  }

  onGridReady(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;

  }

  selectAllRows() {
    this.gridOptions.api.selectAll();
  }
  actionRenderer() {
    let actiontemplate = `
<a href="#">
          <span class="glyphicon glyphicon-edit" style="margin-right:12px;margin-left:2px" aria-hidden="true"></span>
        </a>
<a href="#">
          <span class="glyphicon glyphicon-trash"  aria-hidden="true"></span>
        </a>`;
    return actiontemplate;
  }

}
