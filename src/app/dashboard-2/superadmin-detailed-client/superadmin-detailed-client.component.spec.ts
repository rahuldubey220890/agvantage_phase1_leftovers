import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperadminDetailedClientViewComponent } from './superadmin-detailed-client.component';

describe('SuperAdminGetDetailedRevenueViewComponent', () => {
  let component: SuperadminDetailedClientViewComponent;
  let fixture: ComponentFixture<SuperadminDetailedClientViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperadminDetailedClientViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperadminDetailedClientViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
