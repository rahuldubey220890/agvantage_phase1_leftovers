import {Component, OnInit} from '@angular/core';
import {GridOptions} from "ag-grid/main";
import {serviceUrl} from "../../app.module";
import {Http, Headers, RequestOptions} from '@angular/http';
import {UserRoleService} from '../../user-role.service';

@Component({
	selector: 'aap-superadmin-detailed-client-view',
	templateUrl: './superadmin-detailed-client.component.html',
	styleUrls: ['./superadmin-detailed-client.component.scss']
})
export class SuperadminDetailedClientViewComponent implements OnInit {

	gridOptions: GridOptions;
	gridApi;
	gridColumnApi;
	getRowNodeId;
	defaultColDef;
	columnDefs;
	rowData;
	currentUser;

	constructor(private http: Http, private role: UserRoleService) {
		this.currentUser = this.role.getUser();
		this.gridOptions = <GridOptions>{
			onGridReady: () => {
				// this.gridOptions.api.sizeColumnsToFit();
			}
		};
		this.defaultColDef = {editable: false};
		this.getRowNodeId = function (data) {
			return data.id;
		};
		this.columnDefs = [
			{headerName: "Broker Consultant Company", field: "BrokerConsultantCompany", pinned: 'left'},
			{headerName: "Broker Consultant Office", field: "BrokerConsultantOffice", pinned: 'left',width: 140},
			{headerName: "Broker Consultant Name", field: "BrokerConsultantName", pinned: 'left',width: 140},
			{headerName: "Contact", field: "Contact", pinned: 'left',width: 90},
			{headerName: "Entity", field: "Entity", pinned: 'left',width: 90},
			{headerName: "Priority", field: "Priority", pinned: 'left',width: 90},
			{headerName: "Contact Frequency", field: "ContactFrequency", pinned: 'left',width: 120},
			{headerName: "LastContact", field: "LastContact", pinned: 'left',width: 90},
			{headerName: "Time", field: "Time", pinned: 'left',width: 90},
			{headerName: "Result", field: "Result", pinned: 'left',width: 90},
			{headerName: "Regarding", field: "Regarding", pinned: 'left',width: 100},
			{headerName: "Follow Up Task", field: "FollowUpTask", pinned: 'left',width: 140},
			{headerName: "TaskDate", field: "TaskDate", pinned: 'left',width: 90},
			{headerName: "Days Left For Task Date", field: "DaysLeftForTaskDate", pinned: 'left'},
			{headerName: "Next Scheduled Contact", field: "NextScheduledContact", pinned: 'left',hide : true},
			{headerName: "Days Left For Next Scheduled Contact", field: "DaysLeftForNextScheduledContact", pinned: 'left',hide : true},
			{headerName: "UserType", field: "UserType", pinned: 'left',width:90},
			{headerName: "AgnalysisUserType", field: "AgnalysisUserType", pinned: 'left',hide : true},
			{headerName: "NumberOfContacts", field: "NumberOfContacts", pinned: 'left',hide : true}
		];
		this.rowData = [];
	}

	ngOnInit() {
		this.getDetailedContact();
	}

	onGridReady(params) {
		this.gridApi = params.api;
		this.gridColumnApi = params.columnApi;
	}

	selectAllRows() {
		this.gridOptions.api.selectAll();
	}


	getDetailedContact() {
		// https://129.154.71.56:9074/SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionDetailView?U_ID=279
		// https://agnalysis.io:9074/GetM2MPosition/M2MPosition/api/v1/GetM2MPosition?user_id=261&farming_region_id=142
		let url = serviceUrl + 'SuperadminDashboardReportsSB/SuperadminDashboardReportsRestPS/api/v1/GetDetailedClientManagement?Contact_U_ID=' + this.currentUser.USER.U_ID;
		this.http.get(url).subscribe((data) => {
			if (data.json() != null) {
				this.rowData = data.json() ? data.json().DetailedClientManagementRecord : [];
			} else {
				this.rowData = [];
			}
		})
	}

}
