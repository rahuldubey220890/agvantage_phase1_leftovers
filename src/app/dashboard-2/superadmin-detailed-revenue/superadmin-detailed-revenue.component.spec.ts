import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuperAdminGetDetailedRevenueViewComponent } from './superadmin-detailed-revenue.component';

describe('SuperAdminGetDetailedRevenueViewComponent', () => {
  let component: SuperAdminGetDetailedRevenueViewComponent;
  let fixture: ComponentFixture<SuperAdminGetDetailedRevenueViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuperAdminGetDetailedRevenueViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuperAdminGetDetailedRevenueViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
