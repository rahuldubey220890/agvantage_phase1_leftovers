import { Component, OnInit, ChangeDetectorRef, AfterViewInit, AfterViewChecked } from '@angular/core';
import {Router} from '@angular/router';
import {Http, Headers, RequestOptions} from '@angular/http';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {serviceUrl} from "../app.module";
declare var $: any;

@Component({
    selector: 'app-add-commodity-by',
    templateUrl: './add-commodity-by.component.html',
    styleUrls: ['./add-commodity-by.component.scss']
})
export class AddCommodityByComponent implements OnInit {
    url: string;
    headers: Headers;
    options: RequestOptions;
    commodities: any;
    submitCommodity: string;
    selectedCommodity1: any;
    commodity: any;
    commodityType: any;
    commodityName: any;
    cropYear: any
    gradespec: any;
    nearTrmnlPort;
    pinCode;
    state;
    city;
    suburb;
    street;
    unit;
    grades:any;
    constructor(private cdRef: ChangeDetectorRef, private router: Router, private http: Http, public spinnerService: Ng4LoadingSpinnerService) {
        this.commodity = {};
    }

    ngOnInit() {
        this.getCommodities();
    }
    ngAfterViewInit() {
        this.cdRef.detectChanges();

    }
    ngAfterViewChecked() {
        $('.selectpicker').selectpicker('refresh');
    }
    gotoHome() {
        this.router.navigate(['dashboard']);
    }
    getCommodities() {
        this.url = serviceUrl+"AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
        this.http.get(this.url).subscribe((data) => {
            this.commodities = data.json().CommodityCollection.Commodity;
            //this.selectedCommodity1=this.commodities[0].C_NAME;
            console.log(this.commodities);
        })
    }
    

     callGrades(){
         this.getCommodityGrade(this.commodity.SCC_C_ID);  
     }
     getCommodityGrade(id){
         this.url=serviceUrl+"AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID="+id;
         this.http.get(this.url).subscribe((data)=>{
             this.grades=data.json().gradeCollection.grade;
         })
     }
    addCommodityByType() {
        this.spinnerService.show();
        this.headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json'
        });
        this.options = new RequestOptions({ headers: this.headers });
        console.log(this.commodity);
        this.commodity.SCC_U_ID = 1;
        this.url = serviceUrl+"AgVCommodityRegistartion/AgvantageCommodityRegPS/api/v1/commRegister";

        this.http.post(this.url, $.param(this.commodity), this.options).subscribe((data) => {
            this.submitCommodity = data.json().outcome;
            this.commodity = {};
            // this.grade="";
            this.spinnerService.hide();
            $(".modal-content").scrollTop(0);
        })
    }
    addCommodityByGradeSpec(){
        
    }
    addPublicWarehouseByState(){
        
    }

}
