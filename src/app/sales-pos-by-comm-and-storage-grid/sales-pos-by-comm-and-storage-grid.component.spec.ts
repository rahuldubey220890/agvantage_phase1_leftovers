import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesPosByCommAndStorageGridComponent } from './sales-pos-by-comm-and-storage-grid.component';

describe('SalesPosByCommAndStorageGridComponent', () => {
  let component: SalesPosByCommAndStorageGridComponent;
  let fixture: ComponentFixture<SalesPosByCommAndStorageGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesPosByCommAndStorageGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesPosByCommAndStorageGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
