import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SalesAndM2mPositionComponent } from './sales-and-m2m-position.component';

describe('SalesAndM2mPositionComponent', () => {
  let component: SalesAndM2mPositionComponent;
  let fixture: ComponentFixture<SalesAndM2mPositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SalesAndM2mPositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SalesAndM2mPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
