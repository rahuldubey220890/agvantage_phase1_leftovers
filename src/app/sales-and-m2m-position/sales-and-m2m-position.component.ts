import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Headers, RequestOptions } from '@angular/http';
import { UserRoleService } from '../user-role.service';
import {serviceUrl} from "../app.module";

@Component({
  selector: 'app-sales-and-m2m-position',
  templateUrl: './sales-and-m2m-position.component.html',
  styleUrls: ['./sales-and-m2m-position.component.scss']
})
export class SalesAndM2mPositionComponent implements OnInit {
  columnDefs;
  currentUser;
  // sGBComm=true;
  // sGBCommStrg=false;
  // saleposcommstrg=true;
  // saleposcomm=false;
  options;
  constructor( private http: Http, private role: UserRoleService, private router: Router) {
    this.currentUser = this.role.getUser();
    this.options={};
  }

  ngOnInit() {
   // this.getGridData(this.currentUser);
    // this.options.sgcomm = false;
    // this.options.sgcommstrg = false;
    // this.options.sgdetailview = false;


  }
     gotoHome() {
        this.router.navigate(['dashboard']);
    }


  gridData;
  getGridData(id) {
    let url = serviceUrl+'GetSalesPosition/GetSalesPositionPS/api/v1/getSalesPosition?User_ID=' + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      if (data.json() != null) {
        this.gridData = data.json().AgvSalesPositionCollection;
      } else {
        this.gridData = [];
      }
    })
  }




  salesPosDetailView;
  getSalesPositionDetailView() {
    let url = serviceUrl+'SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionDetailView?U_ID=' + this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      if (data.json() != null) {
        this.salesPosDetailView = data.json()?data.json().SalesPosition:[];
      } else {
        this.salesPosDetailView = [];
      }
    })
  }

  onSGCommStrgChange(){

  //  this.sGBComm = true;
  //  this.sGBCommStrg = false;
  //  this.options.sgcomm = false;
  //  this.options.sgcommstrg = true;
  //  this.options.sgdetailview = false;
  }

  onSGCommChange(){
    // this.sGBCommStrg = true;
    // // this.sGBComm = false;
    // this.options.sgcomm = true;
    // this.options.sgcommstrg = false;
    // this.options.sgdetailview = false;
  }

  onSGDetailsViewChange(){
    // this.options.sgcomm = false;
    // this.options.sgcommstrg = false;
    // this.options.sgdetailview = true;
  }

  option1;
  option2;
  option3;
  openDetailedView(value){
  	if(value==="sgcomm"){
      this.option1 = true;
      this.option2 = false;
      this.option3 = false;
    }
    if(value==="sgcommstrg"){
      this.option2 = true;
      this.option1 = false;
      this.option3 = false;
    }
    if(value==="sgdetailview"){
      this.option2 = false;
      this.option1 = false;
      this.option3 = true;
    }
  }

}
