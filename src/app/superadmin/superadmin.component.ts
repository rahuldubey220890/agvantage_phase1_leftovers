import { Component, OnInit, ViewChild, NgZone, ElementRef } from '@angular/core';
import { serviceUrl } from '../app.module';
import { Http, Headers, RequestOptions } from '@angular/http';
import { GridOptions } from 'ag-grid';
import { UserRoleService } from '../user-role.service';
import { EditDeleteRendererComponent } from '../edit-delete-renderer/edit-delete-renderer.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
declare var google;

@Component({
  selector: 'app-superadmin',
  templateUrl: './superadmin.component.html',
  styleUrls: ['./superadmin.component.scss']
})
export class SuperadminComponent implements OnInit {

  createGroupModal:any;
  @ViewChild('createGroupModalTS') public createGroupModalTS;
  @ViewChild('duplicateGroupModal') public duplicateGroupModalTS;
  @ViewChild('smsModal') public smsModal;
  @ViewChild('emailModal') public emailModal;
  @ViewChild('editConfModal') public editConfModal;
  @ViewChild('editGroupModel') public editGroupModel;
  @ViewChild('createAddMember') public createAddMember;
  @ViewChild('todoTaskModel') public todoTaskModel;
  @ViewChild('storageAndInvModel') public storageAndInvModel;
  @ViewChild('addToDoTaskModal') public addToDoTaskModal;
  @ViewChild('addDestinationModal') public addDestinationModal;
  @ViewChild('editRegionMappingModal') public editRegionMappingModal;
  @ViewChild('delModal') public delModal;
  @ViewChild('locationMapModal') public locationMapModal;
  @ViewChild("mapDiv") public mapDiv: ElementRef;
  @ViewChild("editIDStatusModal") public editIDStatusModal;
  @ViewChild("salesAndPurchaseModal") public salesAndPurchaseModal;
  @ViewChild('salesAndPurchaseModalForm') public salesAndPurchaseModalForm;
  @ViewChild('createGroupForm') public createGroupForm;
  @ViewChild('duplicateGroupForm') public duplicateGroupForm;
  @ViewChild('addInventoryModal') public addInventoryModal;
  @ViewChild('recordHistoryModal') public recordHistoryModal;
  @ViewChild('breakEvenModel') public breakEvenModel;
  @ViewChild('salesPosM2mModel') public salesPosM2mModel;
  @ViewChild('salesForcastModel') public salesForcastModel;
  @ViewChild('viewFavouriteModal') public viewFavouriteModal;
  @ViewChild('viewSalesPurchaseModal') public viewSalesPurchaseModal;
  @ViewChild('ViewContractorModal') public ViewContractorModal;
  @ViewChild('SalesAndPurchaseModal') public SalesAndPurchaseModal;
  @ViewChild('ViewContactModal') public ViewContactModal;
  @ViewChild('FavouritesModal') public FavouritesModal;
  @ViewChild('viewBreakevenModal') public viewBreakevenModal;
  @ViewChild('InventoryModal') public InventoryModal;
  @ViewChild('SalesPosM2MModal') public SalesPosM2MModal;
  @ViewChild('ViewInventoryModel') public ViewInventoryModel;
  
  headers: Headers;
  option: RequestOptions;
  rowSelection: string;
  rowSelectionGroup: string;
  rowData: any;
  rowDataAllGroup:any;
  rowDataAllInvRec:any;
  rowDataToDoTask:any;
  colDef: any[];
  columnDefs1: any[];
  columnDefs2: any[];
  columnDefs3: any[];
  columnDefs4: any[];
  columnDefs5: any[];
  storeInvColDef: any[];
  colDefDynamicGrid : any[];
  colDefViewHistoryGrid: any[];
  colDefBreakEvenGrid: any[];
  colDefGetInvGrid: any[];
  colDefViewSalesM2MPosGrid: any[];//getProdSaleFrcstGridData()
  colDefGetProdSaleFrcstGrid: any[];
  colDefViewFavGrid: any[];
  colDefViewRecHistrGrid: any[];
  colDefViewSalesPurchaseGrid: any[];
  colDefViewContrOnFarmGrid: any[];
  colDefAllSalesPurchaseGrid: any[];
  colDefAllContactsGrid: any[];
  colDefAllInventoryGrid : any[];
  colDefAllFavouritesGrid: any[];
  colDefAllBreakEvenGrid: any[];
  colDefAllSalesM2MGrid: any[];
  columnDefsNTPDetails: any[];

  _values1:any[];
  _values2:any[];
  showGroupMgmtButtons = false;
  showNotificationButtons = false;
  showMasterDataButtons = false;
  showToDoTaskButtons = false;
  showActionPerformed;
  showAddInventory = true;
  val:any;
  columnDefsFarmRgn: any[];
  gridApi: any;
  gridOptions: GridOptions;
  gridOptions1: GridOptions;
  gridOptions2: GridOptions;
  gridOptions3: GridOptions;
  gridOptions4: GridOptions;
  gridOptions5: GridOptions;
  currentUser;
  holdParams;
  gridColumnApi: any;
  salesPurchase;
  addContractors;
  smsFormData;
  editConfig;
  dupGroupFormData;
  creatGroupFormData;
  emailFormData;
  addToDoTaskData;
  addRecHistoryData;
  addDestinationData;
  inventory;
  editIDStatusData;
  submitCreateGroupMessage;
  submitDuplicateGroupMessage;
  addToDoTaskMessage;
  frameworkComponents:any;
  context:any;
  ScheduleWithDDL:any = []; ActivtyTypesDDL:any = []; ReminderDDL:any=[];
  recMgrList:any=[];
  businessCreditRefData: any;
  brokerOffices :any[] = [];

  sellerRepresentatives: any;
  gridOptionsToDo: GridOptions;
  gridOptionsFarmingRegion :GridOptions;
  gridApiFarmRgn: any;
  gridColumnApiFarmRgn: any;
  gridOptionsViewHistoryGrid:GridOptions;
  gridOptionsBreakEvenGrid:GridOptions;
  gridOptionsGetInvGrid:GridOptions;
  gridOptionsGetAllSalesPosGrid:GridOptions;
  gridOptionsProdSaleFrcstGrid:GridOptions;
  gridOptionsViewFavGrid:GridOptions;
  gridOptionsViewRecHistrGrid:GridOptions;
  gridOptionsViewSalesPurchaseGrid:GridOptions;
  gridOptionsViewContrOnFarmGrid:GridOptions;
  gridOptionsAllSalesPurchaseGrid:GridOptions;
  gridOptionsAllSalesM2MGrid:GridOptions;
  gridOptionsInventoryGrid:GridOptions;
  gridOptionsAllBreakEvenGrid:GridOptions;
  gridOptionsAllFavouritesGrid:GridOptions;
  gridOptionsAllContactsGrid:GridOptions;
  gridApiTruck: any;
  gridColumnApiTruck: any;
  gridOptionsTruck :GridOptions;

  public options = { types: ['address'], componentRestrictions: { country: 'AUS' } };

  constructor(private http : Http, private user: UserRoleService, private spinnerService :Ng4LoadingSpinnerService,
    private _ngZone: NgZone) {
      this.salesPurchase=[];
    this.smsFormData=[];
    this.emailFormData = [];
    this.editConfig=[];
    this.creatGroupFormData=[];
    this.dupGroupFormData=[];
    this.addToDoTaskData = [];
    this.addDestinationData = [];
    this.editIDStatusData = [];
    this.addContractors = [];
    this.rowSelection = "multiple";
    this.rowSelectionGroup = "single";
    this.inventory = {};
    this.addRecHistoryData={};
    this.gridOptionsTruck = <GridOptions>{
      onGridReadyTruck: () => {
         //this.setTruckGrid();

      }
    };
    this.ActivtyTypesDDL = [
      { ACT_ID: 1, ACT_NAME: 'call' },
      { ACT_ID: 2, ACT_NAME: 'meeting' },
      { ACT_ID: 2, ACT_NAME: 'todo' },
      { ACT_ID: 2, ACT_NAME: 'sms' },
      { ACT_ID: 2, ACT_NAME: 'email' },
    ];

    this.ReminderDDL = [
      { REM_ID: 1, REM_NAME: 'Daily' },
      { REM_ID: 2, REM_NAME: 'weekly' },
      { REM_ID: 2, REM_NAME: 'fortnightly' },
      { REM_ID: 2, REM_NAME: 'monthly' },
      { REM_ID: 2, REM_NAME: 'quarterly' },
      { REM_ID: 2, REM_NAME: 'annually' }
    ];

    this.ScheduleWithDDL = [
      { SCH_ID: 1, SCH_NAME: 'KR' },
      { SCH_ID: 2, SCH_NAME: 'RD' }
    ];

    this.recMgrList = [
      { mgr_id: 1, mgr_name: 'KR' },
      { mgr_id: 2, mgr_name: 'RD' }
    ];

    this.columnDefsNTPDetails = [
      { headerName: "NTP Name",  field: "NTPName"},
      { headerName: "Nominated Sites", field: "NomSites" },
      { headerName: "Location Differential", field: "LocDiff" },
      { headerName: "Site Price", field: "SitePrice" }
    ]

    this.currentUser = this.user.getUser();
    this.gridOptionsToDo = <GridOptions>{};

    this.gridOptionsViewHistoryGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsViewHistoryGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsBreakEvenGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsBreakEvenGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsGetInvGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsGetInvGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsGetAllSalesPosGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsGetAllSalesPosGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsProdSaleFrcstGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsProdSaleFrcstGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsViewFavGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsViewFavGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsViewRecHistrGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsViewRecHistrGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsViewSalesPurchaseGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsViewSalesPurchaseGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsViewContrOnFarmGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsViewContrOnFarmGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsAllSalesPurchaseGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsAllSalesPurchaseGrid.api.sizeColumnsToFit();
      }
    };
    this.gridOptionsAllSalesM2MGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsAllSalesM2MGrid.api.sizeColumnsToFit();
      }
    };
    this.gridOptionsInventoryGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsInventoryGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsAllBreakEvenGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsAllBreakEvenGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptionsAllFavouritesGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsAllFavouritesGrid.api.sizeColumnsToFit();
      }
    };
    this.gridOptionsAllContactsGrid = <GridOptions>{
      onGridReady: (params) => {
        // this.gridApi = params.api;
        this.gridOptionsAllContactsGrid.api.sizeColumnsToFit();
      }
    };

    this.gridOptions = <GridOptions>{
      onGridReady: (params) => {
          this.gridApi = params.api;
          this.gridOptions.api.sizeColumnsToFit();
          this.getgroups();
      }
  };

  this.gridOptions1 = <GridOptions>{
    onGridReady: () => {
      this.gridOptions1.api.sizeColumnsToFit();
      this.getAllGroupsMember();
    }
  };
  this.gridOptions2 = <GridOptions>{
    onGridReady: () => {
      this.gridOptions2.api.sizeColumnsToFit();

    }
  };
  this.gridOptions3 = <GridOptions>{
    onGridReady: () => {
      this.gridOptions3.api.sizeColumnsToFit();
    }
  };

  this.gridOptions4 = <GridOptions>{
    onGridReady: () => {
      this.gridOptions4.api.sizeColumnsToFit();
      this.getAllInvRecords();
    }
  };

  this.gridOptions5 = <GridOptions>{
    onGridReady: () => {
      this.gridOptions5.api.sizeColumnsToFit();
    }
  };


  this.gridOptionsFarmingRegion = <GridOptions>{
    onGridReadyFarmRgn: () => {
       //this.setTruckGrid();

    }
  };




  this.getTruckTypes();
  this.setCurrentPosition(this);
  this.getStates();
  this.getDestinationType(); this.getTransportMode();
  this.getPriorityList(); this.getIDStatusList(); this.getContactFrqncyList();
  this.getSalesType();
  this.getSalesOriginDest();
  this.getCommodities();
    this.getCropyearList();
    // this.getCommodityOrigin();
    // this.getCommodityDestination();
  this.colDefViewHistoryGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "History ID", field: "History_ID"} ,
    { headerName: "Type Name", field: "Type_Name" },
    { headerName: "Result Name", field: "Result_Name" },
    { headerName: "Contact User Name", field: "Contact_User_Name" },
    { headerName: "Date", field: "Date" },
    { headerName: "Time", field: "Time" },
    { headerName: "RecordManager Name", field: "RecordManager_Name" },
    { headerName: "Task Date", field: "Task_Date" },
    { headerName: "Regarding", field: "Regarding" },
    { headerName: "Reminder Date", field: "Reminder_Date" },
    { headerName: "Reminder Time", field: "Reminder_Time" },
    { headerName: "Details", field: "Details" }
];

  this.colDefBreakEvenGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
     {
      headerName: "Farm Name", field: "farm_name"} ,
    { headerName: "Crop Year", field: "crop_year" },

    { headerName: "Commodity", field: "commodity_name" },
    { headerName: "Area Hectare", field: "AreaHectares" },

    { headerName: "Total Production", field: "TotalProduction" },
    { headerName: "Marketable Production", field: "MarketableProduction" },
    { headerName: "TotalGrowing Costs/Hectare", field: "TotalGrowingCostsPerHectare" },
    { headerName: "TotalGrowing Costs/Crop", field: "TotalGrowingCostsPerCrop" },
    { headerName: "Growing Breakeven/Unit", field: "GrowingBreakevenPerUnit" },
    { headerName: "TotalAssets Cost/Hectare", field: "TotalAssetsCostPerHectare" } ,
    { headerName: "TotalAssetsCost/Crop", field: "TotalAssetsCostPerCrop" } ,
    { headerName: "AssetsEmployed Breakeven/Unit", field: "AssetsEmployedBreakevenPerUnit" } ,
    { headerName: "Target ROI", field: "TargetROI" }
];

this.colDefGetInvGrid = [
  { headerName: "Unsold", field: "unsoldQuantity",aggFunc: 'sum', enableValue: true,
    allowedAggFuncs: ['sum'] 
  },
  { headerName: "Sold", field: "soldQuantity",aggFunc: 'sum', enableValue: true
  },
  { headerName: "Storage Name", field: "storage_name" },
  // { headerName: "Field From", field: "field_from", headerClass: "text-center" },
  { headerName: "Commodity", field: "comm_name", rowGroup:true },
  { headerName: "Crop Year", field: "crop_year" },
  // { headerName: "Load Name", field: "loadName" },
  // { headerName: "Qualities", field: "qualities", width: 200, tooltipField: "qualities" },            
  { headerName: "Grade", field: "grade_name" },
 
  { headerName: "Target Price", field: "target_price" },
    
];

this.colDefViewSalesM2MPosGrid = [
  { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Entity", field: "Entity"} ,
    { headerName: "SaleType", field: "SaleType" },
    { headerName: "M2MProfit", field: "M2MProfit" },
    { headerName: "WeightedAverageRevenue", field: "WeightedAverageRevenue" },
    { headerName: "TotalAssetsEmployedCostPerCrop", field: "TotalAssetsEmployedCostPerCrop" },
    { headerName: "FreightInventoryToSaleLocation", field: "FreightInventoryToSaleLocation" },
    { headerName: "PPDestination", field: "PPDestination" },
    { headerName: "SaleValuePerUnit", field: "SaleValuePerUnit" },
    { headerName: "M2MROI", field: "M2MROI" },
    { headerName: "TargetROI", field: "TargetROI" },
    { headerName: "CarryFumigationCost", field: "CarryFumigationCost" },
    { headerName: "BreakevenPerUnit", field: "BreakevenPerUnit" },
    { headerName: "Commodity", field: "Commodity" },
    { headerName: "StorageName", field: "StorageName" },
    { headerName: "UnsoldQuantity", field: "UnsoldQuantity" },
    { headerName: "ExFarmPrice", field: "ExFarmPrice" },
    { headerName: "AvgGrowingCost", field: "AvgGrowingCost" },
    { headerName: "M2MRevenue", field: "M2MRevenue" },
    { headerName: "FreightFarmToInventoryLocation", field: "FreightFarmToInventoryLocation" },
    { headerName: "UnsoldPerc", field: "UnsoldPerc" },
    { headerName: "M2MProfitLoss", field: "M2MProfitLoss" },
    { headerName: "ROI", field: "ROI" },
    { headerName: "StartingQuantity", field: "StartingQuantity" },
    { headerName: "WeightedAvgGrowingProfitPerHA", field: "WeightedAvgGrowingProfitPerHA" },
    { headerName: "TotalAssetsEmployedToGrow", field: "TotalAssetsEmployedToGrow" },
    { headerName: "WeightedAvgGrowingProfitPerUnit", field: "WeightedAvgGrowingProfitPerUnit" },
    { headerName: "SoldPerc", field: "SoldPerc" },
    { headerName: "CropYear", field: "CropYear" }
]

  this.colDefAllSalesM2MGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Entity", field: "EntityName"} ,
    { headerName: "SaleType", field: "SaleType" },
    { headerName: "M2MProfit", field: "M2MProfit" },
    { headerName: "WeightedAverageRevenue", field: "WeightedAverageRevenue" },
    { headerName: "TotalAssetsEmployedCostPerCrop", field: "TotalAssetsEmployedCostPerCrop" },
    { headerName: "FreightInventoryToSaleLocation", field: "FreightInventoryToSaleLocation" },
    { headerName: "PPDestination", field: "PPDestination" },
    { headerName: "SaleValuePerUnit", field: "SaleValuePerUnit" },
    { headerName: "M2MROI", field: "M2MROI" },
    { headerName: "TargetROI", field: "TargetROI" },
    { headerName: "CarryFumigationCost", field: "CarryFumigationCost" },
    { headerName: "BreakevenPerUnit", field: "BreakevenPerUnit" },
    { headerName: "Commodity", field: "Commodity" },
    { headerName: "StorageName", field: "StorageName" },
    { headerName: "UnsoldQuantity", field: "UnsoldQuantity" },
    { headerName: "ExFarmPrice", field: "ExFarmPrice" },
    { headerName: "AvgGrowingCost", field: "AvgGrowingCost" },
    { headerName: "M2MRevenue", field: "M2MRevenue" },
    { headerName: "FreightFarmToInventoryLocation", field: "FreightFarmToInventoryLocation" },
    { headerName: "UnsoldPerc", field: "UnsoldPerc" },
    { headerName: "M2MProfitLoss", field: "M2MProfitLoss" },
    { headerName: "ROI", field: "ROI" },
    { headerName: "StartingQuantity", field: "StartingQuantity" },
    { headerName: "WeightedAvgGrowingProfitPerHA", field: "WeightedAvgGrowingProfitPerHA" },
    { headerName: "TotalAssetsEmployedToGrow", field: "TotalAssetsEmployedToGrow" },
    { headerName: "WeightedAvgGrowingProfitPerUnit", field: "WeightedAvgGrowingProfitPerUnit" },
    { headerName: "SoldPerc", field: "SoldPerc" },
    { headerName: "CropYear", field: "CropYear" }
  ];

  this.colDefGetProdSaleFrcstGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Commodity Name", field: "Commodity_name"} ,
    { headerName: "cropYear", field: "cropYear" },
    { headerName: "totalProduction", field: "totalProduction" },
    { headerName: "RetainedSeed", field: "RetainedSeed" },
    { headerName: "SaleableProduction", field: "SaleableProduction" },
    { headerName: "RecommendedSale", field: "RecommendedSale" },
    { headerName: "RecommendedSaleQuantity", field: "RecommendedSaleQuantity" },
    { headerName: "ModifiedDatetime", field: "ModifiedDatetime" }
  ];

  this.colDefViewFavGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Farming Region name", field: "Farming_Region_name"} ,
    { headerName: "Country name", field: "Country_name" },
    { headerName: "Port Zone name", field: "Port_Zone_name" },
    { headerName: "Storage Type", field: "Storage_Type" },
    { headerName: "Site name", field: "Site_name" },
    { headerName: "State name", field: "State_name" },
    { headerName: "Alert price", field: "Alert_price" },
    { headerName: "Target ROI", field: "Target_ROI" },
    { headerName: "Grade", field: "Grade" },
    { headerName: "M2M ROI", field: "M2M_ROI" },
    { headerName: "Commodity name", field: "Commodity_name" },
    { headerName: "Price", field: "Price" },
    { headerName: "Crop year", field: "Crop_year" },
    { headerName: "Alert Email", field: "Alert_Email" },
    { headerName: "Alert SMS", field: "Alert_SMS" }
  ];

  this.colDefViewRecHistrGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Contact User Name", field: "Contact_User_Name"} ,
    { headerName: "Details", field: "Details" },
    { headerName: "Type Name", field: "Type_Name" },
    { headerName: "Result Name", field: "Result_Name" },
    { headerName: "RecordManager Name", field: "RecordManager_Name" },
    { headerName: "Time", field: "Time" },
    { headerName: "Task_Date", field: "Task_Date" },
    { headerName: "Regarding", field: "Regarding" },
    { headerName: "Date", field: "Date" },
    { headerName: "Reminder_Date", field: "Reminder_Date" }
];

  this.colDefViewSalesPurchaseGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "SaleDate", field: "SaleDate"} ,
    { headerName: "SaleType", field: "SaleType" },
    { headerName: "SaleOrigin", field: "SaleOrigin" },
    { headerName: "Commodity", field: "Commodity" },
    { headerName: "BuyerName", field: "BuyerName" },
    { headerName: "SellerName", field: "SellerName" },
    { headerName: "SellerRepresentative", field: "SellerRepresentative" },
    { headerName: "BuyerRepresentative", field: "BuyerRepresentative" },
    { headerName: "BusinessCreditedTo", field: "BusinessCreditedTo" },
    { headerName: "OfficeCreditedTo", field: "OfficeCreditedTo" },
    { headerName: "ProducerBrokerTrader", field: "ProducerBrokerTrader" },
    { headerName: "ReferenceNo", field: "ReferenceNo" },
    { headerName: "FFS", field: "FFS" },
    { headerName: "FFSInvoicedTo", field: "FFSInvoicedTo" },
    { headerName: "Variety", field: "Variety" },
    { headerName: "Grade", field: "Grade" },
    { headerName: "CropYear", field: "CropYear" } ,
    { headerName: "ContractNo", field: "ContractNo" },
    { headerName: "ContractType", field: "ContractType" },
    { headerName: "Quantity", field: "Quantity" } ,
    { headerName: "ContractPrice", field: "ContractPrice" } ,
    { headerName: "Discounts", field: "Discounts" } ,
    { headerName: "Increments", field: "Increments" },
    { headerName: "Area", field: "Area" } ,
    { headerName: "MinYield", field: "MinYield" } ,
    { headerName: "MaxYield", field: "MaxYield" } ,
    { headerName: "MinQuantity", field: "MinQuantity" } ,
    { headerName: "MaxQuantity", field: "MaxQuantity" } ,
    { headerName: "Tolerance", field: "Tolerance" } ,
    { headerName: "DeliveryTerms", field: "DeliveryTerms" } ,
    { headerName: "PaymentTerms", field: "PaymentTerms" } ,
    { headerName: "CommodityOrigin", field: "CommodityOrigin" } ,
    { headerName: "CommodityDestination", field: "CommodityDestination" } ,
    { headerName: "FarmingRegion", field: "FarmingRegion" }
  ];

  this.colDefViewContrOnFarmGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    { headerName: "Name", field: "Name"} ,
    { headerName: "Job", field: "Job" },
    { headerName: "Phone", field: "Phone" },
    { headerName: "Hectares Completed", field: "HectaresCompleted" },
    { headerName: "Date Completed", field: "DateCompleted" },
    { headerName: "Key Contact", field: "KeyContact"} ,
    { headerName: "Country name", field: "Country_name" },
    { headerName: "Key Contact", field: "KeyContact"} ,
    { headerName: "Country name", field: "Country_name" },
    { headerName: "UHF", field: "UHF" },
    { headerName: "Date Commenced", field: "DateCommenced" },
    { headerName: "Field Location", field: "FieldLocation" },
    { headerName: "Company Name", cellRenderer: function(params) {
      return params.data.MachineryDetails.CompanyName}
    },
    { headerName: "Comments", cellRenderer: function(params) {
      return params.data.MachineryDetails.Comments}
    },
    { headerName: "Driver Name", cellRenderer: function(params) {
      return params.data.MachineryDetails.DriverName}

    },
    { headerName: "Carrier Rating", cellRenderer: function(params) {
      return params.data.MachineryDetails.CarrierRating}

    },
    { headerName: "Machinery Reg No", cellRenderer: function(params) {
      return params.data.MachineryDetails.MachineryRegNo}
    }

  ];

  this.colDefAllSalesPurchaseGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Confirmation comp by", field: "confirmation_comp_by"} ,
    { headerName: "Farming Region", field: "FARMING_REGION" },
    { headerName: "Storage Name", field: "STORAGE_NAME" },
    { headerName: "Invoice Date", field: "INVOICE_DATE" },
    { headerName: "Reference No", field: "REFERENCE_NO" },
    { headerName: "Seller Trade Name", field: "SELLER_TRADE_NAME" },
    { headerName: "Sale Type", field: "SALE_TYPE" },
    { headerName: "Buyer Trade Name", field: "BUYER_TRADE_NAME" },
    { headerName: "Commodity", field: "C_NAME" },
    { headerName: "Contract No", field: "CONTRACT_NO" },
    { headerName: "Crop Year", field: "CRP_YEAR" },
    { headerName: "Grade", field: "CG_NAME" },
    { headerName: "Quantity", field: "QUANTITY" },
    { headerName: "Contract Type", field: "CONTRACT_PRICE" },
    { headerName: "Delivery Date From", field: "DELIVERY_DATE_FROM" } ,
    { headerName: "Delivery Date To", field: "DELIVERY_DATE_TO" } ,
    { headerName: "Transfer Done By", field: "TRANSFER_DONE_BY" } ,
    { headerName: "Biz Credited", field: "BIZ_CREDITED" } ,
    { headerName: "Office Credited", field: "OFFC_CREDITED" }

];

  this.colDefAllContactsGrid = [
    { headerName: "", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 50,  editable: true },
    { headerName: "Title", field: "add_title"} ,
    { headerName: "first Name", field: "first_name"} ,
    { headerName: "Last Name", field: "last_name" },
    { headerName: "Entity Name", field: "entity_name" },
    { headerName: "UHF", field: "UHF" },
    { headerName: "NGR", field: "ngr" },
    { headerName: "Role Title", field: "role_title" },
    { headerName: "Personal Mobile", field: "personal_mobile" },
    { headerName: "Office phone", field: "office_ph" },
    { headerName: "Home Fax", field: "home_fax" },
    { headerName: "Home Phone", field: "home_ph" },
    { headerName: "Email", field: "email" },
    { headerName: "Address", field: "address" },
    { headerName: "Region", field: "region" },
    { headerName: "City", field: "city" },
    { headerName: "State", field: "state" },
    { headerName: "Postcode", field: "postcode" },
    { headerName: "Spouse Email", field: "spouse_email" },
    { headerName: "Add Partnermobile", field: "add_partnermobile" },
    { headerName: "Work Mobile", field: "work_mobile" },
    { headerName: "Partner", field: "partner" }
];
  this.colDefAllFavouritesGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Site", field: "Site"} ,
    { headerName: "Storage Type", field: "Storage_Type" },
    { headerName: "Alert Price", field: "Alert_price" },
    { headerName: "Port Zone", field: "Port_Zone" },
    { headerName: "Crop Year", field: "Crop_Year" },
    { headerName: "Grade", field: "Grade" },
    { headerName: "Target ROI", field: "TargetROI" },
    { headerName: "Contact Name", field: "Contact_Name" },
    { headerName: "Commodity", field: "Commodity" },
    { headerName: "Price", field: "Price" },
    { headerName: "State", field: "State" },
    { headerName: "Alert Email", field: "Alert_Email" },
    { headerName: "Country", field: "Country" },
    { headerName: "Farming region", field: "Farming_region" },
    { headerName: "Entity Name", field: "Entity_Name" },
    { headerName: "Alert SMS", field: "Alert_SMS" }

];

  this.colDefAllBreakEvenGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 20,  editable: true },

    {
      headerName: "Country", field: "Country"} ,
    { headerName: "State", field: "State" },
    { headerName: "Port_Zone", field: "Port_Zone" },
    { headerName: "CropYear", field: "CropYear" },
    { headerName: "FarmingRegion", field: "FarmingRegion" },
    { headerName: "Entity", field: "Entity" },
    { headerName: "Commodity", field: "Commodity" },
    { headerName: "Area hectare", field: "Area_hectare" },
    { headerName: "Yield hectare", field: "Yield_hectare" },
    { headerName: "Total Production", field: "Total_Production" },
    { headerName: "Sold Quantity", field: "Sold_Quantity" },
    { headerName: "Unsold Quantity", field: "Unsold_Quantity" }

];

  this.colDefAllInventoryGrid = [
    { headerName: "Select", checkboxSelection: true, headerCheckboxSelection: true,
     field: "actions", width: 200,  editable: true },
    {
      headerName: "Unsold", field: "Unsold"} ,
    { headerName: "Storage Type", field: "Storage_Type" },
    { headerName: "Grade", field: "Grade" },
    { headerName: "Target Price", field: "Target_Price" },
    { headerName: "City", field: "City" },
    { headerName: "Post Code", field: "Post_Code" },
    { headerName: "Storage Name Capacity", field: "Storage_Name_Capacity" },
    { headerName: "Commodity", field: "Commodity" },
    { headerName: "Sold", field: "Sold" },
    { headerName: "State", field: "State" },
    { headerName: "Storage Name", field: "Storage_Name" },
    { headerName: "Crop Year", field: "Crop_year" },
    { headerName: "Entity_Name", field: "Entity_Name" },
    { headerName: "Distance To Nearest City", field: "Distance_To_Nearest_City" }
];


    this.colDef = [
      { headerName: "Select", checkboxSelection: true,
       field: "actions", width: 200,  editable: true },
      {
        headerName: "Group Name", field: "Group_Name", onCellClicked: (params) => {
          this.editGroupMember(params);
        },
        cellRenderer: (params) => `<a>${params.value}</a>`, cellStyle: (params) => {
          return { cursor: 'pointer', color: 'blue' }
        }
      } ,
      { headerName: "Date Created", field: "DateCreated" },
      { headerName: "Last Modified Date", field: "LastModifiedDate" },
      { headerName: "Modified By", field: "ModifiedBy" },
  ];
    this.columnDefs1 = [
      { headerName: "Select", checkboxSelection: true,
       headerCheckboxSelection: true,
       field: "actions",
       width: 80,
       pinned: true
      },
      {
        headerName: "First Name", field: "first_name"
      } ,
      { headerName: "Last Name", field: "last_name" },
      { headerName: "Entity", field: "entity_id" },
      { headerName: "NGR", field: "ngr" },
      { headerName: "Phone", field: "office_ph" },
      { headerName: "State", field: "state" },
  ];
  this.columnDefs2 = [

      { headerName: "Action", field: "attrs", width: 100,
        suppressSorting: true,
        suppressMenu: true,
        pinned: true
      },
      { headerName: "Type", field: "sample" },
      { headerName: "Field Name", field: "wheat" },
      { headerName: "Operator", field: "durum" },
      { headerName: "Value", field: "feed" }

  ];

  this.columnDefs3 = [

      { headerName: "User Type", field: "User_type" },
      { headerName: "Contact", field: "Contact" },
      { headerName: "Trading Name", field: "TradingName", },
      { headerName: "Phone", field: "Phone" },
      { headerName: "Email", field: "Email" },
      { headerName: "Mobile Phone", field: "MobilePhone" },
      { headerName: "State", field: "State" }

  ];

  this.columnDefsFarmRgn = [
      { headerName: "Market Zone", field: "MarketZone" },
      { headerName: "Farming Region", field: "FarmingRegion" }
  ];

  this.frameworkComponents = {
    'editdeletecellrenderer': EditDeleteRendererComponent
  }

  this.context = { componentParent: this };
  this.columnDefs4 = [

      { headerName: "Activity Type", field: "activity_type" },
      { headerName: "Start Date", field: "start_date" },
      { headerName: "Start Time", field: "start_time" },
      { headerName: "Schedule With", field: "schedule_with", },
      { headerName: "Regarding", field: "regarding" },
      { headerName: "Notes", field: "notes" },
  ];


  this.storeInvColDef = [

      { headerName: "Entity Name", field: "Entity_Name" },
      { headerName: "Storage Name", field: "Storage_Name" },
      { headerName: "Storage Type", field: "Storage_Type" },
      { headerName: "Storage Name Capacity", field: "Storage_Name_Capacity", },
      { headerName: "Distance To Nearest City", field: "Distance_To_Nearest_City" },
      { headerName: "City", field: "City" },
      { headerName: "State", field: "State" },
      { headerName: "Post Code", field: "Post_Code" },
      { headerName: "Post Code", field: "Post_Code" },
      { headerName: "Commodity", field: "Commodity" },
      { headerName: "Crop year", field: "Crop_year" },
      { headerName: "Grade", field: "Grade" },
      { headerName: "Unsold", field: "Unsold" },
      { headerName: "Sold", field: "Sold" },
      { headerName: "Target Price", field: "Target_Price" },
  ];

  this.columnDefs5 = [

      { headerName: "Market Zones", field: "attrs", width: 100 },
      { headerName: "Mt Mclaren-Central QLD", field: "attrs", width: 100 },
      { headerName: "Emerald-Central QLD", field: "sample" },
      { headerName: "Burnett", field: "wheat" },
      { headerName: "Brisbane", field: "durum", },
      { headerName: "Darling Downs", field: "feed" },
      { headerName: "Northern Downs", field: "malt" }
  ];



  this._values1 = ["Group Management", "Notification Management", "Master Data","Confirmation Redirection","Contact Management","To Do Tasks","Inventory Updates","Entity Report Screens"];
  this._values2 = [];


   }


   ffsInvoice;
  ngOnInit() {
    
    this.salesPurchase.contractPrice = 0;
    this.firstDropDownChanged('Group Management');
    this.showActionPerformed = false;
    this.showGroupMgmtButtons = true;
    this.getBuyerTradingName();
    this.getSellerTradingName();
    this.getBrokerOffice();
    
    this.ffsInvoice = [
      {id:1, name:"Seller"},
      {id:1, name:"Buyer"}
    ]
  }

  tabs = [
    {title: 'Dynamic Title 1', content: 'Dynamic content 1', active: true, disabled: false},
    {title: 'Dynamic Title 2', content: 'Dynamic content 2', active: false, disabled: true},
    {title: 'Dynamic Title 2', content: 'Dynamic content 3', active: false, disabled: true},
  ];

  getBrokerOffice() {
    let url = serviceUrl + "AgvantageGetBrokerOfficeSB/AgvantageGetBrokerOfficeRestPS/api/v1/GetBrokerOffice?broker_name=Agvantage Commodities Pty Ltd";
    this.http.get(url).subscribe((data) => {
        this.brokerOffices = data.json() ? data.json().broker_offices.broker_office : [];
    }),
        (err) => {
            this.brokerOffices = [];
        }
}

  selectNextTab(index) {
    this.tabs[index].active = true;
    this.tabs[index].disabled = false;
  }

  selectBackTab(index) {
    this.tabs[index].active = true;
    // this.tabs[index].disabled = false;
  }

  firstDropDownChanged(val: any) {
    console.log(val);

    if (val == "Group Management") {
      this.showActionPerformed = false;
      this.showGroupMgmtButtons = true;
      this.showToDoTaskButtons = false;
      this.showMasterDataButtons = false;
      this.showNotificationButtons = false;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = true;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this._values2 = ["Create Group", "Edit Group", "Duplicate Group"];
    }
    else if (val == "Notification Management") {
      this.showActionPerformed = false;
      this.showGroupMgmtButtons = false;
      this.showToDoTaskButtons = false;
      this.showMasterDataButtons = false;
      this.showNotificationButtons = true;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = true;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this._values2 = ["Email", "Edit Email Configuration", "SMS"];
    }
    else if (val == "Master Data") {
      this.showActionPerformed = false;
      this.showGroupMgmtButtons = false;
      this.showNotificationButtons = false;
      this.showToDoTaskButtons = false;
      this.showMasterDataButtons = true;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = false;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this._values2 = ["Ability to edit dropdowns", "Add destination", "Edit region mapping"];
    }
    else if (val == "Confirmation Redirection") {
      this.showActionPerformed = true;
      this.showGroupMgmtButtons = false;
      this.showNotificationButtons = false;
      this.showToDoTaskButtons = false;
      this.showMasterDataButtons = false;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = false;

      this.showAllSalesPurchase = false;
      this.showAllContacts=true;


      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;


      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this._values2 = ["Create Confirmation", 'Edit "ID Status", "Contact Frequency" & "Priority"'];
      this.openViewContactModal();
    }
    else if (val == "Contact Management") {
      this.showActionPerformed = true;
      this.showGroupMgmtButtons = false;
      this.showNotificationButtons = false;
      this.showToDoTaskButtons = false;
      this.showMasterDataButtons = false;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this._values2 = ["Record History","View History Records"];
    }
    else if (val == "To Do Tasks") {
      this.showActionPerformed = false;
      this.showGroupMgmtButtons = false;
      this.showNotificationButtons = false;
      this.showMasterDataButtons = false;
      this.showToDoTaskButtons = true;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this._values2 = ["To Do Tasks"];
    }
    else if (val == "Inventory Updates") {
      // this.showActionPerformed = true;
      this.showGroupMgmtButtons = false;
      this.showNotificationButtons = false;
      this.showToDoTaskButtons = false;
      this.showMasterDataButtons = false;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.showAllContacts=true;
      this._values2 = ["Add Inventory","View Inventory","View Breakeven","View Sales Position & M2M","View Production & Sales Forcast","View Favourites","View Sales & Purchases"];
      this.openViewContactModal();
    
    }
    else if (val == "Entity Report Screens") {
      this.showActionPerformed = true;
      this.showGroupMgmtButtons = false;
      this.showNotificationButtons = false;
      this.showToDoTaskButtons = false;
      this.showMasterDataButtons = false;
      this.showViewHistoryGrid = false;
      this.showBreakEvenGrid = false;
      this.showGroupGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this._values2 = ["Sales & Purchases","Contacts","Favourites","Breakeven","Inventory","Sales position & m2m"];
    }
    else {
      this._values2 = [];
    }
  }

  actionPerformed;
  showViewHistoryGrid = false;
  showGroupGrid = true;
  showBreakEvenGrid = false;
  showGetInvGrid = false;
  showGetSalesM2MGrid = false;
  showProdSaleFrcstGrid = false;
  showViewFav = false;
  showViewRecHistr = false;
  showViewSalesPurchase = false;
  showViewContrOnFarm = false;
  showAllSalesPurchase = false;
  showAllContacts=false;
  showAllFavourites=false;
  showAllBreakEven=false;
  showAllInventory=false;
  showAllSalesM2M=false;
  showDocument = false;
  selectedActionPerformed(){
    console.log("--- selected action: ",this.actionPerformed);
    if(this.actionPerformed === 'Edit "ID Status", "Contact Frequency" & "Priority"')
    {
      // this.showAllSalesPurchase = true;
      this.showAllSalesPurchase = false;
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllContacts= true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openEditIDStatus();
    }
    else if(this.actionPerformed === 'Create Confirmation')
    {
      this.showAllSalesPurchase = false;
      // this.showAllSalesPurchase = true;
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openSalesAndPurchaseModal();

    }
    else if(this.actionPerformed == "View History Records"){
      this.showViewHistoryGrid = true;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.getViewHistoryGridData();
    }else if(this.actionPerformed == "Record History"){
      this.showViewHistoryGrid = true;
      this.recordHistoryModal.show();
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.getViewHistoryGridData();
      this.getHistoryType()
      this.getHistoryResult();
    }
    else if(this.actionPerformed == "Add Inventory"){
      // this.showAddInventory = true;
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openAddInventoryModal();
    }
    else if(this.actionPerformed == "View Breakeven"){
      this.showAllContacts=true;
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openBreakEvenModal();
      
    }else if(this.actionPerformed == "View Inventory"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      // this.showGetInvGrid = true
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openViewInventoryModel();
    }
    else if(this.actionPerformed == "View Sales Position & M2M"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openSalesPosM2MModal();
    }
    else if(this.actionPerformed == "View Production & Sales Forcast"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openSalesForcastModel();

    }
    else if(this.actionPerformed == "View Favourites"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openViewFavouriteModal();

    }
    else if(this.actionPerformed == "Record History/Notes about the Contact for the subscribed & unsubscribed User"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = true;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.getViewRecordHistrNotesGridData();
    }
    else if(this.actionPerformed == "View Sales & Purchases"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openViewSalesPurchaseModal();
    }
    else if(this.actionPerformed == "View Contractors on farm"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openViewContractorModal();

    }
    else if(this.actionPerformed == "Sales & Purchases"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showAllSalesPurchase = true;
      this.showViewContrOnFarm = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openAllSalesPurchaseModal();

    }
    else if(this.actionPerformed == "Contacts"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=true;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openViewContactModal();

    }
    else if(this.actionPerformed == "Favourites"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=true;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openFavouritesModal();

    }
    else if(this.actionPerformed == "Breakeven"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=true;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openViewBreakevenModal();


    }
    else if(this.actionPerformed == "Inventory"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=true;
      this.showAllSalesM2M=false;
      this.showDocument = false;
      this.openInventoryModal();

    }
    else if(this.actionPerformed == "Sales position & m2m"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=true;
      this.showDocument = false;
      this.openAllSalesPosM2MModal();

    }
    else if(this.actionPerformed == "Add / Remove document"){
      this.showViewHistoryGrid = false;
      this.showGroupGrid = false;
      this.showBreakEvenGrid = false;
      this.showGetInvGrid = false;
      this.showGetSalesM2MGrid = false;
      this.showProdSaleFrcstGrid = false;
      this.showViewFav = false;
      this.showViewRecHistr = false;
      this.showViewSalesPurchase = false;
      this.showViewContrOnFarm = false;
      this.showAllSalesPurchase = false;
      this.showAllContacts=false;
      this.showAllFavourites=false;
      this.showAllBreakEven=false;
      this.showAllInventory=false;
      this.showAllSalesM2M=false;
      this.showDocument = true;
    }
  }


  place;
  getAddress(place: Object) {
    console.log("Address", place);
    this.place = place;
  }

  setCity(event){

    this._ngZone.run(() => this.addDestinationData.U_A_CITY=event)

  }

  onGridReadyFarmRgn(params) {
    this.gridApiFarmRgn = params.api;
    this.gridColumnApiFarmRgn = params.columnApi;
    this.setFarmRgnGrid();
  }

  setFarmRgnGrid(){
    this.gridOptionsFarmingRegion.api.setRowData([]);
    //setTimeout(()=>{this.gridOptionsTruck.api.sizeColumnsToFit();});
  }

  load=0; farmRgnRowData=[];
  addRowFarmRgnGrid() {
    this.load=this.load+1;
    let obj = {
      MarketZone: this.addDestinationData.MarketZone.PortZone_ID,
      FarmingRegion: this.addDestinationData.FarmingRegion,
    }
    this.gridApiFarmRgn.updateRowData({ add: [obj] });
    this.farmRgnRowData.push(obj);
  }

  onGridReady1(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }

  onGridReady2(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;


  }
  onGridReady3(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
  }
  onGridReady4(params) {
    this.gridApi = params.api;
    this.gridColumnApi = params.columnApi;
    this.getToDoTask();
  }

  // @Output() openEditDialog = new EventEmitter();
  methodFromParentEdit(params){
    console.log("from grid to componenet----",params.data);
    // this.openEditDialog.emit(params.data);
  }
  // @Output() openDeleteDialog = new EventEmitter();
  methodFromParentDelete(params) {
    //console.log(this.gridOptions.api.getSelectedRows());
    // this.openDeleteDialog.emit(params.data);
    this.delModal.show();
  }

  closeDeleteDialog(evnt){
    //close delete dialog
    this.delModal.hide();
  }

  deleteInventory() {



    // this.headers = new Headers({
    //   'Content-Type': 'application/json; charset=UTF-8',
    //   'Accept': 'application/json'
    // });
    // this.options = new RequestOptions({ headers: this.headers });



    // let url = serviceUrl + "AgvantageEntityAddFavouritesSB/AgvantageEntityManageFavouritesRestPS/api/V1/ManageFavourites";

    // this.http.post(url,{Favourite_id : this.favourites.Favourite_id}, this.options).subscribe(
    //   (data) => {
    //     this.spinnerService.hide();
    //     console.log("del fav res: ", data);
    //     this.delModal.hide();
    //     this.broadCaster.broadcast('Delete Favorite Event', "Deleted Succesfully");
    //     // if(this.editFav){
    //     //   this.broadCaster.broadcast('Update Favorite Event', "Updated Succesfully");
    //     // }else{
    //     //   this.broadCaster.broadcast('Save Favorite Event', "Saved Succesfully");
    //     // }

    //   },
    //   (err) => {
    //     console.log("del contractor err: ", err);
    //   }
    // );
   }

  createGroup() {
    console.log("group created successfully");
    this.createGroupModalTS.show();
  }

  closeCreateGroup() {
    console.log("group created successfully");
    this.createGroupModalTS.hide();
    this.createGroupForm.reset();
  }

  duplicateGroup() {
    console.log("duplicate group created successfully");
    this.duplicateGroupModalTS.show();
  }

  sms() {
    console.log("sms created successfully");
    this.smsModal.show();
  }

  email() {
    console.log("email created successfully");
    this.emailModal.show();
    this.emailFormData.from = "support@agnalysis.io";
  }

  closeemail() {
    console.log("email created successfully");
    this.emailModal.hide();
    this.emailFormData.reset();
  }

  editConfiguration() {
    console.log("editConfiguration created successfully");
    this.editConfModal.show();
  }

  openAddMember(){
    this.createAddMember.show();
  }

  todoTask(){
    this.todoTaskModel.show();
  }

  openAddToDoTask(){
    this.addToDoTaskModal.show();
    this.getTODOSheduleWithList();
  }

  openAddDestination(){
    this.addDestinationModal.show();
    
  }
  
  closeAddDestinationModal(){
    this.addDestinationModal.hide();
  }

  openEditRegionMapping(){
    this.editRegionMappingModal.show();
  }

  storageAndInv(){
    this.storageAndInvModel.show();

  }

  closeAddMember(){
    this.createAddMember.hide();
  }

  closeToDoTask(){
    this.todoTaskModel.hide();
  }

  closeStorageAndInv(){
    this.storageAndInvModel.hide();
  }

  openLocationMap(){
    this.locationMapModal.show();
  }

  closeLocationMap(){
    this.locationMapModal.hide();
  }

  openEditIDStatus(){
    this.editIDStatusModal.show();
  }

  closeEditIDStatus(){
    this.editIDStatusModal.hide();
  }

  openAddInventoryModal(){
    this.addInventoryModal.show();
  }

  closeAddInventoryModal(){
    this.addInventoryModal.hide();
  }

  openBreakEvenModal(){
    this.breakEvenModel.show();
      this.getBreakEvenGridData();
  }

  closeBreakEvenModal(){
    this.breakEvenModel.hide();
  }

  openSalesPosM2MModal(){
    this.salesPosM2mModel.show();
    this.getViewSalesM2MGridData();
  }

  closeSalesPosM2mModel(){
    this.salesPosM2mModel.hide();
  }

  openSalesForcastModel(){
    this.salesForcastModel.show();
    this.getProdSaleFrcstGridData();
  }

  closeSalesForcastModel(){
    this.salesForcastModel.hide();
  }

  openViewFavouriteModal(){
    this.viewFavouriteModal.show();
    this.getViewFavGridData();
  }

  closeViewFavouriteModal(){
    this.viewFavouriteModal.hide();
  }


  openSalesAndPurchaseModal(){this.salesAndPurchaseModal.show()}
  closeSalesAndPurchaseModal(){this.salesAndPurchaseModal.hide()}

  openViewSalesPurchaseModal(){
    this.viewSalesPurchaseModal.show()
    this.getViewSalesPurchaseGridData();
  }

  closeViewSalesPurchaseModal(){this.viewSalesPurchaseModal.hide()
    this.getViewSalesPurchaseRowData = [];
  }

  openAllSalesPurchaseModal(){
    // this.SalesAndPurchaseModal.show()
    this.getAllSalesPurchaseGridData();
  }

  closeAllSalesPurchaseModal(){this.SalesAndPurchaseModal.hide()}

  openViewContractorModal(){this.ViewContractorModal.show()
    this.getViewContrOnFarmGridData();}
  closeViewContractorModal(){this.ViewContractorModal.hide()}

  openViewContactModal(){
    // this.ViewContactModal.show()
    this.getAllContactsGridData();
  }

  closeViewContactModal(){this.ViewContactModal.hide()}

  openFavouritesModal(){
    // this.FavouritesModal.show()
    this.getAllFavouritesGridData();
  }
  closeFavouritesModal(){this.FavouritesModal.hide()}

  openViewBreakevenModal(){
    // this.viewBreakevenModal.show()
    this.getAllBreakEvenGridData();
  }
  closeViewBreakevenModal(){this.viewBreakevenModal.hide()}

  openInventoryModal(){
    // this.InventoryModal.show()
    this.getAllInventoryGridData();
  }
  closeInventoryModal(){this.InventoryModal.hide()}

  openAllSalesPosM2MModal(){
    // this.SalesPosM2MModal.show()
    this.getAllSalesM2MGridData();
  }
  closeAllSalesPosM2MModal(){this.SalesPosM2MModal.hide()}
  
  openViewInventoryModel(){
    this.ViewInventoryModel.show();
    // this.SalesPosM2MModal.show()    
    this.getInventoryGridData();
  }

  closeViewInventoryModel(){this.ViewInventoryModel.hide()}


  
  summaryArr = []; selectedUserId;
  updateSummary(event){
    console.log("-- SA summaryArr", event.api.getSelectedNodes());   
    this.showActionPerformed = true;
    this.summaryArr= event.api.getSelectedNodes();
        console.log("-- SA summaryArr",this.summaryArr[0].data); 
        this.salesPurchase.sellerTradingName = this.summaryArr[0].data.entity_name;  
        this.selectedUserId = this.summaryArr[0].data.Id;
        console.log("-- Selected user id",this.selectedUserId);
        this.getSellerRepresentative();

    }

  selectedIDs = [];
  onSelectionMember(param) {
    let selectedRows = this.gridOptions1.api.getSelectedRows();
    this.selectedIDs = [];
    for (let selectedrow of selectedRows) {
      this.selectedIDs.push(selectedrow.Id);
    };
  }

  onSelectionChanged(event) {
    var summaryArr = event.api.getSelectedNodes();
    // this.pushSelectedContact.emit(summaryArr);

    // console.log(event.api.getSelectedNodes());
  }

   holdCorrectRow = [];
  // selectedIDs = [];
  onGroupRowSelection(param) {
    // let selectedRows = this.gridOptions1.api.getSelectedRows();
    // console.log("-- group selectedRows", selectedRows);
    console.log("-- group param", param.data);
    this.holdCorrectRow.push(param.data);
    this.creatGroupFormData.groupname = this.holdCorrectRow[0].Group_Name;
    this.creatGroupFormData.description = this.holdCorrectRow[0].Group_Description;
    this.dupGroupFormData.groupname = this.holdCorrectRow[0].Group_Name;
    this.dupGroupFormData.description = this.holdCorrectRow[0].Group_Description;
    this.emailFormData.groupname = this.holdCorrectRow[0].Group_Name;
    // this.selectedIDs = [];
    // for (let selectedrow of selectedRows) {
      // this.selectedIDs.push(selectedrow.Id);
    // };
    if(this.holdCorrectRow.length == 2){
      this.holdCorrectRow = [];
    }


  }

  selectedGroupRow;
  onCellClicked($event) {

    console.log("--- SA Row Event:",$event);
    if ($event.colDef.field == "first_name") {
        // this.viewContactDetails.show();
        this.selectedGroupRow = $event.data;
        console.log("--- this.selectedGroupRow:",this.selectedGroupRow);
        // this.storageid = $event.data.storage_id;
        // let url = serviceUrl+"AgvInvGetStorageByIDSB/AgvInvGetStorageByIDPS/api/v1/getStoreByID?S_ID=" + this.storageid;
        // this.http.get(url).subscribe((data) => {
        //     this.StorageData = data.json();
        //     // this.selectedCommodity1=this.commodities[0].C_NAME;
        // })
        // this.noBDModal1.show();
    }

}



  getgroups(){
    this.gridOptions.api.showLoadingOverlay();
    // https://129.154.71.56:9074/AgvantageAdminGetGroupsSB/AgvantageAdminGetGroupsRestPS/api/v1/GetGroups?input=1
    let url = `${serviceUrl}AgvantageAdminGetGroupsSB/AgvantageAdminGetGroupsRestPS/api/v1/GetGroups?input=1`;
    this.http.get(url).subscribe(data=>{
      this.rowData = data.json()?data.json().Groups : [];
      this.gridOptions.api.hideOverlay();
    },
    err=>{
      this.rowData = [];
    })
  }

  getAllGroupsMember(){
    this.gridOptions.api.showLoadingOverlay();

    let url = `${serviceUrl}AgvantageAdminGetAllContacts/AgvantageAminGetAllContactsRestPS/api/v1/GetAllContacts?input=1`;
    this.http.get(url).subscribe(data=>{
      // console.log('---', data.json.Contacts.Contact);
      this.rowDataAllGroup = data.json()?data.json().Contacts.Contact : [];
      this.gridOptions.api.hideOverlay();
    },
    err=>{
      this.rowDataAllGroup = [];
    })
  }

  schList:any = [];
  getTODOSheduleWithList(){
    this.spinnerService.show();
    let url = `${serviceUrl}AgvantageAdminGetAllContacts/AgvantageAminGetAllContactsRestPS/api/v1/GetAllContacts?input=1`;
    this.http.get(url).subscribe(data=>{
      this.spinnerService.hide();
      let sheduleWithData = data.json()?data.json().Contacts.Contact : [];
      sheduleWithData.forEach(element => {
        this.schList.push(element);
      });
    },
    err=>{
      this.schList = [];
    })
  }



  getAllInvRecords(){
    this.gridOptions.api.showLoadingOverlay();

    let url = `${serviceUrl}AgvantageAdminGetInventorySB/AgvantageAdminGetInventoryRestPS/api/v1/GetInventory?request=true`;
    this.http.get(url).subscribe(data=>{
      // console.log('---', data.json.Contacts.Contact);
      this.rowDataAllInvRec = data.json()?data.json().Inventory : [];
      this.gridOptions.api.hideOverlay();
    },
    err=>{
      this.rowDataAllInvRec = [];
    })
  }

  getToDoTask(){
    let url = `${serviceUrl}TODOTasks/ToDoTaskService/api/v1/GetToDoTasks?u_id=`+this.currentUser.USER.U_ID;
    this.gridOptionsToDo.api.showLoadingOverlay();
    this.http.get(url).subscribe(data=>{
      this.rowDataToDoTask = data.json()?data.json().ToDoTasksDetails.ToDoTaskDetails :  [];
      this.gridOptionsToDo.api.setRowData(this.rowDataToDoTask);
      this.gridOptionsToDo.api.hideOverlay();
    },
    err=>{
      this.rowDataToDoTask = [];
    })
  }

  states;
  getStates() {
    let url = serviceUrl + "AgvantageGetStatesSB/AgvantageGetStatesRestPS/api/v1/getStates?country_id=1"
    this.http.get(url).subscribe((data) => {
      this.states = data.json()?data.json().getStates:[];
    })
  }

  farmingRegions;
  getFarmingRegions() {
    // this.spinnerService.show();
    // let url = serviceUrl + "AgvantageGetFarmingRegionsSB/AgvantageGetFarmingRegionsRestPS/api/v1/GetFarmingRegions?state_id=" + this.addDestinationData.State + "&pz_id=" + this.addDestinationData.MarketZone.MZ_ID;
    // this.http.get(url).subscribe((data) => {
    //   this.spinnerService.hide();
    //   this.farmingRegions = data.json()?data.json().getFarmingRegions:[];

    // })
    this.farmingRegions = [{FarmingRegion_Name : this.salesPurchase.sellerRepresentative.FarmingRegionId}];
  }
  
  addDestFarmRgns;
  getFarmRegnsForAddDestination() {    
    this.spinnerService.show();
    let url = serviceUrl + "AgvantageGetFarmingRegionsSB/AgvantageGetFarmingRegionsRestPS/api/v1/GetFarmingRegions?state_id=" + this.addDestinationData.State;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.addDestFarmRgns = data.json()?data.json().getFarmingRegions:[];

    })
    
  }

  portZones;
  getPortZones() {
    this.spinnerService.show();
    let url = serviceUrl + "AgvantageGetMarketZoneListSB/AgvantageGetMarketZoneListRestPS/api/v1/GetMarketZone?state_id=" + this.addDestinationData.State +"&NTP_id="+this.addDestinationData.NTP;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.portZones = data.json()?data.json().MarketZone:[];
    })
  }



  truckTypes;
  getTruckTypes() {
    let url = `${serviceUrl}AgvantageGetTruckAccessSB/AgvantageGetTruckAccessRestPS/api/v1/GetTruckAccess`;
    this.http.get(url).subscribe((data) => {
      this.truckTypes = data.json() ? data.json().TruckType : [];
    }, (err) => {
      this.truckTypes = [];
    })
  }

  getDestinationTypeList;
  getDestinationType(){
    let url = `${serviceUrl}AgvantageGetSNPDestinationTypeSB/AgvantageGetSNPDestinationTypeRestPS/api/v1/GetDestinationType?request=true`;
    this.http.get(url).subscribe((data) => {
      this.getDestinationTypeList = data.json() ? data.json().DestinationType : [];
    }, (err) => {
      this.getDestinationTypeList = [];
    })
  }

  getDestinationOwnerList;
  getDestinationOwner(){

    let url = `${serviceUrl}AgvantageGetSNPDestinationOwnerListSB/AgvantageGetDestinationOwnerRestPS/api/v1/GetDestinationOwner?state_id=${this.addDestinationData.State} `;
    this.http.get(url).subscribe((data) => {
      this.getDestinationOwnerList = data.json() ? data.json().DestinationOwner : [];
    }, (err) => {
      this.getDestinationOwnerList = [];
    })
  }
  getTransportModeList;
  getTransportMode(){
    let url = `${serviceUrl}AgvantageGetTransportModeSB/AgvantageGetTransportModeRestPS/api/v1/GetTransportMode?request=trueAgvantageGetTransportModeSB/AgvantageGetTransportModeRestPS/api/v1/GetTransportMode?request=true`;
    this.http.get(url).subscribe((data) => {
      this.getTransportModeList = data.json() ? data.json().Transport_Mode : [];
    }, (err) => {
      this.getTransportModeList = [];
    })
  }

  getNTPList;
  getNTP(){

    let url = `${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?state_id=${this.addDestinationData.State}&storage_type=NTP`;
    this.http.get(url).subscribe((data) => {
      this.getNTPList = data.json() ? data.json().getSites : [];
    }, (err) => {
      this.getNTPList = [];
    })
  }

  HistoryTypeList;
  getHistoryType(){
    this.spinnerService.show();
    let url = `${serviceUrl}AgvantageAdminGetHistoryTypeSB/AgvantageAdminGetHistoryTypeRestPS/api/v1/GetHistoryType?request=true`;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.HistoryTypeList = data.json() ? data.json().Typelist : [];
    }, (err) => {
      this.HistoryTypeList = [];
    })
  }

  HistoryResultList;
  getHistoryResult(){
    this.spinnerService.show();
    let url = `${serviceUrl}AgvantageAdminGetHistoryResultSB/AgvanatgeAdminGetHistoryResultPS/api/v1/GetHistoryResult?request=true`;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.HistoryResultList = data.json() ? data.json().Results : [];
    }, (err) => {
      this.HistoryResultList = [];
    })
  }

  viewHistoryRecordRowData;customColumns=[];
  viewBreakEvenRowData;
  getViewHistoryGridData(){

    let url = `${serviceUrl}AgvantageAdminGetHistoryNotesSB/AgvantageAdminGetHistoryNotesPS/api/v1/GetHistoryNotes?Contact_User_ID=`+this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      this.viewHistoryRecordRowData = data.json() ? data.json().HistoryNotes : [];
      // this.gridOptionsViewHistoryGrid.api.hideOverlay();
    }, (err) => {
      this.viewHistoryRecordRowData = [];
    })
  }

  submitRecHistoryMessage;
  submitRecordHistoryNotes(){
    let url = `${serviceUrl}AgvantageAdminRecordHistoryNotesSB/AgvantageAdminRecordHistoryNotesRestPS/api/v1/AddRecordNotes`;
    let params = {
      "Type_ID": this.addToDoTaskData.historyType,
      "Result_ID": this.addToDoTaskData.historyResult,
      "Contact_User_ID": this.addToDoTaskData.contact,
      "Date": this.addToDoTaskData.date,
      "Time": this.addToDoTaskData.rhTime,
      "RecordManager_U_ID": this.addToDoTaskData.recMgr,
      "Task_Date": this.addToDoTaskData.taskDate,
      "Regarding": this.addToDoTaskData.regarding,
      "Reminder_Date": this.addToDoTaskData.rhRemDate,
      "Reminder_Time": this.addToDoTaskData.remTime,
      "Details": this.addToDoTaskData.details,

    }
    console.log("--- params", params);
    this.spinnerService.show();
    this.http.post(url, params).subscribe((data) => {
      this.spinnerService.hide();
      this.responsemessage = data.json() ? data.json().result : [];
      this.recordHistoryModal.hide();
      setTimeout(() => {
      this.editAddResmessage = false;
      }, 2000);
      this.getViewHistoryGridData();
    }, (err) => {
      this.submitRecHistoryMessage = "Some error occured";
      console.log(err.json());
    })
  }

  getBreakEvenGridData(){
    this.spinnerService.show();
    let url = `${serviceUrl}AgvantageGetSummaryBreakevenSB/AgvantageGetSummaryBreakevenRestPS/api/v1/GetSummaryBreakeven?user_id=`+this.selectedUserId;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.viewBreakEvenRowData = data.json() ? data.json().BreakevenResponse : [];
    }, (err) => {
      this.viewBreakEvenRowData = [];
    })
  }



  inventoryRowData;
  getInventoryGridData(){    
    this.spinnerService.show();
    let url = `${serviceUrl}AgvInvGetCommodity/AgvInvGetCommPS/api/v1/getInvComm?U_ID=`+this.selectedUserId;
    this.http.get(url).subscribe((data) => {  
      this.spinnerService.hide();    
      this.inventoryRowData = data.json() ? data.json().comStore : [];
    }, (err) => {
      this.inventoryRowData = [];
    })
  }

  getProdSaleFrcstRowData;
  getProdSaleFrcstGridData(){
    let url = `${serviceUrl}AgvantageGetProductionAndSalesForecast/AgvantageGetProductionSalesForecastRestPS/api/v1/GetSalesForecast?user_id=`+this.selectedUserId;
    // let url = `${serviceUrl}AgvantageGetProductionAndSalesForecast/AgvantageGetProductionSalesForecastRestPS/api/v1/GetSalesForecast?user_id=`+this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      this.getProdSaleFrcstRowData = data.json() ? data.json().SalesForecast.commodityDetails : [];
    }, (err) => {
      this.getProdSaleFrcstRowData = [];
    })
  }

  getViewFavRowData;
  getViewFavGridData(){
    let url = `${serviceUrl}AgvEntityGetFavouritesSB/AgvEntityGetFavouritesRestPS/api/v1/GetFavourites?User_id=`+this.selectedUserId;
    this.http.get(url).subscribe((data) => {
      this.getViewFavRowData = data.json() ? data.json().Favourite : [];
    }, (err) => {
      this.getViewFavRowData = [];
    })
  }

  getViewRecHistrRowData;
  getViewRecordHistrNotesGridData(){
    let url = `${serviceUrl}AgvantageAdminGetHistoryNotesSB/AgvantageAdminGetHistoryNotesPS/api/v1/GetHistoryNotes?contact_user_id=`+this.currentUser.USER.U_ID;

    this.http.get(url).subscribe((data) => {
      this.getViewRecHistrRowData = data.json() ? data.json().HistoryNotes : [];
    }, (err) => {
      this.getViewRecHistrRowData = [];
    })
  }

  getViewSalesPurchaseRowData;
  getViewSalesPurchaseGridData(){

    let url = `${serviceUrl}SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseHistoryByUserId?U_ID=`+this.selectedUserId+`&U_Type=`+this.currentUser.USER.U_TYPE;

    this.http.get(url).subscribe((data) => {
      this.getViewSalesPurchaseRowData = data.json() ? data.json().SaleHistory : [];
    }, (err) => {
      this.getViewSalesPurchaseRowData = [];
    })
  }

  getViewContrOnFarmRowData;
  getViewContrOnFarmGridData(){

    let url = `${serviceUrl}ContractorOnFarmSB/ContractorsOnFarmPS/api/v1/GetAllContractors?U_ID=`+this.currentUser.USER.U_ID;

    this.http.get(url).subscribe((data) => {
      this.getViewContrOnFarmRowData = data.json() ? data.json().Contractors.Contractor : [];
    }, (err) => {
      this.getViewContrOnFarmRowData = [];
    })
  }

  getAllSalesPurchaseRowData;
  getAllSalesPurchaseGridData(){

    let url = `${serviceUrl}AgvantageAdminGetSalesAndPurchasesSB/AgvantageAdminGetSalesHistoryRestPS/api/v1/GetSalesHistory`;

    this.http.get(url).subscribe((data) => {
      this.getAllSalesPurchaseRowData = data.json() ? data.json().SaleHistory : [];
    }, (err) => {
      this.getAllSalesPurchaseRowData = [];
    })
  }

  getAllContactsRowData;
  getAllContactsGridData(){

    let url = `${serviceUrl}AgvantageAdminGetAllContacts/AgvantageAminGetAllContactsRestPS/api/v1/GetAllContacts?input=true`;

    this.http.get(url).subscribe((data) => {
      this.getAllContactsRowData = data.json() ? data.json().Contacts.Contact : [];
    }, (err) => {
      this.getAllContactsRowData = [];
    })
  }

  getAllFavouritesRowData;
  getAllFavouritesGridData(){

    let url = `${serviceUrl}AgvantageAdminGetFavouritesSB/AgvantageAdminGetFavouritesRestPS/api/v1/GetFavourites?request=true`;

    this.http.get(url).subscribe((data) => {
      this.getAllFavouritesRowData = data.json() ? data.json().FavouriteList : [];
    }, (err) => {
      this.getAllFavouritesRowData = [];
    })
  }

  getAllBreakEvenRowData;
  getAllBreakEvenGridData(){
    let url = `${serviceUrl}AgvantageAdminGetEntityBreakeven/AgvantageAdminGetBreakevenRestPS/api/v1/GetBreakeven?request=true`;
    this.http.get(url).subscribe((data) => {
      this.getAllBreakEvenRowData = data.json() ? data.json().Breakeven : [];
    }, (err) => {
      this.getAllBreakEvenRowData = [];
    })
  }

  getAllInventoryRowData;
  getAllInventoryGridData(){
    let url = `${serviceUrl}AgvantageAdminGetInventorySB/AgvantageAdminGetInventoryRestPS/api/v1/GetInventory?request=true`;
    this.http.get(url).subscribe((data) => {
      this.getAllInventoryRowData = data.json() ? data.json().Inventory : [];
    }, (err) => {
      this.getAllInventoryRowData = [];
    })
  }

  getAllSalesM2MRowData;
  getAllSalesM2MGridData(){
    let url = `${serviceUrl}AgvantageAdminGetSalesPositionM2MSB/AgvantageAdminGetSalesPositionM2MRestPS/api/v1/GetSalesPositionM2M?request=true`;
    this.http.get(url).subscribe((data) => {
      this.getAllSalesM2MRowData = data.json() ? data.json().SalesPosition : [];
    }, (err) => {
      this.getAllSalesM2MRowData = [];
    })
  }

  // viewSalesPosM2MRowData;
  // getSalesPosM2MGridData(){
  //   this.spinnerService.show();

  //   this.http.get(url).subscribe((data) => {
  //     this.spinnerService.hide();
  //     this.viewSalesPosM2MRowData = data.json() ? data.json().BreakevenResponse : [];
  //   }, (err) => {
  //     this.viewSalesPosM2MRowData = [];
  //   })
  // }

  // viewSalesForcastRowData;
  // getSalesForcastGridData(){
  //   this.spinnerService.show();
  //   let url = `${serviceUrl}AgvantageGetProductionAndSalesForecast/AgvantageGetProductionSalesForecastRestPS/api/v1/GetSalesForecast?user_id=`+this.selectedUserId;
  //   this.http.get(url).subscribe((data) => {
  //     this.spinnerService.hide();
  //     this.viewSalesForcastRowData = data.json() ? data.json().SalesForecast : [];
  //   }, (err) => {
  //     this.viewSalesForcastRowData = [];
  //   })
  // }

  getViewSalesM2MRowData;
  getViewSalesM2MGridData(){
    let url = `${serviceUrl}SalesAndM2MPosition/SalesPositionPS/api/v1/GetSalesPositionByCommodity?U_ID=`+this.selectedUserId;
    // let url = `${serviceUrl}SalesAndM2MPosition/SalesPositionPS/GetSalesPositionByCommodity/api/v1/GetSalesPositionByCommodity?U_ID=`+this.currentUser.USER.U_ID;
    this.http.get(url).subscribe((data) => {
      this.getViewSalesM2MRowData = data.json() ? data.json().SalesPosition : [];
    }, (err) => {
      this.getViewSalesM2MRowData = [];
    })
  }

  Destination;
  getDestination(){
    let url = "http://129.154.71.56:9073/SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSaleOriginDestinations?Request=GetSaleOrigin";
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.Destination = data.json()?data.json().SiteOrigin:[];
    })
  }

  showNTPDetails = false;
  saleOriginChange(){
    this.getCommodityOrigin();
    this.getCommodityDestination();
    this.getFarmingRegions();
    console.log("saleOrigin selection...");
    if(this.salesPurchase.saleOrigin==="Nearest Terminal Port (NTP)" && this.salesPurchase.SaleType==="Forward"){
      this.showNTPDetails = true;
    }
  }

  salesTypeList;
  getSalesType() {
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesType?Request=GetSalesType"
    console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesTypeList = data.json() ? data.json().SaleType : [];
    },
      (err) => {
        this.salesTypeList = [];
      });
  }

  salesOriginDestList;
  getSalesOriginDest() {
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSaleOriginDestinations?Request=GetSaleOrigin"
    console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesOriginDestList = data.json() ? data.json().SiteOrigin : [];
    },
      (err) => {
        this.salesOriginDestList = [];
      });
  }


  showCommDestInput=false; showCommDestDDL=true; showTolerance = true;
showCommDestination(){
  if(this.salesPurchase.saleOrigin.Name === "DelieveredDestination"){
    this.showCommDestInput = true;
    this.showCommDestDDL = false;
  }else{
    this.showCommDestInput=false;
    this.showCommDestDDL=true;
  }
  if(this.salesPurchase.saleOrigin.Name==="NTP" && this.salesPurchase.SaleType.Name==="Forward"){
    this.showNTPDetails = true;
    this.showTolerance = false;
    this.getNTPSiteList();
  }else{
    this.showTolerance = true;
    this.showNTPDetails = false;
  }
}

 ntpSiteListData;
  getNTPSiteList(){
    this.spinnerService.show();
    let url=`${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?state_id=1&storage_type=`+this.salesPurchase.saleOrigin.Name;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.ntpSiteListData = data.json() ? data.json().getSites : [];
    },
      (err) => {
        this.ntpSiteListData = [];
      });
  }

  ntpNominatedSiteListData;
  getNTPNominatedSiteList(){
    this.spinnerService.show();
    let url=`${serviceUrl}AgvantageGetNTPNominatedSitesSB/AgvantageGetNTPNominatedSitesRestPS/api/v1/GetNominatedSites?ntp_id=`+this.salesPurchase.ntpNameList.Site_ID;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.ntpNominatedSiteListData = data.json() ? data.json().NominatedSite : [];
    },
      (err) => {
        this.ntpNominatedSiteListData = [];
      });
  }

  onGridReadyTruck(params) {
    this.gridApiTruck = params.api;
    this.gridColumnApiTruck = params.columnApi;
    this.setTruckGrid();
  }

  setTruckGrid(){
    this.gridOptionsTruck.api.setRowData([]);
    //setTimeout(()=>{this.gridOptionsTruck.api.sizeColumnsToFit();});

  }

  ntpLoadData =[];NTPName; NomSites;LocDiff;SitePrice;
  addNTPSites() {
    // this.load=this.load+1;
    // NTP Contract Price - Location Differential = Site Price.
    this.salesPurchase.ntpSitePrice = this.salesPurchase.contractPrice - this.salesPurchase.locDiff;
    console.log("--- this.salesPurchase.ntpSitePrice", this.salesPurchase.ntpSitePrice);
    let obj = {
      NTPName: this.salesPurchase.ntpNameList.Site_Name,
      NomSites: this.salesPurchase.ntpNomSiteList.NominatedSite_Name,
      LocDiff: this.salesPurchase.locDiff,
      SitePrice: this.salesPurchase.ntpSitePrice
    }
    this.gridApiTruck.updateRowData({ add: [obj] });

    this.ntpLoadData.push(obj);
  }

  getLocDiff(){

    this.spinnerService.show();
    let url = `${serviceUrl}GetLocationDifferential/GetLocationDifferentialPS/api/v1/GetLocationDifferential?NTPName=`+this.salesPurchase.ntpNomSiteList.NTP_Name+`&NominatedSite=`+this.salesPurchase.ntpNomSiteList.NominatedSite_Name;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.salesPurchase.locDiff = data.json().Differential;
    })
  }

  buyerTradingNames;
  getBuyerTradingName(){
    let url = `${serviceUrl}GetRegisteredCompaniesSB/GetRegisteredCompaniesPS/api/v1/GetRegisteredCompanies?CompanyType=Buyers`;
    this.http.get(url).subscribe(data=>{
      this.buyerTradingNames = data.json()?data.json().CompanyTradingName : [];
    },err=>{
      this.buyerTradingNames = [];
    })
  }

  sellerTradingNames;
  getSellerTradingName(){
    let url = `${serviceUrl}GetRegisteredCompaniesSB/GetRegisteredCompaniesPS/api/v1/GetRegisteredCompanies?CompanyType=Sellers`;
    this.http.get(url).subscribe(data=>{
      this.sellerTradingNames = data.json()?data.json().CompanyTradingName : [];
    },err=>{
      this.sellerTradingNames = [];
    })
  }

  getSellerRepresentative(){
    let url = `${serviceUrl}GetCompanyMembers/GetCompanyMembersPS/api/v1/GetCompanyMembers?CompanyName=${this.salesPurchase.sellerTradingName}`;
    this.http.get(url).subscribe(data=>{
      this.sellerRepresentatives = data.json()?data.json().Member : [];
    },err=>{
      this.sellerRepresentatives = [];
    })
  }
  salesPurchaseRefDataList;
  tolaranceRefDataList = [];
  tradeDisputeRuleRefDataList = [];
  transferredConfirmedByRefDataList = [];
  transferDoneByRefDataList = [];
  weightTermsRefData = [];
  rCTIRefDataList = [];
  paymentTermsRefData = [];
  gTAContractRefData = [];
  gradeStandardsRefData = [];
  gSTRefData = [];
  deliveryTermsRefData = [];
  arbitrationRuleRefData = [];
  contractTypeRefData = [];
  getSalesAndPurchaseRefDataOriginDest() {
    // http://129.154.71.56:9073/SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=1&SiteOrigin=4&Commodity=Cotton
    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseReferenceData?SaleType=" + this.salesPurchase.SaleType.Name + "&SiteOrigin=" + this.salesPurchase.saleOrigin.Id + "&Commodity=" + this.salesPurchase.commodity
    // console.log();
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.salesPurchaseRefDataList = data.json() ? data.json() : [];
      this.businessCreditRefData = this.salesPurchaseRefDataList.BusinessCreditedRefData ? this.salesPurchaseRefDataList.BusinessCreditedRefData.RefData : [];
      this.tolaranceRefDataList = this.salesPurchaseRefDataList.ToleranceRefData ? this.salesPurchaseRefDataList.ToleranceRefData.RefData : [];
      this.tradeDisputeRuleRefDataList = this.salesPurchaseRefDataList.TradeDisputeRuleRefData ? this.salesPurchaseRefDataList.TradeDisputeRuleRefData.RefData : [];
      this.transferredConfirmedByRefDataList = this.salesPurchaseRefDataList.TransferredConfirmedByRefData ? this.salesPurchaseRefDataList.TransferredConfirmedByRefData.RefData : [];
      this.transferDoneByRefDataList = this.salesPurchaseRefDataList.TransferDoneByRefData ? this.salesPurchaseRefDataList.TransferDoneByRefData.RefData : [];
      this.weightTermsRefData = this.salesPurchaseRefDataList.WeightTermsRefData ? this.salesPurchaseRefDataList.WeightTermsRefData.RefData : [];
      this.rCTIRefDataList = this.salesPurchaseRefDataList.RCTIRefData ? this.salesPurchaseRefDataList.RCTIRefData.RefData : [];
      this.paymentTermsRefData = this.salesPurchaseRefDataList.PaymentTermsRefData ? this.salesPurchaseRefDataList.PaymentTermsRefData.RefData : [];
      this.gTAContractRefData = this.salesPurchaseRefDataList.GTAContractRefData ? this.salesPurchaseRefDataList.GTAContractRefData.RefData : [];
      this.gradeStandardsRefData = this.salesPurchaseRefDataList.GradeStandardsRefData ? this.salesPurchaseRefDataList.GradeStandardsRefData.RefData : [];
      this.gSTRefData = this.salesPurchaseRefDataList.GSTRefData ? this.salesPurchaseRefDataList.GSTRefData.RefData : [];
      this.deliveryTermsRefData = this.salesPurchaseRefDataList.DeliveryTermsRefData ? this.salesPurchaseRefDataList.DeliveryTermsRefData.RefData : [];
      this.arbitrationRuleRefData = this.salesPurchaseRefDataList.ArbitrationRuleRefData ? this.salesPurchaseRefDataList.ArbitrationRuleRefData.RefData : [];
      this.contractTypeRefData = this.salesPurchaseRefDataList.ContractTypeRefData ? this.salesPurchaseRefDataList.ContractTypeRefData.RefData : [];
    },
      (err) => {
        this.salesPurchaseRefDataList = [];
      });
  }

  commodityOriginList;
  getCommodityOrigin() {
    let user_id = this.currentUser.USER.U_A_REGION;
    let url = `${serviceUrl}/AgvantageGetFieldDetailsSB/AgvantageGetFieldDetailsRestPS/api/v1/GetFieldDetails?farm_id=${this.salesPurchase.sellerRepresentative.UserId}`;
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.commodityOriginList = data.json() ? data.json().Storage.Storage_Name : [];
    },
      (err) => {
        this.commodityOriginList = [];
      });
  }

  commodityDestination;
  getCommodityDestination() {
    let url = `${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList?storage_type=${this.salesPurchase.saleOrigin.Name}&state_id=${this.salesPurchase.sellerRepresentative.StateId}`;
    // let url = `${serviceUrl}AgvantageGetSitesSB/AgvantageGetSitesRestPS/api/v1/GetSitesList`;
    this.spinnerService.show();
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      console.log("res", data);
      this.commodityDestination = data.json() ? data.json().getSites : [];
      // this.commodityDestination = data.json() ? data.json().getSites.Market_Zone.DeliveredDestinations : [];
    },
      (err) => {
        this.commodityDestination = [];
      });
  }

  commodities;
  getCommodities() {
    this.spinnerService.show();
    let url = serviceUrl + "AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.commodities = data.json().CommodityCollection.Commodity;
    })
  }


  callGrades() {
    this.getCommodityGrade(this.salesPurchase.commodity);
    this.getSalesAndPurchaseRefDataOriginDest();
  }

  grades;
  getCommodityGrade(id) {
    this.spinnerService.show();
    let url = serviceUrl + "AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.grades = data.json().gradeCollection.grade;
    })
  }

  cropYearList;
  getCropyearList() {
    this.spinnerService.show();
    let url = serviceUrl + 'AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.cropYearList = data.json().CropYear;
    })
  }

  saleHistoryData
  getSaleHistory() {
    let url = `${serviceUrl}SalesAndPurchase/SalesAndPurchasePS/api/v1/GetSalesAndPurchaseHistoryByUserId?U_ID=${this.currentUser.USER.U_ID}&U_Type=Broker/Seller/Buyer`;
    this.http.get(url).subscribe((data) => {
      this.saleHistoryData = data.json() ? data.json().SaleHistory : [];
    },
      (err) => {
        this.saleHistoryData = [];
      });
  }

  idStatusList;
  getIDStatusList() {
    let url = `${serviceUrl}AgvantageGetContactIDStatusSB/AgvantageGetContactIDStatusRestPS/api/v1/GetIDStatus?request=true`;
    this.http.get(url).subscribe((data) => {
      this.idStatusList = data.json() ? data.json().IDStatus : [];
    },
      (err) => {
        this.idStatusList = [];
      });
  }

  statusList;
  getStatus(){
    this.statusList = [
      { key: 'Open', value: 'Open' },
      { key: 'Confirmed', value: 'Confirmed' },
      { key: 'Washed Out - No Confirmation Revenue', value: 'Washed Out - No Confirmation Revenue' },
      { key: 'Washed Out - Keep Confirmation Revenue', value: 'Washed Out - Keep Confirmation Revenue' }
    ];
  }

  contactFrqncyList;
  getContactFrqncyList() {

    let url = `${serviceUrl}AgvantageGetContactFrequencyListSB/AgvantageGetContactFrequencyListRestPS/api/v1/GetContactFrequencyList?request=true`;
    this.http.get(url).subscribe((data) => {
      this.contactFrqncyList = data.json() ? data.json().Frequency : [];
    },
      (err) => {
        this.contactFrqncyList = [];
      });
  }

  priorityList;
  getPriorityList() {
    let url = `${serviceUrl}AgvantageGetPriorityListSB/AgvantageGetPriorityListRestPS/api/v1/GetPriorityList?request=true`;
    this.http.get(url).subscribe((data) => {
      this.priorityList = data.json() ? data.json().Priority : [];
    },
      (err) => {
        this.priorityList = [];
      });
  }

  rowDataSelectedGroupMember;edit_groupID;
  getSelectedGroupMembersById(group_id) {
    this.edit_groupID = group_id;
    this.spinnerService.show();
    let url = `${serviceUrl}AgvantageGetGroupMembersByIDSB/AgvantageGetGroupMembersRestPS/api/v1/GetGroupMembersByID?Group_ID=`+ group_id;
    this.http.get(url).subscribe((data) => {
      this.spinnerService.hide();
      this.rowDataSelectedGroupMember = data.json() ? data.json().Members : [];
    },
      (err) => {
        this.spinnerService.hide();
        this.rowDataSelectedGroupMember = [];
      });
  }

  editGroupFlag = false;
  editGroupMember(params){
    this.editGroupFlag = true;
    this.holdParams = params.value;
    this.editGroupModel.show();
    this.getSelectedGroupMembersById(params.data.Group_ID);
  }

  closeEditAddMember(){
    this.editGroupModel.hide();
  }

  AddMemberIDs(){
    if(this.editGroupFlag){
      this.submitEditAddGroup();
    }else
      this.createAddMember.hide();
  }

  editAddResmessage
  submitEditAddGroup(){
    this.editGroupFlag = false;
    let url = `${serviceUrl}AgvantageAdminManageNotificationGroups/AgvantageAdminManageNotificationGroup/api/v1/ManageGroups`;
    let params = {
        "Group_ID" : this.edit_groupID,
        "ModifiedBy_Uid" : this.currentUser.USER.U_ID,
        "MembersList" : {
        "User_ID" : this.selectedIDs
      }
    }
      console.log("--- params" , params);
      this.spinnerService.show();
    this.http.post(url, params).subscribe((data)=>{
      this.spinnerService.hide();
      this.responsemessage = data.json()?data.json().result:[];
      this.createAddMember.hide();
      setTimeout(()=>{this.editAddResmessage = false;
      },4000);
      this.getSelectedGroupMembersById(this.edit_groupID);
    },(err)=>{
      this.submitDuplicateGroupMessage = "Some error occured";
      console.log(err.json());
    })
  }


  submitSalesAndPurchase() {
    let params = {
      "SaleDate": this.salesPurchase.date,
      "SaleType": this.salesPurchase.SaleType,
      "SaleOrigin": this.salesPurchase.saleOrigin,
      "Commodity": this.salesPurchase.commodity,
      "BuyerName": this.salesPurchase.buyerTradingName,
      "SellerName": this.salesPurchase.sellerTradingName,
      "SellerRepresentative": this.salesPurchase.sellerRepresentative,
      "BuyerRepresentative": this.salesPurchase.buyersRepresentative,
      "BusinessCreditedTo": this.salesPurchase.businessCreditedTo,
      "OfficeCreditedTo": this.salesPurchase.officeCreditedTo,
      "ProducerBrokerTrader": this.salesPurchase.producer,
      "ReferenceNo": this.salesPurchase.refNumber,
      "FFS": this.salesPurchase.FFS,
      "FFSInvoicedTo": this.salesPurchase.FFSInvoiced,
      "Variety": this.salesPurchase.variety,
      "Grade": this.salesPurchase.grade,
      "CropYear": this.salesPurchase.cropYear,
      "ContractNo": this.salesPurchase.contractNo,
      "ContractType": this.salesPurchase.contractType,
      "Quantity": this.salesPurchase.quantity,
      "ContractPrice": this.salesPurchase.contractPrice,
      "Discounts": this.salesPurchase.discounts,
      "Increments": this.salesPurchase.increment,
      "Area": this.salesPurchase.areahec,
      "MinYield": this.salesPurchase.minyuphec,
      "MaxYield": this.salesPurchase.maxyuphec,
      "MinQuantity": this.salesPurchase.minQuantity,
      "MaxQuantity": this.salesPurchase.maxQuantity,
      "Tolerance": this.salesPurchase.toleranceList,
      "DeliveryTerms": this.salesPurchase.delvTerms,
      "PaymentTerms": this.salesPurchase.payTermsRefData,
      "CommodityOrigin": this.salesPurchase.commodityOrigin,
      "CommodityDestination": this.salesPurchase.commodityDestination,
      "FarmingRegion": this.salesPurchase.farmRegion,
      "AdditionalFreight": this.salesPurchase.addFreight,
      "DeliveryPeriodFrom": this.salesPurchase.delPeriodFrom,
      "DeliveryPeriodTo": this.salesPurchase.delPeriodTo,
      "WeightTerms": this.salesPurchase.weightTerms,
      "ArbitrationRules": this.salesPurchase.rulesArb,
      "GradeStandards": this.salesPurchase.gradeStd,
      "GTAContractNo": this.salesPurchase.gtaContractNo,
      "GST": this.salesPurchase.gst,
      "BuyerSuppliedRCTI": this.salesPurchase.buyerRCTI,
      "SellerTaxInvoice": this.salesPurchase.sellerRCTI,
      "TransferDoneBy": this.salesPurchase.transtobedoneby,
      "VendorDeclarationReqd": this.salesPurchase.vendorDecl,
      "Mortgage": this.salesPurchase.mortgage,
      "Encumbrance": this.salesPurchase.encumb,
      "PlantBreeders": this.salesPurchase.plactBreedRight,
      "Royalties": "string",
      "SecurityInterest": this.salesPurchase.regUnregSecIntrst,
      "DisclosureComment": this.salesPurchase.briefDetails,
      "TradeDisputeResolutionRule": this.salesPurchase.incTradeAndDispResRule,
      "ContractConditions": this.salesPurchase.contractCondtn,
      "Status": this.salesPurchase.status,
      "OKToProcess": this.salesPurchase.okToProcess,
      "ConfirmationRevenue": this.salesPurchase.confirmRevenue,
      "BankDetailsToSeller": this.salesPurchase.bankDetailsToSeller,
      "ReceivedConfirmationFromSeller": this.salesPurchase.confirmationRecFrmSeller,
      "SellerRCTIReturnedToBuyer": this.salesPurchase.sellerRCTIReturnBuyer,
      "ContractReceivedFromBuyer": this.salesPurchase.contractRecFrmBuyer,
      "ContractSentToSeller": this.salesPurchase.contractSentToSeller,
      "ReceivedContractFromSeller": this.salesPurchase.recContractFrmSeller,
      "TransferConfirmedBy": this.salesPurchase.transConfirmBy,
      "UnitsDeliveredToBuyer": this.salesPurchase.unitsDelvToBuyer,
      "FinalDeliveryData": this.salesPurchase.finalDelvDate,
      "OKToInvoice": this.salesPurchase.okToInvoice,
      "ConfirmationRevenueInclGST": this.salesPurchase.confrmRevenueInclGST,
      "ConfirmationRevenueExclGST": this.salesPurchase.confrmRevenueExclGST,
      "InvoiceNo": this.salesPurchase.invoiceNo,
      "InvoiceDate": this.salesPurchase.invoiceDate,
      "InvoiceReceived": this.salesPurchase.invoiceReceivedAndClosed,
      "U_ID": this.selectedUserId
    }
    console.log("params", params);
    this.spinnerService.show();
    this.headers = new Headers({
      'Content-Type': 'application/json; charset=UTF-8',
      'Accept': 'application/json'
    });
    this.option = new RequestOptions({ headers: this.headers });

    let url = serviceUrl + "SalesAndPurchase/SalesAndPurchasePS/api/v1/SubmitSalesAndPurchaseData";

    this.http.post(url, params, this.option).subscribe((data) => {
      console.log('resp', data.json());
      // this.submitSalesAndPurchaseMsg = data.json().Response;
      // this.submitAddffForcast = data.json().Outcome;
      this.salesAndPurchaseModalForm.reset();
      // this.broadCaster.broadcast('Save Event', 'Saved Successfully.');
      this.spinnerService.hide();
      // $(".modal-content").scrollTop(0);
      this.salesAndPurchaseModal.hide();
    })
  }

  submitEditIDStatusMessage;
  submitEditIDStatus() {
    let url = `${serviceUrl}AgvantageEditUserPriorityAndStatusSB/AgvantageEditUserPriorityRestPS/api/v1/EditUserPriorityAndStatus`;
    let params = {
      "u_id": this.currentUser.USER.U_ID,
      "ID_Status": this.editIDStatusData.IDStatus,
      "Contact_frequency": this.editIDStatusData.ContactFrequency,
      "Priority": this.editIDStatusData.priority
    }
    this.spinnerService.show();
    this.http.post(url, params).subscribe((data) => {
      this.spinnerService.hide();
      this.responsemessage = data.json() ? data.json().result : [];
      this.editIDStatusModal.hide();
      setTimeout(() => {
      this.responsemessage = false;
      }, 4000);
    }, (err) => {
      this.submitEditIDStatusMessage = "Some error occured";
      this.editIDStatusModal.hide();
      setTimeout(()=>{this.submitEditIDStatusMessage = false;

      },4000);
    })
  }


  submitCreateGroup(){
    let url = `${serviceUrl}AgvantageAdminCreateNotificationGroupSB/AgvantageAdminCreateNotificationGroupRestPS/api/v1/CreateNotificationGroup`;
    let params = {
        "Group_Name" : this.creatGroupFormData.groupname,
        "Group_Description" : this.creatGroupFormData.description,
        "CreatedBy_Uid" : this.currentUser.USER.U_ID,
        "MembersList" : {
        "User_ID" : this.selectedIDs
      }
    }
      console.log("--- params" , params);
      this.spinnerService.show();
    this.http.post(url, params).subscribe((data)=>{
      this.spinnerService.hide();
      this.responsemessage = data.json()?data.json().result:[];
      this.createGroupModalTS.hide();
      setTimeout(()=>{this.responsemessage = false;
      },4000);
      this.getgroups();
    },(err)=>{
      this.submitCreateGroupMessage = "Some error occured";
      // setTimeout(()=>{this.submitCreateGroupMessage = false;
        this.createGroupModalTS.hide();
      // },4000);
      console.log(err.json());
    })
  }

  submitDuplicateGroup(){
    let url = `${serviceUrl}AgvantageAdminCreateNotificationGroupSB/AgvantageAdminCreateNotificationGroupRestPS/api/v1/CreateNotificationGroup`;
    let params = {
        "Group_Name" : this.dupGroupFormData.groupname,
        "Group_Description" : this.dupGroupFormData.description,
        "CreatedBy_Uid" : this.currentUser.USER.U_ID,
        "MembersList" : {
        "User_ID" : this.selectedIDs
      }
    }
      console.log("--- params" , params);
      this.spinnerService.show();
    this.http.post(url, params).subscribe((data)=>{
      this.spinnerService.hide();
      this.responsemessage = data.json()?data.json().result:[];
      this.duplicateGroupModalTS.hide();
      setTimeout(()=>{this.responsemessage = false;
      },4000);
      this.getgroups();
    },(err)=>{
      this.submitDuplicateGroupMessage = "Some error occured";
      console.log(err.json());
    })
  }



   sendSMSMessage;
  sendSMS(){
    let url = `${serviceUrl}AgvantageAdminSendSMSService/AgvantageAdminSendSMSPS/api/v1/SendSMS`;

    let params = {
      "Group_ID" : "1",
        "SMS_Body" : this.smsFormData.textmessage
      };
      console.log("--- params" , params);
      this.spinnerService.show();
    this.http.post(url, params).subscribe((data)=>{
      this.spinnerService.hide();
      console.log(data.json());
      this.responsemessage = data.json()?data.json().result:[];
      this.smsModal.hide();
      setTimeout(()=>{this.responsemessage = false;

      },4000);

    },(err)=>{
      this.sendSMSMessage = "Some error occured";
      console.log(err.json());
    })
  }

  emailEditConfMessage;
  emailEditConfiguration(){
    let url = `${serviceUrl}AgvantageAdminEditEmailConfiguration/AgvantageAdminEditEmailConfigurationPS/api/v1/EditConfiguration`;

    let params = {
        "FromEmail_ID" : this.editConfig.fromEmailId,
        "Signature" : this.editConfig.Signature
      };
      console.log("--- params" , params);
      this.spinnerService.show();
    this.http.post(url, params).subscribe((data)=>{
      this.spinnerService.hide();
      console.log(data.json());
      this.responsemessage = data.json()?data.json().result:[];
      this.editConfModal.hide();
      setTimeout(()=>{this.responsemessage = false;

      },4000);

    },(err)=>{
      this.emailEditConfMessage = "Some error occured";
      console.log(err.json());
    })
  }

  addToDoTask() {
    this.spinnerService.show();
    let url = `${serviceUrl}TODOTasks/ToDoTaskService/api/v1/AddToDoTask`;
    let params = {
      "activity_type": this.addToDoTaskData.activity_type,
      "start_date": this.addToDoTaskData.start_date,
      "start_time": this.addToDoTaskData.start_time,
      "duration": this.addToDoTaskData.duration,
      "schedule_with": this.addToDoTaskData.schedule_with,
      "regarding": this.addToDoTaskData.regarding,
      "notes": this.addToDoTaskData.notes,
      "reminder": this.addToDoTaskData.reminder,
      "u_id": this.currentUser.USER.U_ID
    };
    console.log("--- params", params);
    this.spinnerService.show();
    this.http.post(url, params).subscribe((data) => {
      this.spinnerService.hide();
      this.addToDoTaskMessage = data.json() ? data.json().response : [];
      this.addToDoTaskModal.hide();
      this.spinnerService.hide();
      this.getToDoTask();

    }, (err) => {
      this.addToDoTaskMessage = "Some error occured";
      console.log(err.json());
    })
  }

  addFarmingRegion(){

  }
  sendEmail(){

    let emailObj = {
      "To" : "pradeep.mishra@vedantus.com",
      "CC" : this.emailFormData.cc,
      "BCC" : this.emailFormData.bcc,
      "Subject" : this.emailFormData.subject,
      "Body" : this.emailFormData.body,
      "Attachments" : {
      "Attachment" : [ ]
      }
      };
      let url = `${serviceUrl}AgVantageNotificationsService/AgVantageNotifications/api/v1/SendEmail`;
      this.spinnerService.show();
      this.http.post(url,emailObj).subscribe(data=>{
        this.sendSMSMessage = "Email Sent Successfully";
        this.emailModal.hide();
        this.spinnerService.hide();
        setTimeout(()=>{
          this.sendSMSMessage = false;
        },4000);
      },
      err=>{
        alert("Server error");
      })


  }


  responsemessage;
  submitDestination(){
    let params = {
      "Destination_Type": this.addDestinationData.DestinationType,
      "Storage_Name": this.addDestinationData.StrgName,
      "Country_ID": "1",//"1",
      "State_ID": this.addDestinationData.State,
      "NTP_ID": this.addDestinationData.NTP,//"1",
      "CityTownSuburb": this.addDestinationData.citytownsuburb,
      "PostCode": this.addDestinationData.U_A_POSTCODE,
      "FarmingRegionMapping":this.farmRgnRowData,
      "GPSLocation": this.addDestinationData.GPS_LOCATION,
      "DestinationOwner": this.addDestinationData.DestiantionOwner,
      "TransportMode": this.addDestinationData.TransportMode,
      "TruckAccess": this.addDestinationData.TruckAccess,
      "LocationDifferential": this.addDestinationData.LocDiff
    }
    console.log('--- dest params', params);
    this.spinnerService.show();
    let url = `${serviceUrl}AgvantageAdminAddDestinationSB/AgvantageAdminAddDestinationRestPS/api/v1/AddDestination`;
    this.http.post(url, params).subscribe((data) => {
      this.responsemessage = data.json() ? data.json().response : [];
      setTimeout(()=>{this.responsemessage = false;},6000);
      this.addDestinationModal.hide();
      this.spinnerService.hide();
      // this.getToDoTask();

    }, (err) => {
      this.responsemessage = "Some error occured";
      setTimeout(()=>{this.responsemessage = false;},6000);
      this.addDestinationModal.hide();
      console.log(err.json());
    })
  }

  latitude;longitude;zoom;
  /* Show current position */
  setCurrentPosition(context) {

    // this.initMap(this);
    if ("geolocation" in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        console.log("setCurrentPosition ", position);
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom = 12;
          this.getGPSLocation(context);
        // this.initMap(this);
        // google.maps.event.addListener(window, 'load', this.initialize());
        // this.initializeAutocomplete();
      });
    }
  }
 gpsloctext;
  getGPSLocation(context) {

    // this.showGPSLocMap=true;
    let currentPosition = new google.maps.LatLng(-24.994167, 134.866944);//lat,long
    var markers = [];
    // Clear out the old markers.
    markers.forEach(function (marker) {
      marker.setMap(null);
    });
    markers = [];
    var map;
    var originIcon = 'https://chart.googleapis.com/chart?' +
      'chst=d_map_pin_letter&chld=O|FFFF00|000000';

    map = new google.maps.Map(document.getElementById("locationMapDiv"), {
      center: currentPosition,
      zoom: 13,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });
    console.log('--- map', map);


    // Create a marker for each place.
    markers.push(new google.maps.Marker({
      map: map,
      // icon: icon,
      position: currentPosition
    }));

    //Add listener
    google.maps.event.addListener(map, "click", function (event) {
      markers.forEach(function (marker) {
        marker.setMap(null);
      });
      markers = [];
      var latitude = event.latLng.lat();
      var longitude = event.latLng.lng();
      console.log(latitude + ', ' + longitude);

      this.gpsloctext = latitude + ', ' + longitude;


      context.addDestinationData.GPS_LOCATION = this.gpsloctext;
      // (<HTMLInputElement>document.getElementById("gpsloctext")).value = this.gpsloctext;
      console.log("GPS_LOCATION :", this.gpsloctext);

      markers.push(new google.maps.Marker({ map: map, position: event.latLng}));
      // let radius = new google.maps.Circle({
      //   map: map,
      //   radius: 800,
      //   center: event.latLng,
      //   fillColor: '#777',
      //   fillOpacity: 0.1,
      //   strokeColor: '#AA0000',
      //   strokeOpacity: 0.8,
      //   strokeWeight: 2,
      //   draggable: true,    // Dragable
      //   editable: true      // Resizable
      // });

      // Center of map
      // map.panTo(new google.maps.LatLng(latitude, longitude));

    }); //end addListener

    // google.maps.event.addListener(markers, 'dragend', function(evt){
    //   infoWindow.setOptions({
    //       content: '<p>Marker dropped: Current Lat: ' + evt.latLng.lat().toFixed(3) + ' Current Lng: ' + evt.latLng.lng().toFixed(3) + '</p>'
    //   });
    //   infoWindow.open(map, markers);
  // });

  // google.maps.event.addListener(markers, 'drag', function(evt){
  //     console.log("marker is being dragged");
  // });
  }



}
