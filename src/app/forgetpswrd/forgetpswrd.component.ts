import { Component, OnInit, ViewChild } from '@angular/core';
import {Http} from '@angular/http';
import {Router} from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import {serviceUrl} from "../app.module";
declare var $: any;

@Component({
    selector: 'app-forgetpswrd',
    templateUrl: './forgetpswrd.component.html',
    styleUrls: ['./forgetpswrd.component.scss']
})
export class ForgetpswrdComponent implements OnInit {
    reasons: any;
    showSuccessMsg: boolean;
    showFPBox: boolean;
    USER: any;
    usrname: string;
    checking: boolean;
    userStatusRes: any;
    hideValid: boolean;
    url: string;
    FPres: string;
    constructor(private http: Http, private spinnerService: Ng4LoadingSpinnerService,private router:Router) {
        this.showFPBox = true;
        this.FPres = "";
        this.showSuccessMsg = false;
        this.USER = { U_TYPE: "" };
        this.userStatusRes = {};
        this.checking = false;

		this.reasons = [
			{img: "assets/images/dashboardMenu/comprehensive.png", why: "Comprehensive Farm Marketing Plans"},
			{img: "assets/images/dashboardMenu/marketing.jpg", why: "Marketing Support"},
			{img: "assets/images/dashboardMenu/automation.jpg", why: "Automation, Price Alerts & Reminders"},
			{img: "assets/images/dashboardMenu/alwaysKnow.jpg", why: "Always know their position"},
			{img: "assets/images/dashboardMenu/recordHistory.jpg", why: "Records, History, & Reports - in one place "},
			{img: "assets/images/dashboardMenu/higlySecure.jpg", why: "Highly Secure "}];
    }
    @ViewChild('userform') public userform;
    checkUserStatus() {
        console.log(this.userform);
        let emailValid = this.userform.form.controls.email._status;
        this.checking = true;
        if (this.USER.U_USERNAME != undefined && this.USER.U_USERNAME.length > 0 && emailValid === "VALID") {
            this.checking = true;
            this.spinnerService.show();
            this.url = serviceUrl+"AccountGatewayCheckUserStatus/AccountGatewayUserStatusPS/checkUserStatus?USERNAME=" + this.USER.U_USERNAME;
            this.http.get(this.url).subscribe(data => {
                // Read the result field from the JSON response.
                this.checking = false;
                this.hideValid = true;
                console.log(data.json());
                this.userStatusRes = data.json();
                if(this.userStatusRes.Status==='ACTIVE'){
                 this.showMsg();
                }else if(this.userStatusRes.Status==='NOT EXISTS'){
                  this.spinnerService.hide();
                }
            });
        } else if (emailValid === "INVALID") {
            this.checking = false;
            this.hideValid = false;
             this.spinnerService.hide();
        }

    }
    showMsg() {
        this.showFPBox = false;
        let url = serviceUrl+"AccouneGatewayFPSB/AccountGatewayPS/api/v1/forgotPassword?username=" + this.USER.U_USERNAME;
        this.http.get(url).subscribe((data) => {
            console.log(data.json());
            this.showSuccessMsg = true;
             this.spinnerService.hide();
            this.FPres = data.json().Result;
        })


    }
gotoLogin(){
  this.router.navigate(['login']);
}

    ngOnInit() {
    }

}
