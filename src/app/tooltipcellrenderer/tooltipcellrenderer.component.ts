import { Component, OnInit } from '@angular/core';
import {ICellRendererAngularComp} from "ag-grid-angular";

@Component({
  selector: 'app-tooltipcellrenderer',
  templateUrl: './tooltipcellrenderer.component.html',
  styleUrls: ['./tooltipcellrenderer.component.scss']
})
export class TooltipcellrendererComponent implements ICellRendererAngularComp {

  public params: any;

    agInit(params: any): void {
        this.params = params;
    }

    public invokeParentMethod() {
        console.log(this.params.node);
        this.params.context.componentParent.methodFromParent(this.params.node);
    }

    refresh(): boolean {
        return false;
    }

}
