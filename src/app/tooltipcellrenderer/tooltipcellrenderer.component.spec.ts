import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TooltipcellrendererComponent } from './tooltipcellrenderer.component';

describe('TooltipcellrendererComponent', () => {
  let component: TooltipcellrendererComponent;
  let fixture: ComponentFixture<TooltipcellrendererComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TooltipcellrendererComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TooltipcellrendererComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
