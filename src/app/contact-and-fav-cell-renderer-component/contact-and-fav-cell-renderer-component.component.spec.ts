import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactAndFavCellRendererComponentComponent } from './contact-and-fav-cell-renderer-component.component';

describe('ContactAndFavCellRendererComponentComponent', () => {
  let component: ContactAndFavCellRendererComponentComponent;
  let fixture: ComponentFixture<ContactAndFavCellRendererComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactAndFavCellRendererComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactAndFavCellRendererComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
