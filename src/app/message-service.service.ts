import { Injectable } from '@angular/core';

@Injectable()
export class MessageService {
    message:string;
  constructor() { 
  this.message="";
  }

    getMessage():string{
      return this.message;  
    }
    
    setMessage(msg){
      this.message=msg;  
    }
}
