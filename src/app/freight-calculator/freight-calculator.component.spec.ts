import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreightCalculatorComponent } from './freight-calculator.component';

describe('FreightCalculatorComponent', () => {
  let component: FreightCalculatorComponent;
  let fixture: ComponentFixture<FreightCalculatorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreightCalculatorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreightCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
