import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import {Router} from '@angular/router';
import { UserRoleService } from '../user-role.service';
declare var google;
declare var $: any;
import {Http, Headers, RequestOptions} from '@angular/http';
import { Context } from 'ag-grid/dist/lib/context/context';
import {serviceUrl} from "../app.module";

@Component({
  selector: 'app-freight-calculator',
  templateUrl: './freight-calculator.component.html',
  styleUrls: ['./freight-calculator.component.scss']
})
export class FreightCalculatorComponent implements OnInit {
  freightCalc;
  SourceTypes;DestinaionTypes;
  td;
  srcsrch;
  destsrch;  
  public options = { types: ['address'], componentRestrictions: { country: 'AUS' } };
  @ViewChild('freightCalcform') public freightCalcform;

  @ViewChild("sourceSearch")
  public sourceSearchElementRef: ElementRef;
  
  @ViewChild("destiSearch")
  public destiSearchElementRef: ElementRef;
 

  constructor(private role: UserRoleService, private http: Http, private router: Router) {
    this.freightCalc={};
   }

  ngOnInit() {
    // this.getWarehouseTypes();
    // this.getDestinaionTypes();
    // this.getOnfarmWarehouseList();
    // this.getPublicWarehouseList();
    this.getTranseMethodList();
    // this.getTranseDetailsList();
    this.getDistance();
    this.initMap(this);
    // this.freightCalc.SourceType = 1;
    // this.freightCalc.DestinationType = 1;
    // this.callSourceType();
    // this.callDestType();
  }

  getWarehouseTypes() {
    console.log('this.role.getRole(): ', this.role.getRole());
    if (this.role.getRole() === "superadmin") {
      this.SourceTypes = [
        { T_ID: '1', T_NAME: 'On Farm' },
        { T_ID: '2', T_NAME: 'Ex Farm' }
      ]
    } else if (this.role.getRole() === "P") {
      this.SourceTypes = [
        { T_ID: '1', T_NAME: 'On Farm' },
        { T_ID: '2', T_NAME: 'Public' }
      ]
    }
  }

  getDestinaionTypes() {
    console.log('this.role.getRole(): ', this.role.getRole());
    if (this.role.getRole() === "superadmin") {
      this.DestinaionTypes = [
        { T_ID: '1', T_NAME: 'On Farm' },
        { T_ID: '2', T_NAME: 'Ex Farm' }
      ]
    } else if (this.role.getRole() === "P") {
      this.DestinaionTypes = [
        { T_ID: '1', T_NAME: 'On Farm' },
        { T_ID: '2', T_NAME: 'Public' }
      ]
    }
  }

  showExfarm; showPublic; showOnfarm
  callSourceType(){
    console.log('selected warehouse type: ',this.freightCalc.SourceType);
    if(this.freightCalc.SourceType == 1){
      this.showExfarm = false;
      this.showPublic = false;
      this.showOnfarm = true;
    }else if(this.freightCalc.SourceType == 2){
      if (this.role.getRole() === "superadmin"){
          this.showExfarm = true;
      }
      else{ 
      // if (this.role.getRole() === "p"){
          this.showPublic = true;
          this.showOnfarm = false;
          console.log('--- show public : ', this.showPublic);
      // }
      }
      // this.showOnfarm = true;
      console.log('--- show showOnfarm : ', this.showOnfarm);
    }
    // this.getWareHouseData(this.inventory.WarehouseType); 
  }

  showDestExfarm; showDestPublic; showDestOnfarm
  callDestType(){
    console.log('selected warehouse type: ',this.freightCalc.DestinationType);
    if(this.freightCalc.DestinationType == 1){
      this.showDestExfarm = false;
      this.showDestPublic = false;
      this.showDestOnfarm = true;
    }else if(this.freightCalc.DestinationType == 2){
      if (this.role.getRole() === "superadmin"){
          this.showDestExfarm = true;
      }
      else{ 
      // if (this.role.getRole() === "p"){
          this.showDestPublic = true;
          this.showDestOnfarm = false;
          console.log('--- show public : ', this.showDestPublic);
      // }
      }
      // this.showDestOnfarm = true;
      console.log('--- show showOnfarm : ', this.showDestOnfarm);
    }
    // this.getWareHouseData(this.inventory.WarehouseType); 
  }
  OnfarmList;
  getOnfarmWarehouseList(){
    console.log('inventory.Onfarm : ',this.freightCalc.Onfarm);
    let url = serviceUrl+'AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=OFW';
      this.http.get(url).subscribe((data) => {
        console.log('storage data:', data);
        this.OnfarmList = data.json().store;
        // this.selectedCommodity1=this.commodities[0].C_NAME;
        // console.log(this.commodities);
      })
  }
  PublicList;
  getPublicWarehouseList(){
    console.log('inventory.Onfarm : ',this.freightCalc.Onfarm);
    let url = serviceUrl+'AgcInvgetStorage/AgvInvGetStoragePS/api/v1/getStorage?store_type=PW';
      this.http.get(url).subscribe((data) => {
        console.log('storage data:', data);
        this.PublicList = data.json().store;
        // this.selectedCommodity1=this.commodities[0].C_NAME;
        // console.log(this.commodities);
      })
  }

  TransMethodList;
  getTranseMethodList(){
    console.log('inventory.Onfarm : ',this.freightCalc.Onfarm);
    
    let url = serviceUrl+'GetTransportationDetailsSB/GetTransportationDetailsPS/api/v1/getTransportationMethods?request=getTransportationMethod'
      this.http.get(url).subscribe((data) => {
        console.log('transe method data:', data.json().TransportationMethodList);
        this.TransMethodList = data.json().TransportationMethodList.TransportationMethod;
        // this.selectedCommodity1=this.commodities[0].C_NAME;
        // console.log(this.commodities);
      })
  }
  TransDetailsList;
  getTranseDetailsList(){
    console.log('inventory.Onfarm : ',this.freightCalc.Onfarm);
    
    let url = serviceUrl+'GetTransportationDetailsSB/GetTransportationDetailsPS/api/v1/getTransportationDetailsById?transportation_id=1'
      this.http.get(url).subscribe((data) => {
        console.log('trans details data:', data);
        this.TransDetailsList = data.json().TransportationTypeList.TransportationType;
        // this.selectedCommodity1=this.commodities[0].C_NAME;
        // console.log(this.commodities);
      })
  }

  callDetails(){
    this.getTranseDetailsList();
  }

  transDetails(selectedTransDetails){
    console.log('selectedTransDetails', selectedTransDetails);
    console.log('selectedTransDetails', this.freightCalc.transDetails);
  }

  calculateFreight(){
    if(this.freightCalc.distance != undefined && this.freightCalc.transDetails!=undefined){
    console.log('calc freight',  this.freightCalc.transDetails * this.freightCalc.distance);
    this.freightCalc.freight =   this.freightCalc.transDetails * this.freightCalc.distance;
    }
  }
  // this.showDestEx = false;
  //     this.showDestPub = false;
  //     this.showDestOnFarm = true;
  closeModal(){
    this.freightCalcform.reset();
  }

   /* Show current position */
  calDistance() {   
    this.initMap(this);    
  }

  inputSource;inputDest;sourceBox;destBox;
  getDistance(){
    
    this.inputSource = this.sourceSearchElementRef.nativeElement;
    this.inputDest = this.destiSearchElementRef.nativeElement;
    this.sourceBox = new google.maps.places.SearchBox(this.inputSource);
    this.destBox = new google.maps.places.SearchBox(this.inputDest);
    
    // this.initMap(this);
  }
    gotoHome() {
        this.router.navigate(['dashboard']);
    }

  initMap(contxt) {         
    
    // if(this.sourceBox!=undefined){
    var src = this.sourceBox.getPlaces();
    
    var dest = this.destBox.getPlaces();    
      
    var origin1 = src[0].formatted_address; //'Pune, Maharashtra, India'; //{lat: 55.93, lng: -3.118};
    // var origin2 = 'Greenwich, England';
    var destinationA = dest[0].formatted_address; //'Delhi, India';
    // var destinationB = {lat: 50.087, lng: 14.421};      
    var service = new google.maps.DistanceMatrixService;

    service.getDistanceMatrix({
      // origins: [origin1, origin2],
      origins: [origin1],
      // destinations: [destinationA, destinationB],
      destinations: [destinationA],
      travelMode: 'DRIVING',
      unitSystem: google.maps.UnitSystem.METRIC,
      avoidHighways: false,
      avoidTolls: false
    }, function(response, status) {
      if (status !== 'OK') {
        alert('Error was: ' + status);
      } else {
        var originList = response.originAddresses;
        var destinationList = response.destinationAddresses;                  
        for (var i = 0; i < originList.length; i++) {
          var results = response.rows[i].elements;          
          for (var j = 0; j < results.length; j++) {
            var distance = results[j].distance.text.split(" ")[0];                
            distance = distance.replace( /,/g, "" );
            contxt.freightCalc.distance = distance;      
            console.log('--- distance : ', distance);            
          }
        }
      }
    });
  }
// }
}