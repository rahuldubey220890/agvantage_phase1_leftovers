import { Component, ViewChild, OnInit, ChangeDetectorRef } from '@angular/core';
import {GridOptions} from "ag-grid/main";
import {Http, Headers, RequestOptions} from '@angular/http';
import { Broadcaster } from '../broadcaster';
import {serviceUrl} from "../app.module";
import { CommodityUpdateCellRendererComponent } from '../commodity-update-cell-renderer/commodity-update-cell-renderer.component';
@Component({
    selector: 'app-addedcommoditylist',
    templateUrl: './addedcommoditylist.component.html',
    styleUrls: ['./addedcommoditylist.component.scss']
})
export class AddedcommoditylistComponent implements OnInit {
    gridOptions: GridOptions;
    submitCommodity:any;
    gridApi:any;
    gridColumnApi:any;
    columnDefs:any;
    options: RequestOptions;
    rowData:any;
    defaultColDef:any;
    getRowNodeId:any;
    url: string;
    commodityGrid: any;
    commodity: any;
    context:any;
    frameworkComponents:any;
    saveMessage:any;
    headers: Headers;

    ngOnInit() {

        this.getGridData();
        this.getCommodities();
        this.getCropyearList();
    }

    constructor(private cdRef: ChangeDetectorRef, private http: Http, private broadCaster: Broadcaster) {

        // we pass an empty gridOptions in, so we can grab the api out
        this.commodity = {};
        this.gridOptions = <GridOptions>{
            onGridReady: () => {
               this.gridApi.autoSizeColumns();
            }
        };


        this.columnDefs = [
            { headerName: "Actions", field: "actions", width: 100, cellRenderer: 'commodityUpdateCellRendererComponent', cellEditor: 'agRichSelect', editable: true },
            { headerName: "Commodity Name", field: "CommodityName", width: 100 },

            { headerName: "Crop Year", field: "sccCropYr", width: 100 },
            { headerName: "Area (ha.)", field: "sccAreaH", width: 100 },
            { headerName: "Yield/ha.", field: "sccYieldPerH", width: 100 },
            { headerName: "Sharefarmer%", field: "sccAvlStrgPer", width: 100 },
            { headerName: "Stock on farm", field: "sccRemAmt", width: 100 },
            { headerName: "Stock in market", field: "sccMrkt", width: 100 },
            { headerName: "Labour charge", field: "sccLbr", width: 100 },
            { headerName: "Repairs null", field: "sccMaint", width: 100 },
            { headerName: "Operating-Interest", field: "sccOperIntrst", width: 100 },
            { headerName: "Other-Interest", field: "sccOthIntrst", width: 100 },
            { headerName: "Rent/Mortgage", field: "sccRent", width: 100 },
            { headerName: "Seed", field: "sccSeed", width: 100 },
            { headerName: "Fertilizer", field: "sccFrtlzr", width: 100 },
            { headerName: "Chemicals", field: "sccChem", width: 100 },
            { headerName: "Machine Hire", field: "sccMcnCntr", width: 100 },
            { headerName: "Supplies/sundries", field: "sccSuppliers", width: 100 },
            { headerName: "Gas/Fuel/Oil", field: "sccFuel", width: 100 },
            { headerName: "Taxes", field: "sccTaxes", width: 100 },
            { headerName: "Insurance", field: "sccInsrns", width: 100 },
            { headerName: "Utilities", field: "sccUtlt", width: 100 },
            { headerName: "Freight", field: "sccFreight", width: 100 }
        ];

        this.broadCaster.on<string>('Save Event')
            .subscribe(message => {
                this.gridOptions.api.showLoadingOverlay()
                this.getGridData();
                this.saveMessage = message;
            });
        this.broadCaster.on<string>('Update Event')
            .subscribe(message => {
                this.gridOptions.api.showLoadingOverlay()
                this.getGridData();
                this.saveMessage = message;
            });


        this.context = { componentParent: this };
        this.frameworkComponents = {
            commodityUpdateCellRendererComponent: CommodityUpdateCellRendererComponent
        };


        this.getRowNodeId = function(data) {
            return data.id;
        };

    } // constructor
    @ViewChild('updateCommodity') updateCommodity;
    @ViewChild('addCommodityForm') addCommodityForm;

    getGridData() {
        this.url = serviceUrl+"AgVantageGetSellerCommodityListSB/GetSellerCommodityListPS/api/v1/getSellerCommodityList?UID=1";
        this.http.get(this.url).subscribe((data) => {
            this.commodityGrid = data.json().SellerCommCost;
            //this.selectedCommodity1=this.commodities[0].C_NAME;
            console.log(this.commodityGrid);
            this.rowData = this.commodityGrid;
        })
    }

    onGridReady(params) {
        this.gridApi = params.api;
        this.gridColumnApi = params.columnApi;

    }
    commodities;
    getCommodities() {
        this.url = serviceUrl+"AgVantageGetCommoditiesSB/getCommodities/api/v1/getCommodity?getCommodities=true";
        this.http.get(this.url).subscribe((data) => {
            this.commodities = data.json().CommodityCollection.Commodity;
            //this.selectedCommodity1=this.commodities[0].C_NAME;
            console.log(data.json());
        })

    }
    cropYearList;
    getCropyearList() {
        let url = 'http://129.154.71.56:9073/AgvGetCropYear/GetCropYear/api/v1/getCropYear?getcropyer=true';
        this.http.get(url).subscribe((data) => {
            // console.log('crp year data:',data.json().CropYear);
            this.cropYearList = data.json().CropYear;
            // this.selectedCommodity1=this.commodities[0].C_NAME;
            // console.log(this.commodities);
        })
    }

    closeModal() {
        this.updateCommodity.hide();
    }

    callGrades() {
        this.getCommodityGrade(this.commodity.sccCId);
    }
    grades;
    getCommodityGrade(id) {
        this.url = serviceUrl+"AgVantageGetCommodityGrade/AgVantageGetGradePS/api/v1/getGrade?C_ID=" + id;
        this.http.get(this.url).subscribe((data) => {
            this.grades = data.json().gradeCollection.grade;
        })
    }

    methodFromParent(rowObj) {
        console.log("invoke modal", rowObj.data);
        this.commodity = rowObj.data;
        this.getCommodityGrade(this.commodity.sccCId);
        this.updateCommodity.show();
    }
    selectAllRows() {
        this.gridOptions.api.selectAll();
    }

    updateCommodityData() {

        // console.log('comm params', this.commodity);
        let params = {
            SCC_U_ID: this.commodity.sccUId,
            SCC_C_ID: this.commodity.sccCId,
            SCC_CROP_YR: this.commodity.sccCropYr,
            SCC_AREA_H: this.commodity.sccAreaH,
            SCC_YIELD_PER_H: this.commodity.sccYieldPerH,
            SCC_REM_AMT: this.commodity.sccRemAmt,
            SCC_AVL_STRG_PER: this.commodity.sccAvlStrgPer,
            SCC_OLD_CRP_LFT: this.commodity.sccOldCrpLft
            ,
            SCC_LBR: this.commodity.sccLbr,
            SCC_MAINT: this.commodity.sccMaint,
            SCC_OPER_INTRST: this.commodity.sccOperIntrst,
            SCC_OTH_INTRST: this.commodity.sccOthIntrst,
            SCC_RENT: this.commodity.sccRent,
            SCC_SEED: this.commodity.sccSeed,
            SCC_FRTLZR: this.commodity.sccFrtlzr,
            SCC_CHEM: this.commodity.sccChem,
            SCC_MCN_CNTR: this.commodity.sccMcnCntr,
            SCC_SUPPLIERS: this.commodity.sccSuppliers,
            SCC_FUEL: this.commodity.sccFuel,
            SCC_TAXES: this.commodity.sccTaxes,
            SCC_INSRNS: this.commodity.sccInsrns,
            SCC_UTLT: this.commodity.sccUtlt,
            SCC_MRKT: this.commodity.sccMrkt,
            SCC_FREIGHT: this.commodity.sccFreight
        }
        console.log('params ', params);

        let URL = serviceUrl+'AgVCommodityRegistartion/AgvantageCommodityRegPS/api/v1/commRegister';
        this.headers = new Headers({
            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
            'Accept': 'application/json'
        });
        this.options = new RequestOptions({ headers: this.headers });
        this.http.post(URL, $.param(params), this.options).subscribe((data) => {
            console.log('--- update : ', data.json());
            if (!data.json().errorCode) {
                this.addCommodityForm.reset();
                this.broadCaster.broadcast('Update Event', 'Updated Successfully.');
                $(".modal-content").scrollTop(0);
                this.updateCommodity.hide();
            } else {
                this.broadCaster.broadcast('Update Event', data.json().errorMessage);
                this.updateCommodity.hide();
            }
            this.getGridData();
        })
    }
}// class
