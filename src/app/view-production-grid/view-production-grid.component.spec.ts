import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewProductionGridComponent } from './view-production-grid.component';

describe('ViewProductionGridComponent', () => {
  let component: ViewProductionGridComponent;
  let fixture: ComponentFixture<ViewProductionGridComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewProductionGridComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewProductionGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
