import { Component, OnInit,ChangeDetectorRef,AfterViewInit,NgZone } from '@angular/core';
import  {Router} from '@angular/router';
@Component({
  selector: 'app-view-production-grid',
  templateUrl: './view-production-grid.component.html',
  styleUrls: ['./view-production-grid.component.scss']
})
export class ViewProductionGridComponent implements OnInit,AfterViewInit {

  constructor(private cdRef:ChangeDetectorRef,private router:Router,private _ngZone: NgZone) { }

  ngOnInit() {
  }
  ngAfterViewInit(){
      this.cdRef.detectChanges();
      }
gotoHome(){
    this._ngZone.run(() => this.router.navigate(['dashboard']));
    }
}
